<?php
/*
 *  modele acpa: ce fichier contient toutes les requetes necessaire au dialogue entre l'interface de gestion et la base de données. elles sont triées en class:
 * - la class ACPA qui contient les informations de connexion à la bdd
 * - la classe getter qui contient les requetes de lecture des informations contenu dans la bdd
 * - la classe setter qui contient les requetes d'écriture dans la bdd
 *
 * lexique:
 * - adherent: concernet les personnes inscrite a une des activités sportives du club
 * - contact: concerne les personnes qui ont un lien avec le club
 * - personne: regroupe les adherents et les contacts
 */


/*
 * Appel du fichier de configuration qui contient les informations de connexions
 */
require("conf.php");


/**
 * Class Acpa
 * cette classe contient les function général de liaison avec la base de données ACPA:
 * - fonction de connection à la BDD
 * - fonction de demarrage d'une transaction
 * - fonction de commit d'une transaction
 * - fonction de rollback d'une transaction
 */
class Acpa
{
    /**
     * function de création de la connection a la bdd
     * @return PDO connexion a la bdd
     */
    public function dbconnect()
	{
        
        // recuperation des constantes contenu dans conf.php    
        $host = DB_HOST;
        $dataBaseName = DB_NAME; 
        $user = DB_USER;
        $pass = DB_PASS;
		
		
		$db = new PDO('mysql:host=' . $host . ';dbname=' . $dataBaseName , $user, $pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				
		return $db;
	}

    /**
     * fonction de demarage d'une transaction
     * @param $db object connexion a la base de données
     */
    public function startTransaction($db)
    {
        
        $db->beginTransaction();
    }

    /**
     * fonction de commit
     * a appeler s'il n'y a pas d'erreur sur les transactions
     * @param $db object connexion a la base de données
     */
    public function execTransaction($db)
    {
        $db->commit();
    }

    /**
     * fonction de rollback
     * a appeler s'il y a des erreurs sur les transactions
     * @param $db object connexion a la base de données
     */
    public function abordTransaction($db)
    {
        $db->rollback();
    }
    
    
}


/**
 * class contenant toute les fonctions de requetes de lecture de la bdd
 */
class GetterAcpa extends Acpa
{

    /**
     * requete simplifié de recuperation de la liste des adherents, de leurs adhesions et de leurs saisons
     * @param $db object connexion a la base de données
     * @return mixed liste des adherents
     */
	public function getListAdherent($db)
	{
		$listAdherents = $db->query(
						'SELECT acpa_personne.*, 
                        acpa_adhesion.*, 
                        acpa_saison.*
                        FROM acpa_personne 
                        LEFT JOIN acpa_adhesion ON acpa_adhesion.id_personne = acpa_personne.id_personne 
                        LEFT JOIN acpa_saison ON acpa_adhesion.id_saison = acpa_saison.id_saison
                        WHERE acpa_personne.id_organisation IS NULL
                        AND acpa_saison.yn_saison_ouverte = 1
                        ORDER BY lib_nom');
		
		return $listAdherents;
		$listAdherents->closeCursor();
	}

    /**
     * requete simplifié de recuperation de la liste des Contacts
     * @param $db object connexion a la base de données
     * @return mixed liste des contacts
     */
	public function getListContact($db)
	{
		$listContacts = $db->query(
						'SELECT * 
						FROM acpa_personne
						WHERE acpa_personne.id_organisation IS NOT NULL');
		
		return $listContacts;
		$listContacts->closeCursor();
	}


    /**
     * requete simplifié de recuperation de la liste des Paiements
     * @param $db object connexion a la base de données
     * @return mixed liste des payements
     */
	public function getListPayment($db)
	{
		$lignePayments = $db->query(
						'SELECT * 
						FROM acpa_ligne_paiement');
		
		return $lignePayments;
		$lignePayments->closeCursor();
	}

  
    /**
     * requete simplifié de recuperation de la liste des Saisons
     * @param $db object connexion a la base de données
     * @return mixed liste des saisons
     */
	public function getSaison($db)
	{
		$season = $db->query(
				'SELECT *
				FROM acpa_saison
				ORDER BY lib_saison');
		return $season;
		$season->closeCursor();
	}



    /**
     * requete simplifié de recuperation de la liste des Activites
     * @param $db object connexion a la base de données
     * @return mixed liste des activités
     */
	public function getActivity($db)
	{
		$activity = $db->query(
				'SELECT *
				FROM acpa_activite
				ORDER BY id_activite');
		return $activity;
		$activity->closeCursor();
	}	
		


    /**
     * requete simplifié de recuperation de la liste des Roles
     * @param $db object connexion a la base de données
     * @return mixed liste des roles
     */
	public function getRoles($db)
	{
		$roles = $db->query(
				'SELECT *
				FROM acpa_role
				ORDER BY id_role');
		return $roles;
		$roles->closeCursor();
	}


    /**
     * requete simplifié de recuperation de la liste des Organisation
     * @param $db object connexion a la base de données
     * @return mixed liste des organisation
     */
    public function getOrganisation($db)
	{
		$Organism = $db->query(
				'SELECT *
				FROM acpa_organisation
				ORDER BY id_organisation');
		return $Organism;
		$Organism->closeCursor();
	}


    /**
     * requete de recuperation de la liste des couples adhesion-activité
     * ayant pour paramettre un identifiant d'adhesion et un identifiant d'activity normalement la liste contient 0 ou 1 élement)
     * @param $db object connexion a la base de données
     * @param $idAdhesion int identifiant de l'adhesion
     * @param $idActivite int identifiant de l'activité
     * @return mixed liste nul ou d'un élément
     */
    public function getActivityAdhesionExist($db, $idAdhesion, $idActivite)
    {
		$getActivityAdhesionExist = $db->prepare(
				'SELECT * 
                FROM acpa_activite_adh 
                WHERE id_adhesion = :idAdhesion 
                And id_activite = :idActivity');
        $getActivityAdhesionExist->execute(array('idAdhesion' => $idAdhesion,
                                        'idActivity' => $idActivite));
		return $getActivityAdhesionExist;
		$getActivityAdhesionExist->closeCursor();
	}


    /**
     * requete de recuperation de la liste des couples adhesion-role
     * ayant pour paramettre un identifiant d'adhesion et un identifiant de role normalement la liste contient 0 ou 1 élement)
     * @param $db object connexion a la base de données
     * @param $idAdhesion int identifiant de l'adhesion
     * @param $idRole int identifiant du role
     * @return mixed liste nul ou d'un élément
     */
    public function getRoleAdhesionExist($db, $idAdhesion, $idRole)
    {
		$getRoleAdhesionExist = $db->prepare(
				'SELECT * 
                FROM acpa_role_adhesion 
                WHERE id_adhesion = :idAdhesion 
                And id_role= :idRole');
        $getRoleAdhesionExist->execute(array('idAdhesion' => $idAdhesion,
                                        'idRole' => $idRole));
		return $getRoleAdhesionExist;
		$getRoleAdhesionExist->closeCursor();
	}


    /**
     * requete simplifié de recuperation de la liste des types de licences
     * @param $db object connexion a la base de données
     * @return mixed liste des licences
     */
	public function getTypeLic($db)
	{
		$typeLic = $db->query(
				'SELECT *
				FROM acpa_type_licence
				ORDER BY lib_type_licence');
		return $typeLic;
		$typeLic->closeCursor();
	}


    /**
     * requete simplifié de recuperation de la liste des types de Statuts d'adhesion
     * @param $db object connexion a la base de données
     * @return mixed liste des differents statut d'adhesion
     */
	public function getStatusAdhesion($db)
	{
		$status = $db->query(
				'SELECT *
				FROM acpa_statut
				ORDER BY id_statut');
		return $status;
		$status->closeCursor();
	}


    /**
     * requete simplifié de recuperation de la liste des Categories d'Ages
     * @param $db object connexion a la base de données
     * @return mixed liste des ctaégories d'ages
     */
	public function getCategory($db)
	{
		$category = $db->query(
			'SELECT * 
			FROM acpa_categorie_age 
			ORDER BY acpa_categorie_age.num_tri_categorie_age');
		return $category;
		$category->closeCursor();
	}


    /**
     * requete simplifié de recuperation de la liste des Moyens de Paiement accepté par l'ACPA
     * @param $db object connexion a la base de données
     * @return mixed liste des moyens de payement
     */
	public function getPayMethod($db)
	{
		$category = $db->query(
			'SELECT * 
            FROM acpa_moyen_paiement 
            ORDER BY acpa_moyen_paiement.id_moyen_paiement ASC');
		return $category;
		$category->closeCursor();
	}


    /**
     * requete simplifié de recuperation de l'ID de la derniere personne integré à la bdd
     * @param $db object connexion a la base de données
     * @return mixed int numero d'identification du dernier adhérent
     */
	public function getLastPerson($db)
	{
		$lastAdherent = $db->query(
				'SELECT max(id_personne) 
				AS max_id from acpa_personne');
		return $lastAdherent;
		$lastAdherent->closeCursor();
	}


    /**
     * requete de recuperation de l'identifiant d'une saison en fonction de son libellé
     * @param $db object connexion a la base de données
     * @param $seasonAdhesion String libellé de la saison
     * @return mixed int numero d'identification de la saison
     */
	public function getSeasonAdhesion($db, $seasonAdhesion)
	{
		$idSeasonAdhesion = $db->prepare(
				'SELECT id_saison 
				FROM acpa_saison 
				WHERE lib_saison = :saison ');
        $idSeasonAdhesion->execute(array('saison' => $seasonAdhesion));
		return $idSeasonAdhesion;
		$idSeasonAdhesion->closeCursor();
	}


    /**
     * requete de recuperation de l'identifiant d'un statut de paiement en fonction de son libellé
     * @param $db object connexion a la base de données
     * @param $etatPayement String libellé du status de paiement
     * @return mixed int numero d'identification du status de paiement
     */
	public function getStatusPayAdhesion($db, $etatPayement)
	{
		$idStatutPayment = $db->prepare(
				'SELECT id_statut 
				FROM acpa_statut 
				WHERE lib_statut = :status');
        $idStatutPayment->execute(array('status' => $etatPayement));
		return $idStatutPayment;
		$idStatutPayment->closeCursor();
	}


    /**
     * requete de recuperation de l'identifiant d'une categories d'age en fonction de son libellé
     * @param $db object connexion a la base de données
     * @param $catAge String libellé de la catégorie d'age
     * @return mixed int numero d'identification de la categorie d'age
     */
	public function getCategoryAdhesion($db, $catAge)
	{
		$idCatAge = $db->prepare(
				'SELECT id_categorie_age 
				FROM acpa_categorie_age 
				WHERE lib_categorie_age = :catAge');
		$idCatAge->execute(array('catAge' => $catAge));
		return $idCatAge;
		$idCatAge->closeCursor();
	}


    /**
     * requete de recuperation de l'identifiant d'un Type de licence en fonction de son libellé
     * @param $db object connexion a la base de données
     * @param $typeLic String libellé du type de licence
     * @return mixed  int numero d'identification du type de licence
     */
	public function getTypLicAdhesion($db, $typeLic)
	{
		$idTypeLicence = $db->prepare(
				'SELECT id_type_licence 
				FROM acpa_type_licence 
				WHERE lib_type_licence = :typeLic');
		$idTypeLicence->execute(array('typeLic' => $typeLic));
		return $idTypeLicence;
		$idTypeLicence->closeCursor();
	}


    /**
     * requete simplifié de recuperation de l'ID de la derniere adhesion integré à la bdd
     * @param $db object connexion a la base de données
     * @return mixed int numero d'identification de la derniere adhesion
     */
	public function getLastAdhesion($db)
	{
		$newAdhesion = $db->query(
				'SELECT max(id_adhesion) 
				AS max_idad from acpa_adhesion');
		return $newAdhesion;
		$newAdhesion->closeCursor();
	}


    /**
     * requete simplifié de recuperation de l'ID de la derniere organisation integré à la bdd
     * @param $db object connexion a la base de données
     * @return mixed int numero d'identification de la derniere organisation
     */
	public function getLastOrganisation($db)
	{
		$newOrganisation = $db->query(
				'SELECT max(id_organisation) 
				AS max_idorg from acpa_organisation');
		return $newOrganisation;
		$newOrganisation->closeCursor();
	}


    /**
     * requete de recuperation de l'identifiant d'une activités en fonction de son libellé
     * @param $db object connexion a la base de données
     * @param $activite String libellé du type de licence
     * @return mixed int numero d'identification du type de licence
     */
	public function getIdsActivitysAdhesion($db, $activite)
	{
		$idactivity = $db->prepare(
				'SELECT id_activite 
				FROM acpa_activite 
				WHERE lib_court_activite = :activity');
		$idactivity->execute(array('activity' => $activite));
		return $idactivity;
		$idactivity->closeCursor();
	}


    /**
     * requete de recuperation de l'identifiant d'une personne en fonction de
     * - son nom
     * - son prénom
     * - sa date de naissance
     * @param $db object connexion a la base de données
     * @param $nom String nom de l'adherent
     * @param $prenom String prenom de l'adherent
     * @param $dateNaissance (date) date de naissance de l'adherent
     * @return mixed int numero d'identification de l'adherent
     */
	public function getIdUpdatePerson($db, $nom, $prenom, $dateNaissance)
	{
		$idUpdatePersonne = $db->prepare(
				'SELECT id_personne 
				FROM acpa_personne 
				WHERE lib_nom = :nomAd
                AND lib_prenom = :prenomAd
                AND dt_naissance = :dtNaissance');
		$idUpdatePersonne->execute(array('nomAd' => $nom,
                                  'prenomAd' => $prenom,
                                  'dtNaissance' => $dateNaissance));
		return $idUpdatePersonne;
		$idUpdatePersonne->closeCursor();
	}


    /**
     * requete complexe de recuperation de la liste des adherents, leurs activité en fonction
     * - de la saison
     * - de la categorie
     * - des première lettre du nom
     * - des première lettre du prenom
     * - des premier chiffre du numero de SIFFA
     * - de l'activité
     * - des première lettre de l'adresse Mail
     * @param $db object connexion a la base de données
     * @param $saison String Libellé de la saison
     * @param $catage String Libellé de la catégorie d'age
     * @param $nom String nom de l'adhérent
     * @param $prenom String prénom de l'adhérent
     * @param $numeroLicence int numéro de licence de l'adhérents
     * @param $activite String Libellé de l'activité
     * @param $mel String adresse mail de l'adhérent
     * @param $siffa String numéro de siffa de l'adhérents
     * @param $sorting String type de tri
     * @return mixed liste des adhérents corespondant aux critères
     */
	public function getListAdherents(
	    $db,
        $saison,
        $catage,
        $nom,
        $prenom,
        $numeroLicence,
        $activite,
        $mel,
        $siffa,
        $sorting
    )
		{
        
        /*
         * mise en forme des variables de la requete:
         * - si la variable est vide alors elle est instancié par le caractère %
         */
        if($saison == "" || $saison == " " ){
            $saison = '%';
        }else{
            $saison = $saison.'%';
        } 
        if($catage == "" || $catage == " " ){
            $catage = '%';
        }else{
            $catage = $catage.'%';
        } 
        if($nom == "" || $nom == " " ){
            $nom = '%';
        }else{
            $nom = $nom.'%';
        } 
        if($prenom == "" || $prenom == " " ){
            $prenom = '%';
        }else{
            $prenom = $prenom.'%';
        } 
        if($numeroLicence == "" || $numeroLicence == " " ){
            $numeroLicence = '%';
        }else{
            $numeroLicence = $numeroLicence.'%';
        }
        if($activite == "" || $activite == " " ){
            $activite = '%';
        }else{
            $activite = $activite.'%';
        }
        if($mel == "" || $mel == " " ){
            $mel = '%';
        }else{
            $mel = $mel.'%';
        }
        if($siffa == "" || $siffa == " " ){
            $siffa = '%';
        }else{
            $siffa = $siffa.'%';
        }
        
        /*
         * création du tri de la requete en fonction de la variable $sorting
         */
        switch ($sorting){
            case 'orderNameAsc':
                $sort = 'acpa_personne.lib_nom DESC';
                break;
            case 'orderNameDesc':                
                $sort = 'acpa_personne.lib_nom ASC';
                break;
            case 'orderFirstNameAsc':
                $sort = 'acpa_personne.lib_prenom DESC';
                break;
            case 'orderFirstNameDesc':
                $sort = 'acpa_personne.lib_prenom ASC';
                break;                
            case 'orderNumAsc':
                $sort = 'acpa_personne.num_licence_SIFFA DESC';
                break;
            case 'orderNumDesc':
                $sort = 'acpa_personne.num_licence_SIFFA ASC';
                break;
            case 'orderActivityAsc':
                $sort = 'acpa_activite.lib_activite DESC';
                break;
            case 'orderActivityDesc':
                $sort = 'acpa_activite.lib_activite ASC';
                break;
            case 'orderMelAsc':
                $sort = 'acpa_personne.lib_mail_personne DESC';
                break;
            case 'orderMelDesc':
                $sort = 'acpa_personne.lib_mail_personne DESC';
                break;                
            default:
                $sort = 'acpa_personne.lib_nom DESC';
                break;
                
        }
                        

        $listAdherent = $db->prepare('
            SELECT acpa_activite.lib_activite, 
            acpa_personne.*,
            acpa_saison.lib_saison,
            GROUP_CONCAT(DISTINCT acpa_activite.lib_activite) AS list_activite
            FROM acpa_personne
            LEFT JOIN acpa_adhesion ON acpa_personne.id_personne = acpa_adhesion.id_personne
            LEFT JOIN acpa_activite_adh ON  acpa_adhesion.id_adhesion = acpa_activite_adh.id_adhesion
            LEFT JOIN acpa_activite ON acpa_activite.id_activite = acpa_activite_adh.id_activite
            LEFT JOIN acpa_saison ON acpa_adhesion.id_saison = acpa_saison.id_saison
            WHERE acpa_personne.lib_nom LIKE :nom
            AND acpa_personne.lib_prenom LIKE :prenom
            AND acpa_personne.num_licence_SIFFA LIKE :numlic
            AND acpa_adhesion.id_saison LIKE :saison
            AND acpa_adhesion.id_categorie_age LIKE :cat
            AND acpa_activite.id_activite LIKE :activity
            AND acpa_personne.lib_mail_personne LIKE :mail
            AND acpa_personne.id_organisation IS NULL
            GROUP BY acpa_personne.id_personne
            ORDER BY '. $sort);

        $listAdherent->execute(array(
            'nom' => $nom, 
            'prenom' => $prenom, 
            'numlic' => $numeroLicence, 
            'saison' => $saison, 
            'cat' => $catage, 
            'activity' => $activite,
            'mail' => $mel));
    
		
        return $listAdherent;
        $listAdherent->closeCursor();
	}


    /**
     * requete complexe de recuperation de la liste des contacts, leurs roles et leur organisation en fonction
     * - des première lettre du nom
     * - des première lettre du prenom
     * - des première lettre de l'organisation
     * - des première lettre de l'adresse Mail
     * - des premier chiffre du numero de téléphone portable
     * - des premier chiffre du numero de téléphone fixe
     * - de lettre contenu dans l'adresse
     * - du roles
     * integre aussi le notion de tri par ordre alphabetique
     * @param $db object connexion a la base de données
     * @param $Name String nom du contact
     * @param $nickName String prenom du contact
     * @param $organism String nom de l'organisme
     * @param $mail String adresse mail du contact
     * @param $portable int numeros de téléphone portable du contact
     * @param $fixe int numeros de téléphone fixe du contact
     * @param $adress String adresse du contact
     * @param $role String roles du contact
     * @param $sorting String ordre de tri
     * @return mixed liste des contact corespondant aux critères
     */
	public function getListContacts($db, $Name, $nickName, $organism, $mail, $portable, $fixe, $adress, $role, $sorting)
    {
        
        /* mise en forme des variables de la requete:
         *    - si la variable est vide alors elle est instancié par le caractère % ou est remplacé par IS NULL
         */
        if($Name == "" || $Name == " " ){
            $Name = '%';
        }else{
            $Name = $Name.'%';
        } 
        if($nickName == "" || $nickName == " " ){
            $nickName = '%';
        }else{
            $nickName = $nickName.'%';
        } 
        if($organism == "" || $organism == " " ){
            $organism = '%';
        }else{
            $organism = $organism.'%';
        } 
        if($mail == "" || $mail == " " ){
            $mail = '%';
            $reqMail = '(acpa_personne.lib_mail_personne LIKE :mel OR acpa_personne.lib_mail_personne IS NULL)';
        }else{
            $mail = $mail.'%';
            $reqMail = 'acpa_personne.lib_mail_personne LIKE :mel';
        } 
        if($portable == "" || $portable == " " ){
            $portable = '%';
            $reqPortable = '(acpa_personne.tel_mobile LIKE :mobile OR acpa_personne.tel_mobile IS NULL)';
        }else{
            $portable = $portable.'%';
            $reqPortable = 'acpa_personne.tel_mobile LIKE :mobile';
        }
        if($fixe == "" || $fixe == " " ){
            $fixe = '%';
            $reqFixe = '(acpa_personne.tel_fixe LIKE :fixe OR acpa_personne.tel_fixe IS NULL)';
        }else{
            $fixe = $fixe.'%';            
            $reqFixe = 'acpa_personne.tel_fixe LIKE :fixe';
        }
        if($adress == "" || $adress == " " ){
            $adress = '%';
            $reqAdress = '(concat(acpa_personne.lib_adresse, " ", acpa_personne.num_code_postal, " ",acpa_personne.lib_ville) Like :adress OR concat(acpa_personne.lib_adresse, " ", acpa_personne.num_code_postal, " ",acpa_personne.lib_ville) IS NULL)';
        }else{
            $adress = '%'.$adress.'%';
            $reqAdress = 'concat(acpa_personne.lib_adresse, " ", acpa_personne.num_code_postal, " ",acpa_personne.lib_ville) Like :adress';
        }
        if($role == "" || $role == " " ){
            $role = '%';
            $reqRole = '(acpa_role.id_role LIKE :role OR acpa_role.id_role IS NULL)';
        }else{
            $role = $role.'%';
            $reqRole = 'acpa_role.id_role LIKE :role';
        }
        
        // création du tri de la requete en fonction de la variable $sorting
        switch ($sorting){
            case 'orderNameAsc':
                $sort = 'acpa_personne.lib_nom DESC';
                break;
            case 'orderNameDesc':                
                $sort = 'acpa_personne.lib_nom ASC';
                break;
            case 'orderFirstNameAsc':
                $sort = 'acpa_personne.lib_prenom DESC';
                break;
            case 'orderFirstNameDesc':
                $sort = 'acpa_personne.lib_prenom ASC';
                break;                
            case 'orderOrganismAsc':
                $sort = 'acpa_organisation.lib_organisation DESC';
                break;
            case 'orderOrganismDesc':
                $sort = 'acpa_organisation.lib_organisation ASC';
                break;
            case 'orderMailAsc':
                $sort = 'acpa_personne.lib_mail_personne DESC';
                break;
            case 'orderMailDesc':
                $sort = 'acpa_personne.lib_mail_personne ASC';
                break;
            case 'orderSmartPhoneAsc':
                $sort = 'acpa_personne.tel_mobile DESC';
                break;
            case 'orderSmartPhoneDesc':
                $sort = 'acpa_personne.tel_mobile ASC';
                break;  
            case 'orderFixePhoneAsc':
                $sort = 'acpa_activite.tel_fixe DESC';
                break;
            case 'orderFixePhoneDesc':
                $sort = 'acpa_activite.tel_fixe ASC';
                break;
            case 'orderAdressDesc':
                $sort = 'acpa_personne.lib_adresse DESC';
                break;
            case 'orderSmartPhoneDesc':
                $sort = 'acpa_personne.lib_adresse DESC';
                break;               
            default:
                $sort = 'acpa_personne.lib_nom DESC';
                break;
                
        }

        $listContact = $db->prepare('
            SELECT acpa_personne.*, acpa_role.*, acpa_organisation.*, concat(acpa_personne.lib_adresse, "-sautDeLigne-", acpa_personne.num_code_postal, ", ",acpa_personne.lib_ville) AS adresse_complete
            FROM acpa_personne 
            LEFT JOIN acpa_organisation ON acpa_organisation.id_organisation = acpa_personne.id_organisation 
            LEFT JOIN acpa_role_personne ON acpa_role_personne.id_personne = acpa_personne.id_personne
            LEFT JOIN acpa_role ON acpa_role_personne.id_role = acpa_role.id_role
            WHERE acpa_organisation.lib_organisation LIKE :organisation
            AND acpa_personne.lib_nom LIKE :nom
            AND acpa_personne.lib_prenom LIKE :prenom
            AND '.$reqMail.'
            AND '.$reqPortable.'
            AND '.$reqFixe.'
            AND '.$reqRole.'
            AND '.$reqAdress.'
            ORDER BY ' .$sort);

        $listContact->execute(array(
            'nom' => $Name, 
            'prenom' => $nickName, 
            'organisation' => $organism, 
            'mel' => $mail, 
            'mobile' => $portable, 
            'fixe' => $fixe,
            'adress' => $adress,
            'role' => $role));
       		
        return $listContact;
        $listContact->closeCursor();
	}


    /**
     * requete de recuperation des informations lies a une personne, son adhesion, sa categories d'age, ses saisons, son type de licence,
     * ses activites son role, le statut de son adhesion, et ses documents en fonction:
     * - du nom
     * - du prenom
     * - du mail
     * - de la saison
     * @param $db object connexion a la base de données
     * @param $nomAdherent String nom de l'adhérent
     * @param $prenomAdherent String Prénom de l'adhérent
     * @param $mailAdherent String Adresse mail de l'adhérent
     * @param $saison int Identifiant de la saison
     * @return mixed objet contenant les informations cherchés
     */
	public function getAdherent($db, $nomAdherent, $prenomAdherent, $mailAdherent, $saison)
	{
		//$db = $this->dbconnect();
		$adherent = $db->prepare('
			SELECT acpa_personne.*, 
            acpa_adhesion.*, 
            acpa_adhesion.id_adhesion AS id_adhesion_1, 
            acpa_adhesion.mnt_tarif_licence AS tarif_adhesion,
            acpa_categorie_age.*, 
            acpa_saison.*, 
            acpa_type_licence.*, 
            acpa_activite_adh.*, 
            acpa_role_adhesion.*,
            acpa_statut.*, 
            acpa_document.*, 
            GROUP_CONCAT(DISTINCT acpa_role.lib_role) AS list_role, 
            GROUP_CONCAT(DISTINCT acpa_activite.lib_activite) AS list_activite
            FROM acpa_personne 
                LEFT JOIN acpa_adhesion ON acpa_adhesion.id_personne = acpa_personne.id_personne 
                LEFT JOIN acpa_categorie_age ON acpa_adhesion.id_categorie_age = acpa_categorie_age.id_categorie_age 
                LEFT JOIN acpa_saison ON acpa_adhesion.id_saison = acpa_saison.id_saison 
                LEFT JOIN acpa_type_licence ON acpa_adhesion.id_type_licence = acpa_type_licence.id_type_licence 
                LEFT JOIN acpa_activite_adh ON acpa_activite_adh.id_adhesion = acpa_adhesion.id_adhesion  
	            LEFT JOIN acpa_statut ON acpa_adhesion.id_statut = acpa_statut.id_statut 
                LEFT JOIN acpa_activite ON acpa_activite_adh.id_activite = acpa_activite.id_activite 
                LEFT JOIN acpa_role_adhesion ON acpa_role_adhesion.id_adhesion = acpa_adhesion.id_adhesion 
                LEFT JOIN acpa_role ON acpa_role_adhesion.id_role = acpa_role.id_role
                LEFT JOIN acpa_document ON acpa_document.id_adhesion = acpa_adhesion.id_adhesion
            WHERE acpa_saison.id_saison Like :saison
            AND acpa_personne.lib_nom LIKE :nom
            AND acpa_personne.lib_prenom LIKE :prenom
            AND acpa_personne.lib_mail_personne LIKE :mail
            GROUP BY acpa_personne.id_personne
            ');
	
		$adherent->execute(array(
            'saison' => $saison,
			'nom' => $nomAdherent, 
			'prenom' => $prenomAdherent,
			'mail' => $mailAdherent));
		
		return $adherent;	
		$adherent->closeCursor();
	}


    /**
     * requete de recuperation des informations lies a une personne, son adhesion, sa categorie d'age,
     * ses saisons, son type de licence son activité sportive son role, le statut de son adhesion et ses documents en fonction:
     * - de l'id personne
     * - de l'id Adhesion
     * @param $db object connexion a la base de données
     * @param $idAdherent int id de l'adhérent
     * @param $idAdhesion int id de l'adhésion
     * @return mixed objet contenant les informations cherchées
     */
	public function getAdherentById($db, $idAdherent, $idAdhesion)
	{

		$adherent = $db->prepare('
			SELECT acpa_personne.*, 
            acpa_adhesion.*, 
            acpa_adhesion.id_adhesion AS id_adhesion_1, 
            acpa_adhesion.mnt_tarif_licence AS tarif_adhesion,
            acpa_categorie_age.*, 
            acpa_saison.*, 
            acpa_type_licence.*, 
            acpa_activite_adh.*, 
            acpa_role_adhesion.*,
            acpa_statut.*,
            acpa_document.*, 
            GROUP_CONCAT(DISTINCT acpa_role.lib_role) AS list_role, 
            GROUP_CONCAT(DISTINCT acpa_activite.lib_activite) AS list_activite
            FROM acpa_personne 
                LEFT JOIN acpa_adhesion ON acpa_adhesion.id_personne = acpa_personne.id_personne 
                LEFT JOIN acpa_categorie_age ON acpa_adhesion.id_categorie_age = acpa_categorie_age.id_categorie_age 
                LEFT JOIN acpa_saison ON acpa_adhesion.id_saison = acpa_saison.id_saison 
                LEFT JOIN acpa_type_licence ON acpa_adhesion.id_type_licence = acpa_type_licence.id_type_licence 
                LEFT JOIN acpa_activite_adh ON acpa_activite_adh.id_adhesion = acpa_adhesion.id_adhesion  
	            LEFT JOIN acpa_statut ON acpa_adhesion.id_statut = acpa_statut.id_statut 
                LEFT JOIN acpa_activite ON acpa_activite_adh.id_activite = acpa_activite.id_activite 
                LEFT JOIN acpa_role_adhesion ON acpa_role_adhesion.id_adhesion = acpa_adhesion.id_adhesion 
                LEFT JOIN acpa_role ON acpa_role_adhesion.id_role = acpa_role.id_role
                LEFT JOIN acpa_document ON acpa_document.id_adhesion = acpa_adhesion.id_adhesion
            WHERE acpa_personne.id_personne LIKE :idAdherent
            AND acpa_adhesion.id_adhesion LIKE :idAdhesion
            GROUP BY acpa_personne.id_personne
            ');
	
		$adherent->execute(array(
            'idAdherent' => $idAdherent,
			'idAdhesion' => $idAdhesion));
		
		return $adherent;	
		$adherent->closeCursor();
	}


    /**
     * requete de recuperation des informations d'une personnes grace à sont ID
     * @param $db object connexion a la base de données
     * @param $idPersonne int identifiant de la personnes
     * @return mixed objet contenant les informations cherchées
     */
    public function getPersonnByID($db, $idPersonne)
    {
        $idPersonnes = $idPersonne;

		$people = $db->prepare('
			SELECT acpa_personne.* 
            FROM acpa_personne 
            WHERE acpa_personne.id_personne LIKE :idpersonne
            ');
	
		$people->execute(array(
            'idpersonne' => $idPersonnes));
		
		return $people;	
		$people->closeCursor();
    }


    /*
     * note du dev:
     * je prefère recuperer le login de la bdd et le comparer au login entré par l'utilisateur plutot que de verifier si le couple
     * login mot de passe existe, cela me permet de reduire le risque d'injection
     */
    /**
     * requete de recuperation du mot de passe d'une personnes grace à sont login
     * @param $db object connexion a la base de données
     * @param $login String login d'une personnes
     * @return mixed object contenant le login et le mot de passe
     */
	public function checkIdentifiant($db, $login)
	{

		$ligneIdentifiant = $db->prepare(
						'SELECT acpa_personne.login, 
                        acpa_personne.pwd
						FROM acpa_personne
						WHERE acpa_personne.login LIKE :login
                        ORDER BY acpa_personne.lib_nom DESC');
		$ligneIdentifiant->execute(array('login' => $login));
		return $ligneIdentifiant;
		$ligneIdentifiant->closeCursor();
	}


    /**
     * requete de recuperation des informations d'un document grace à sont ID
     * @param $db object connexion a la base de données
     * @param $idDocument
     * @return mixed
     */
    public function seeDoc($db, $idDocument)
    {

		$document = $db->prepare(
						'SELECT * 
						FROM acpa_document
						WHERE id_document = :idDoc');
		$document->execute(array('idDoc' => $idDocument));
		return $document;
		$document->closeCursor();
    }


    /**
     * requete de recuperation des identifiant des adhesion lié a une personnes
     * @param $db object connexion a la base de données
     * @param $idpersonne identifiant de la personnes
     * @return mixed
     */
    public function idAdhesionByIdPersonne($db, $idpersonne)
    {
        $listIdAdhesion = $db->prepare("SELECT acpa_adhesion.id_adhesion
                                        FROM acpa_adhesion
                                        WHERE acpa_adhesion.id_personne = :idpersonne");
        $listIdAdhesion->execute(array('idpersonne' => $idpersonne));
        return $listIdAdhesion;
        $listIdAdhesion->closeCursor();
    }
}


/**
 * class contenant toute les fonctions de requetes d'ecriture dans la bdd
 */
class SetterAcpa extends Acpa
{

    /**
     * requete d'insertion d'une entrée (adherent) dans la table personnes
     * @param $db object connexion a la base de données
     * @param array $dataPersonne contenant les informations lié a une personne
     */
	public function addNewPersonne($db, array $dataPersonne)
	{
        $newPersonne = $db->prepare("INSERT INTO acpa_personne (
                                        num_licence_SIFFA, 
                                        dt_licence, 
                                        lib_nom, 
                                        lib_prenom, 
                                        lib_sexe, 
                                        dt_naissance, 
                                        lib_nationalite, 
                                        lib_adresse, 
                                        num_code_postal, 
                                        lib_ville, 
                                        lib_mail_personne, 
                                        tel_mobile, 
                                        tel_fixe,
                                        login, 
                                        id_parent,
                                        id_organisation) 
                                    VALUES (
                                        :numLicenceSIFFA, 
                                        :dtLicence, 
                                        :libNom, 
                                        :libPrenom, 
                                        :libSexe, 
                                        :dtNaissance, 
                                        :libNationalite, 
                                        :libAdresse, 
                                        :numCodePostal, 
                                        :libVille, 
                                        :libMailPersonne, 
                                        :telMobile, 
                                        :telFixe,
                                        :login,  
                                        :idParent,
                                        :idOrganisation)");
        $newPersonne->execute($dataPersonne);
       
        
        $newPersonne->closeCursor();
           
	}


    /**
     * requete de mise a jour d'une entrée (adherent) dans la table personnes
     * @param $dbobject connexion a la base de données
     * @param array $dataPersonne2 contenant des informations lié a une personne
     */
    public function updateAdherent($db, array $dataPersonne2)
	{
		$updateAdherent = $db->prepare("UPDATE acpa_personne 
								SET num_licence_SIFFA = :numLicenceSIFFA,  
								lib_sexe = :libSexe, 					 
								lib_nationalite = :libNationalite, 
								lib_adresse = :libAdresse, 
								num_code_postal = :numCodePostal, 
								lib_ville = :libVille, 
								lib_mail_personne = :libMailPersonne, 
								tel_mobile = :telMobile,
								tel_fixe = :telFixe, 
								login = :login, 
								id_parent = :idParent
								WHERE acpa_personne.id_personne = :idAdherent");
        $updateAdherent->execute($dataPersonne2);
        $updateAdherent->closeCursor();
           
	}


    /**
     * requete de mise a jour d'une entrée (adherent) dans la table personnes en utilisant l'ID
     * @param $db object connexion a la base de données
     * @param array $dataPersonne contenant les informations lié a une personne
     */
    public function updateAdherentById($db, array $dataPersonne)
	{
		$updateAdherentById = $db->prepare("UPDATE acpa_personne 
								SET num_licence_SIFFA = :numSIFFA,
                                dt_licence = :dteLicence,
                                lib_nom = :nameAdherent,
                                lib_prenom = :firstNameAdherent,
								lib_sexe = :sexeAdherent,
                                dt_naissance = :birthdayAdherent,		 
								lib_nationalite = :country, 
								lib_adresse = :adress, 
								num_code_postal = :zipCode, 
								lib_ville = :city, 
								lib_mail_personne = :mail, 
								tel_mobile = :mobilePhone,
								tel_fixe = :fixePhone, 
								login = :idConnexion,
                                dtm_connexion = :dteLastConnect,
                                id_parent = :idParent
								WHERE acpa_personne.id_personne = :idPersonne");
        $updateAdherentById->execute($dataPersonne);
        $updateAdherentById->closeCursor();
           
	}


    /**
     * requete de mise a jour d'une entrée (contact) dans la table personnes en utilisant l'ID
     * @param $db object connexion a la base de données
     * @param array $dataContact données lié a un contact
     */
    public function updateContactById($db, array $dataContact)
	{
		$updateAdherentById = $db->prepare("UPDATE acpa_personne 
								SET lib_nom = :nameContact,
                                lib_prenom = :firstNameContact,
								lib_adresse = :adress, 
								num_code_postal = :zipCode, 
								lib_ville = :city, 
								lib_mail_personne = :mail, 
								tel_mobile = :mobilePhone,
								tel_fixe = :fixePhone, 
                                id_organisation = :idOrganisation
								WHERE acpa_personne.id_personne = :idPersonne");
        $updateAdherentById->execute($dataContact);
        $updateAdherentById->closeCursor();
           
	}


    /**
     * requete de mise a jour des login et password dans la table personnes en utilisant l'ID
     * @param $db object connexion a la base de données
     * @param $idPersonne int identifiant de la personne
     * @param $login String login de la personne
     * @param $pass String mot de passe de la personne
     */
    public function addPassPersonne($db, $idPersonne, $login, $pass)
	{
		$passPersonne = $db->prepare("UPDATE acpa_personne 
								SET login = :login,
                                pwd = :password
								WHERE acpa_personne.id_personne = :idPersonne");
        $passPersonne->execute(array('login' => $login,
                                    'password' => $pass,
                                    'idPersonne' => $idPersonne));
        $passPersonne->closeCursor();
           
	}


    /**
     * requete d'insertion d'une entrée dans la table adhesion
     * @param $db object connexion a la base de données
     * @param array $dataAdhesion données necessaire à la création d'une adhésion
     */
	public function addNewAdhesion($db, array $dataAdhesion)
	{
		$newAdhesion = $db->prepare("INSERT INTO acpa_adhesion (
									lib_nom_prenom_responsable_legal, 
									lib_type_responsable_legal, 
									lib_identite_enfant, 
									yn_prelevement, 
									yn_hospitalisation, 
									yn_reglement_interieur, 
									yn_assurance, 
									yn_droit_image, 
									yn_cnil, 
                                    dtm_validation,
									dtm_adhesion, 
									lib_identifiant_gr_adhesion, 
									id_personne, 
									id_saison, 
									id_statut,
									id_categorie_age,
									id_type_licence,
                                    mnt_tarif_licence) 
								VALUES (
									:identiteRepLegal,
									:typeRepLegal,
									:identiteEnfant,
									:prelevementSanguin,
									:hospitalisation,
									:reglementInterieur,
									:assurance,
									:droitImage,
									:cnil,
									:dateValidation,
									:dateAdhesion,
									:idGrpAdhesion,
									:idPersonne,
									:idSaison,
									:idStatut,
									:idCatAge,
									:idTypLic,
                                    :mntTarifLicence)");
			
        $newAdhesion->execute($dataAdhesion);
        
		$newAdhesion->closeCursor();
	}


    /**
     * requete de mise a jour d'une entrée dans la table adhesion en utilisant l'ID
     * @param $db object connexion a la base de données
     * @param array $dataAdhesion données necessaire à la modification d'une adhésion
     */
    public function updateAdhesionById($db, array $dataAdhesion)
	{
		$updateAdhesionById = $db->prepare("UPDATE acpa_adhesion 
								SET lib_nom_prenom_responsable_legal =  :respLegal,
                                lib_type_responsable_legal =  :typeRespLegal,
                                lib_identite_enfant =  :identityYoungAdherent,
                                yn_prelevement =  :authSample,
                                yn_hospitalisation =  :authHospital,
                                yn_reglement_interieur =  :interiorRule,
                                yn_assurance =  :assurance,
                                yn_droit_image =  :authProfil,
                                yn_cnil =  :authRGPD,
                                dtm_validation =  :datePaiement,
                                dtm_adhesion =  :dateAdhesion,
                                lib_identifiant_gr_adhesion =  :grpAdherent,
                                mnt_tarif_licence =  :mntAdhesion,
                                id_personne =  :idPersonne,
                                id_saison =  :idSaison,
                                id_statut =  :statusAdhesion,
                                id_categorie_age =  :catAge,
                                id_type_licence =  :typeLic
								WHERE acpa_adhesion.id_adhesion = :idAdhesion");
        $updateAdhesionById->execute($dataAdhesion);
        $updateAdhesionById->closeCursor();
           
	}


    /**
     * requete d'insertion d'une entrée dans la table activites/adhesion
     * permet de liéer une adhesion aux differentes activités pratiqué par l'adherent
     * @param $db object connexion a la base de données
     * @param $idAdhesion int identifiant de l'adhesion
     * @param $idActiviteAdhesion int identifiant de l'activité
     */
	public function addNewActivityAdhesion($db, $idAdhesion, $idActiviteAdhesion)
	{
		$newActivityAdhesion = $db->prepare("INSERT INTO acpa_activite_adh (
													id_adhesion,
													id_activite)
												VALUES (
													:idadhesion,
													:idactivite)");
		
        $newActivityAdhesion->execute(array('idadhesion' => $idAdhesion,
												'idactivite' => $idActiviteAdhesion));
		
        
		$newActivityAdhesion->closeCursor();
	}


    /**
     * requete d'insertion d'une entrée dans la table document
     * @param $db object connexion a la base de données
     * @param array $dataimage données de l'image
     */
	public function addNewDoc($db, array $dataimage)
	{
		$newDoc = $db->prepare("INSERT INTO acpa_document (
									lib_nom_document, 
									bin_fichier, 
									dt_validite, 
									dtm_remise, 
									id_adhesion, 
									id_type_document, 
									id_personne_doc) 
								VALUES (
									:libDocument, 
									:cheminFichier, 
									:dateValidite, 
									:dateRemise, 
									:idAdhesion, 
									:idTypeDocument, 
									:idPersonne)");
			
        $newDoc->execute($dataimage);
			
		$newDoc->closeCursor();
	}


    /**
     * requete de suppression des documents lié a une adhesion dans la table document
     * @param $db object connexion a la base de données
     * @param $idAdhesion
     */
	public function removeDocAdhesion($db, $idAdhesion)
	{
		$deleteDocumentAdhesion = $db->prepare(
						'DELETE FROM `acpa_document` 
                        WHERE acpa_document.id_adhesion = :idAdhesion');
        
        $deleteDocumentAdhesion->execute(array('idAdhesion' => $idAdhesion));
			
		$deleteDocumentAdhesion->closeCursor();
	}


    /**
     * requete de suppression des documents lié a un adherent dans la table document
     * @param $db object connexion a la base de données
     * @param $idAherent
     */
	public function removeDocAdherent($db, $idAherent)
	{
		$deleteDocumentAdherent = $db->prepare(
						'DELETE FROM `acpa_document` 
                        WHERE acpa_document.id_personne_doc = :idAdherent');
        
        $deleteDocumentAdherent->execute(array('idAdherent' => $idAherent));
			
		$deleteDocumentAdherent->closeCursor();
	}


    /**
     * requete de mise a jour du document dans la table document
     * @param $db object connexion a la base de données
     * @param $idDoc int identifiant du document
     * @param $certif blob document
     */
    public function updateCertifByIdAdhesion($db, $idDoc, $certif)
	{

		$updateCertificat = $db->prepare("UPDATE acpa_document SET bin_fichier = ? WHERE acpa_document.id_document = ?");
    
        $updateCertificat->bindParam(1, $certif, PDO::PARAM_LOB);
        $updateCertificat->bindParam(2, $idDoc);

        $updateCertificat->execute();
        $updateCertificat->closeCursor();
           
	}


    /**
     * requete d'insertion d'une entrée dans la table role/adhesion
     * @param $db object connexion a la base de données
     * @param $idAdhesion int identifiant de l'adhesion
     */
	public function addRoleNewAdhesion($db, $idAdhesion)
	{

		$newRole = $db->prepare("INSERT INTO acpa_role_adhesion (
									id_adhesion, 
									id_role) 
								VALUES (
									:idAdhesion, 
									'101')");
			
        $newRole->execute(array('idAdhesion' => $idAdhesion));
			
		$newRole->closeCursor();
	}


    /**
     * requete de suppression de toutes les entrées liée a une adhesion dans la table activites/adhesion en utilisant l'ID de l'adhesion
     * @param $db object connexion a la base de données
     * @param $idAdhesion int id de l'adhesion
     */
    public function removeAllActivity($db, $idAdhesion)
	{

		$removeAllActivity = $db->prepare("DELETE FROM acpa_activite_adh 
                                        WHERE id_adhesion = :idAdhesion");
			
        $removeAllActivity->execute(array('idAdhesion' => $idAdhesion));
			
		$removeAllActivity->closeCursor();
	}

    /**
     * requete d'insertion d'une entrée dans la table activity/adhesion
     * @param $db object connexion a la base de données
     * @param $idAdhesion int id de l'activite
     * @param $idActivity int id de l'adhesion
     */
    public function addOneActivity($db, $idAdhesion, $idActivity)
	{

		$addAnActivity = $db->prepare("INSERT INTO acpa_activite_adh 
                                            (id_adhesion, id_activite) 
                                            VALUES (:idAdhesion, :idActivity)");
			
        $addAnActivity->execute(array('idAdhesion' => $idAdhesion,
                                         'idActivity' => $idActivity));
			
		$addAnActivity->closeCursor();
	}


	/**
     * requete de suppression de toutes les entrées liée a une adhesion dans la table role/adhesion en utilisant l'ID de l'adhesion
     * @param $db object connexion a la base de données
     * @param $idAdhesion int id de l'adhesion
     */
    public function removeAllRolesAdhesion($db, $idAdhesion)
	{

		$removeAllRoles = $db->prepare("DELETE FROM acpa_role_adhesion
                                        WHERE id_adhesion = :idAdhesion");
			
        $removeAllRoles->execute(array('idAdhesion' => $idAdhesion));
			
		$removeAllRoles->closeCursor();
	}

    /**
     * requete d'insertion d'une entrée dans la table role/adhesion
     * permet de lié une adhesion au role de l'adherent (sportif, entraineur....)
     * @param $db object connexion a la base de données
     * @param $idAdhesion int id de l'adhesion
     * @param $idRole int id du role a attribuer
     */
    public function addOneRoleAdhesion($db, $idAdhesion, $idRole)
	{
		$addARole = $db->prepare("INSERT INTO acpa_role_adhesion 
                                            (id_adhesion, id_role) 
                                            VALUES (:idAdhesion, :idRole)");
			
        $addARole->execute(array('idAdhesion' => $idAdhesion,
                                         'idRole' => $idRole));
			
		$addARole->closeCursor();
	}

    /**
     * requete de suppression de toutes les entrées liée a un aherent dans la table role/personne en utilisant l'ID de la personne pour les adherents
     * @param $db object connexion a la base de données
     * @param $idPersonne int id de la personne
     */
    public function removeAllRolesPersonne($db, $idPersonne)
	{
		$removeRolesPersonne = $db->prepare("DELETE FROM acpa_role_personne
                                        WHERE id_personne = :idPersonne");
			
        $removeRolesPersonne->execute(array('idPersonne' => $idPersonne));
			
		$removeRolesPersonne->closeCursor();
	}


    /**
     * requete d'insertion d'une entrée dans la table role/personne
     * permet de lie un contact  a un role
     * @param $db object connexion a la base de données
     * @param $idContact int id du contact
     * @param $roleContacts int id du role a attribuer
     */
    public function addOneRolePersonne($db, $idContact, $roleContacts)
	{
		$addARolePersonne = $db->prepare("INSERT INTO acpa_role_personne 
                                            (id_role, id_personne) 
                                            VALUES (:idRole, :idPersonne)");
			
        $addARolePersonne->execute(array('idRole' => $roleContacts,
                                         'idPersonne' => $idContact
                                         ));
			
		$addARolePersonne->closeCursor();
	}


    /**
     * requete de suppression de toutes les entrées liée a un adherent dans la table groupe ged/personne en utilisant l'ID de la personne pour les adherents
     * @param $db object connexion a la base de données
     * @param $idPersonne
     */
    public function removeAllGroupePersonne($db, $idPersonne)
	{
		$deleteGroupPersonne = $db->prepare(
                        'DELETE FROM `acpa_appartient_gr` 
                        WHERE acpa_appartient_gr.id_personne = :idPersonne');
        $deleteGroupPersonne->execute(array('idPersonne' => $idPersonne));
		$deleteGroupPersonne->closeCursor();
	}


    /**
     * requete d'insertion d'une entrée dans la table organisation
     * @param $db object connexion a la base de données
     * @param $libOrganisation String nom de l'organisation
     */
    public function addOneOrganisation($db, $libOrganisation)
	{
		$addAnOrganism = $db->prepare("INSERT INTO acpa_organisation 
                                            (lib_organisation) 
                                            VALUES (:libOrganisation)");
			
        $addAnOrganism->execute(array('libOrganisation' => $libOrganisation));
			
		$addAnOrganism->closeCursor();
	}


    /**
     * requete de suppression de toutes les entrées liée a une adhesion dans la table role/personne en utilisant l'ID de la personne pour les contacts
     * @param $db object connexion a la base de données
     * @param $idPersonne int identifiant de la personne
     */
    public function removeAllRolesContact($db, $idPersonne)
	{
		$removeRoleContact = $db->prepare("DELETE FROM acpa_role_personne
                                        WHERE id_personne = :idPersonne");
			
        $removeRoleContact->execute(array('idPersonne' => $idPersonne));
			
		$removeRoleContact->closeCursor();
	}


    /**
     * requete de suppression d'une entrées la table personne en utilisant l'ID de la personne
     * @param $db object connexion a la base de données
     * @param $idPersonne int identifiant de la personne
     */
    public function deleteTheLineContact($db, $idPersonne)
	{
		$deleteLineContact = $db->prepare("DELETE FROM acpa_personne
                                        WHERE id_personne = :idPersonne");
			
        $deleteLineContact->execute(array('idPersonne' => $idPersonne));
			
		$deleteLineContact->closeCursor();
	}


    /**
     * requete de suppression d'une entrées la table saison en utilisant l'ID de la saison
     * @param $db object connexion a la base de données
     * @param $idSeason int identifiant de la saison
     */
    public function deleteTheLineSeason($db, $idSeason)
	{
		$deleteLineSeason = $db->prepare("DELETE FROM acpa_saison
                                        WHERE id_saison = :idSaison");
		
        $deleteLineSeason->execute(array('idSaison' => $idSeason));
        			
		$deleteLineSeason->closeCursor();
	}


    /**
     * requete de mise 0 de la colonne yn_saison_ouverte dans la table saison pour la saison a 1
     * permet de mettre a zero les saison en cours
     * @param $db object connexion a la base de données
     */
     public function resetSeasons($db)
	{
		$resetSeason = $db->query("UPDATE acpa_saison 
                                SET yn_saison_ouverte = '0' 
                                WHERE acpa_saison.yn_saison_ouverte = '1'");
        			
		$resetSeason->closeCursor();
	}


    /**
     * requete de mise a jour de la colonne yn_saison_ouverte dans la table saison en utilisant l'ID de la saison
     * permet de definir la saison en cours
     * @param $db object connexion a la base de données
     * @param $idSeason int identifiant de la saison
     */
    public function activateSeason($db, $idSeason)
	{
		$activSeason = $db->prepare("UPDATE acpa_saison 
                                        SET yn_saison_ouverte = '1' 
                                        WHERE acpa_saison.id_saison = :idSaison");
		
        $activSeason->execute(array('idSaison' => $idSeason));
        			
		$activSeason->closeCursor();
	}

    /**
     * requete d'insertion d'une entrée dans la table saison
     * @param $db object connexion a la base de données
     * @param $libSaison String libellé de la saison (exemple Saison 2018-2019)
     */
    public function CreateSeason($db, $libSaison)
	{
		$creatSeason = $db->prepare("INSERT INTO acpa_saison
                                    (`id_saison`, `lib_saison`, `yn_saison_ouverte`) 
                                    VALUES (NULL, :libsaison, '0')");
		
        $creatSeason->execute(array('libsaison' => $libSaison));
        			
		$creatSeason->closeCursor();
	}

    /**
     * requete d'insertion d'une entrée dans la table categorie
     * @param $db object connexion a la base de données
     * @param $nomCategory String nom de la catégorie
     * @param $libCategory String libellé de la categorie
     * @param $numCategory int numero d'ordre de la catégorie
     * @param $condCategory int age maximal de la categorie
     */
    public function CreateCategory($db, $nomCategory, $libCategory, $numCategory, $condCategory)
	{

		$creatActivity = $db->prepare("INSERT INTO acpa_categorie_age
                                    (id_categorie_age, lib_court_age, lib_categorie_age, num_tri_categorie_age, conditions) 
                                    VALUES (NULL, :libCatAge, :nomCatAge, :numCatAge, :condCatAge)");
		
        $creatActivity->execute(array('libCatAge' => $libCategory,
                                      'nomCatAge' => $nomCategory,
                                      'numCatAge' => $numCategory,
                                      'condCatAge' => $condCategory,
                                     ));
        			
		$creatActivity->closeCursor();
	}


    /**
     * requete de suppression d'une entrées la table categorie en utilisant l'ID de la categorie
     * @param $db object connexion a la base de données
     * @param $idCategory int identifiant de la categorie
     */
    public function deleteTheLineCategory($db, $idCategory)
	{
		$deleteLineCategory = $db->prepare("DELETE FROM acpa_categorie_age
                                        WHERE id_categorie_age = :idCategorie");
		
        $deleteLineCategory->execute(array('idCategorie' => $idCategory));
        			
		$deleteLineCategory->closeCursor();
	}

    /**
     * requete de suppression d'une entrées la table adhesion en utilisant l'ID de l'adhesion
     * @param $db object connexion a la base de données
     * @param $idAdhesion int identifiant de l'adhesion
     */
    public function deleteTheLineAdhesion($db, $idAdhesion)
	{
		$deleteAdhesion = $db->prepare(
						'DELETE FROM `acpa_adhesion` 
                        WHERE acpa_adhesion.id_adhesion = :idAdhesion');        
        $deleteAdhesion->execute(array('idAdhesion' => $idAdhesion));
        			
		$deleteAdhesion->closeCursor();
	}


    /**
     * requete de mise a jour d'une entrées la table categorie en utilisant l'ID de la categorie
     * @param $db object connexion a la base de données
     * @param $idCategory int identifiant de la catégorie
     * @param $nomCategory String nom de la catégorie
     * @param $libCategory String libellé de la categorie
     * @param $numCategory int numero d'ordre de la catégorie
     * @param $condCategory int age maximal de la categorie
     */
    public function modifyCategory($db, $idCategory, $nomCategory, $libCategory, $numCategory, $condCategory)
	{
		$activSeason = $db->prepare("UPDATE acpa_categorie_age 
                                        SET lib_court_age = :libCatAge,
                                        lib_categorie_age = :nomCatAge,
                                        num_tri_categorie_age = :numCatAge,
                                        conditions = :condCatAge
                                        WHERE acpa_categorie_age.id_categorie_age = :idCatAge");
		
        $activSeason->execute(array('libCatAge' => $libCategory,
                                   'nomCatAge' => $nomCategory,
                                   'numCatAge' => $numCategory,
                                   'condCatAge' => $condCategory,
                                   'idCatAge' => $idCategory));
        			
		$activSeason->closeCursor();
	}


    /**
     * requete de suppression d'une entrées la table Moyen de paiement en utilisant l'ID du moyen de paiement
     * @param $db object connexion a la base de données
     * @param $idPayMethod int identifiant du moyen de paiement
     */
    public function deleteTheLinePayMethod($db, $idPayMethod)
	{
		$deleteLinePaiMethode = $db->prepare("DELETE FROM acpa_moyen_paiement
                                        WHERE id_moyen_paiement = :idMoyenPaiement");
		
        $deleteLinePaiMethode->execute(array('idMoyenPaiement' => $idPayMethod));
        			
		$deleteLinePaiMethode->closeCursor();
	}


    /**
     * requete d'insertion d'une entrée dans la table methode de paiement
     * permet d'ajouter un moyen de paiement et de definir si il est accepter ou non
     * @param $db object connexion a la base de données
     * @param $nomPayMethod String nom du nouveau moyen de paiement
     * @param $isAccept boolean true si le moyen de paiement est accepter
     */
    public function CreatePayMethod($db, $nomPayMethod, $isAccept)
	{
		$createPayMethod = $db->prepare("INSERT INTO acpa_moyen_paiement
                                    (id_moyen_paiement, lib_moyen_paiement, yn_accepte) 
                                    VALUES (NULL, :libMoyenPaiement, :ynAccepte)");
		
        $createPayMethod->execute(array('libMoyenPaiement' => $nomPayMethod,
                                      'ynAccepte' => $isAccept
                                     ));
        			
		$createPayMethod->closeCursor();
	}


    /**
     * requete de mise a jour d'une entrées la table moyen de payement en utilisant l'ID du moyen de paiement
     * permet de definir si un moyen de payement est accepté ou pas
     * @param $db object connexion a la base de données
     * @param $idPayMethod int id du moyen de payement
     * @param $isAccept boolean true si le moyen de payement est accepté
     */
    public function swithAcceptPayMethod($db, $idPayMethod, $isAccept)
	{
		$switchPayMethod = $db->prepare("UPDATE acpa_moyen_paiement 
                                        SET yn_accepte = :ynAccepte 
                                        WHERE acpa_moyen_paiement.id_moyen_paiement = :idMoyenPaiement");
		
        $switchPayMethod->execute(array('ynAccepte' => $isAccept,
                                   'idMoyenPaiement' => $idPayMethod));
        			
		$switchPayMethod->closeCursor();
	}


    /**
     * requete de mise a jour de la colonne categorie d'age d'une entrées de la table adhesion en utilisant l'ID de l'adhesion et l'ID de la categorie d'age
     * @param $db object connexion a la base de données
     * @param $idCategory int id de la catégorie d'age
     * @param $idAdhesion int id de l'adhesion
     */
    public function updateCatAgeAdhesionById($db, $idCategory, $idAdhesion)
	{
		$updateAdhesionById = $db->prepare("UPDATE acpa_adhesion 
								SET id_categorie_age =  :catAge
								WHERE acpa_adhesion.id_adhesion = :idAdhesion");
        $updateAdhesionById->execute(array('catAge' => $idCategory,
                                          'idAdhesion' => $idAdhesion));
        $updateAdhesionById->closeCursor();
           
	}


    /**
     * requete supression du lien d'une personnes avec ses "enfants"
     * @param $db object connexion a la base de données
     * @param $idPersonne int id de la personnes
     */
    public function deleteLinkChildPersonne($db, $idPersonne)
	{
		$deleteChildPersonne = $db->prepare(
            'UPDATE acpa_personne
            SET id_parent = NULL
            WHERE acpa_personne.id_parent = :idPersonne');
        $deleteChildPersonne->execute(array('idPersonne' => $idPersonne));
        $deleteChildPersonne->closeCursor();
           
	}
   
}

