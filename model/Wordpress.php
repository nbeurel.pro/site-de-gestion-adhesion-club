<?php
/* modele Worpresse: ce fichier contient toutes les requettes necessaire au dialogue entre l'interface de gestion et la base de données. elles sont triées en class:
 - la class Wordpress qui contient les informations de connexion à la bdd
 - la classe getter qui contient les requettes de lecture des informations contenu dans la bdd
 - la classe setter qui contient les requettes d'écriture dans la bdd
*/


//Appel du fichier de configuration qui contient les informations de connexions
require("conf.php");


// class de connection à la BDD

/**
 * Class Wordpress
 * cette classe contient les function général de liaison avec la base de données ACPA:
 * - fonction de connection à la BDD
 * - fonction de demarrage d'une transaction
 * - fonction de commit d'une transaction
 * - fonction de rollback d'une transaction
 */
class Wordpress
{

    /**
     * function de création de la connection a la bdd
     * @return PDO connexion a la bdd
     */
    protected function dbwpconnect()
	{
		
        // recuperation des constantes contenu dans conf.php
        $host = DBWP_HOST;
        $dataBaseName = DBWP_NAME; 
        $user = DBWP_USER;
        $pass = DBWP_PASS;
		
		try
		{
			
			$dbwp = new PDO('mysql:host=' . $host . ';dbname=' . $dataBaseName , $user, $pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
		
		}
		catch (Exception $e)
		{
			die('erreur : ' . $e->getmessage());
		}
		
		return $dbwp;
	}
    
}


/**
 * Class GetterWordpress contenant toute les fonctions de requettes de lecture de la bdd
 */
class GetterWordpress extends Wordpress
{	
    

    /**
     * requette simplifié de recuperation de la liste des nouveaux adherents en attente d'inscription
     * adherent dans la base de données de wordpresse en attente de transfert vers la bdd acpa
     * @return false|PDOStatement liste des nouveau adherents
     */
    public function getWaitingMembership()
	{
		$dbwp = $this->dbwpconnect();
		$listAdherentWait = $dbwp->query(
				'SELECT * 
				FROM wp_ACPA_inscription_en_attente
				WHERE wp_ACPA_inscription_en_attente.num_lic_siffa = 0');
		return $listAdherentWait;
		$listAdherentWait->closeCursor();
	}


	/**
     * requette simplifié de recuperation de la liste des ancien adherents en attente de reinscription     *
     * adherent dans la base de données de wordpresse en attente de transfert vers la bdd acpa
     * @return false|PDOStatement liste des adherents reinscrit
     */
    public function getWaitingReEnrollement()
	{
		$dbwp = $this->dbwpconnect();
		$listAdherentReEnrollement = $dbwp->query(
				'SELECT * 
				FROM wp_ACPA_inscription_en_attente
				WHERE wp_ACPA_inscription_en_attente.num_lic_siffa != 0');
		return $listAdherentReEnrollement;
		$listAdherentReEnrollement->closeCursor();
	}


    /**
     * requette simplifié de recuperation du nombre d'inscription en attente
     * @return int nombre d'adhesion en attente
     */
    public function getNumberReEnrollement()
	{
		$dbwp = $this->dbwpconnect();
		$ligneAdherents = $dbwp->query(
						'SELECT * 
						FROM wp_ACPA_inscription_en_attente');
		$numberAdherents = $ligneAdherents->fetchAll();
		$numberAdherents = count($numberAdherents);
		return $numberAdherents;
		$ligneAdherents->closeCursor();
	}
		
}


/**
 * Class SetterWordpress contenant toute les fonctions de requettes d'ecriture dans la bdd
 */
class SetterWordpress extends Wordpress
{	

    /**
     * requette de supression d'une entré dans la table inscription en attente en fonction de son ID
     * @param $ligne int nomero d'identifiant de la ligne
     * @return false|PDOStatement
     */
    public function deleteTheLine($ligne)
	{
		$dbwp = $this->dbwpconnect();
		$deleteLine = $dbwp->prepare(
				"DELETE FROM wp_ACPA_inscription_en_attente
				WHERE wp_ACPA_inscription_en_attente.id = :ligne");
        $deleteLine->execute(array(
            'ligne' => $ligne));
		return $deleteLine;
		$deleteLine->closeCursor();
	}
		
}

