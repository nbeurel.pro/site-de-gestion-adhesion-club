<?php 
$pageTitle = "gestion des moyen de paiement";
$title = "ACPA " . $pageTitle; 
ob_start(); ?>  

<section class="addNew messagepop popInMoyenPeiment">
    <div id="addMoyenPaiement"  class="popinList popInLittleContener">
        <form class="page" id="fichNewMoyenPaiement" >
            <h2 class="h2View">Créer un noyen de Paiement</h2>
            <div class="column-tot">
                <div class="group">
                    <label class="labelMoyenPaiement labelInputMoyenPaiement" for="addNomMoyenPaiement">nom</label>
                    <input type="text" class="inputMoyenPaiement addMoyenPayement" id="addNomMoyenPaiement" name="addNomMoyenPaiement" required/>
                </div>
                <div class="group">
                    <label class="labelMoyenPaiement labelInputMoyenPaiement" for="addAccepteMoyenPaiement">accepté</label>
                    <input type="checkbox" class="inputMoyenPaiement addMoyenPayement" id="addAccepteMoyenPaiement" name="addAccepteMoyenPaiement" required/>
                </div>
            </div>
            <div class="control">
                <button type="button" class="buttonMoyenPaiement modify saveAdd">Enregistrer</button>
                <button type="button" class="buttonMoyenPaiement close resumAdd">Retour</button>
            </div >
        </form>
    </div>
</section>
<section id="bdd">
    <h2><?= $pageTitle ?></h2>
    <form>
        <div id="control">
            <button class="logoAction logoAdd">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" d="M8 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10zM13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
                </svg>
            </button>  
            <button class="logoPrintPaper logoAction print" title="imprimer" id="printAdherents">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-printer" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path d="M11 2H5a1 1 0 0 0-1 1v2H3V3a2 2 0 0 1 2-2h6a2 2 0 0 1 2 2v2h-1V3a1 1 0 0 0-1-1zm3 4H2a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h1v1H2a2 2 0 0 1-2-2V7a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-1v-1h1a1 1 0 0 0 1-1V7a1 1 0 0 0-1-1z"/>
                  <path fill-rule="evenodd" d="M11 9H5a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1zM5 8a2 2 0 0 0-2 2v3a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2v-3a2 2 0 0 0-2-2H5z"/>
                  <path d="M3 7.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z"/>
                </svg>
            </button>
            <button class="logoDL logoAction excelize" id="excelizeAdherents" title="exporter">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-download" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" d="M.5 8a.5.5 0 0 1 .5.5V12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V8.5a.5.5 0 0 1 1 0V12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V8.5A.5.5 0 0 1 .5 8z"/>
                  <path fill-rule="evenodd" d="M5 7.5a.5.5 0 0 1 .707 0L8 9.793 10.293 7.5a.5.5 0 1 1 .707.707l-2.646 2.647a.5.5 0 0 1-.708 0L5 8.207A.5.5 0 0 1 5 7.5z"/>
                  <path fill-rule="evenodd" d="M8 1a.5.5 0 0 1 .5.5v8a.5.5 0 0 1-1 0v-8A.5.5 0 0 1 8 1z"/>
                </svg>
            </button>
        </div>
        <div id="affichTable" class="printable">
					<table id="tableListMoyenPaiement"  class="tableList Small">
						<thead>
							<tr class="test">
								<th>Nom du moyen de paiement</th>
								<th>Accepté</th>
								<th class="derCol"></th>
								<th class="derCol"></th>
                                <th class="hideCol"></th>
							</tr>
						</thead>
						<tbody id="table">
                            <?php
                                while ($payMethodItem = $payMethod->fetch())
                                {?>                            
                            <tr>
                                <td class="nomMoyenPaiement"><?= $payMethodItem['lib_moyen_paiement'] ?></td>
                                <?php if($payMethodItem['yn_accepte'] == '1'){ ?>
                                <td class="statutMoyenPaiement important">&#10004;</td>
                                <td class="derCol">
                                    <span class="link switchMoyenPaiement notAccept" title="ne plus accepter">
                                        &#10006;
                                    </span>
                                </td>
                                <td class="derCol">
                                    <span class="link deleteMoyenPaiement" title="supprimer ce moyen de paiement">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-x-octagon" fill="red" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M4.54.146A.5.5 0 0 1 4.893 0h6.214a.5.5 0 0 1 .353.146l4.394 4.394a.5.5 0 0 1 .146.353v6.214a.5.5 0 0 1-.146.353l-4.394 4.394a.5.5 0 0 1-.353.146H4.893a.5.5 0 0 1-.353-.146L.146 11.46A.5.5 0 0 1 0 11.107V4.893a.5.5 0 0 1 .146-.353L4.54.146zM5.1 1L1 5.1v5.8L5.1 15h5.8l4.1-4.1V5.1L10.9 1H5.1z"/>
                                            <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                        </svg>
                                    </span>  
                                </td>
                                <?php  }else{ ?>
                                <td class="libCategorie important">&#10006;</td>
                                <td class="derCol">
                                    <span class="link switchMoyenPaiement accept"  title="accepter" >
                                        &#10004;
                                    </span>
                                </td>
                                <td class="derCol">
                                    <span class="link deleteMoyenPaiement" title="supprimer ce moyen de paiement">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-x-octagon" fill="red" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M4.54.146A.5.5 0 0 1 4.893 0h6.214a.5.5 0 0 1 .353.146l4.394 4.394a.5.5 0 0 1 .146.353v6.214a.5.5 0 0 1-.146.353l-4.394 4.394a.5.5 0 0 1-.353.146H4.893a.5.5 0 0 1-.353-.146L.146 11.46A.5.5 0 0 1 0 11.107V4.893a.5.5 0 0 1 .146-.353L4.54.146zM5.1 1L1 5.1v5.8L5.1 15h5.8l4.1-4.1V5.1L10.9 1H5.1z"/>
                                            <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                        </svg>
                                    </span>  
                                </td>
                                    
                                 <?php  } ?>
                                <td class="hidecol idMoyenPaiement"><?= $payMethodItem['id_moyen_paiement'] ?></td>
                            </tr>
                            <?php } ?>
						</tbody>
					</table>
				</div>
    </form>
</section>
	
<?php $content = ob_get_clean(); ?>
<link rel="stylesheet" type="text/css" href="public/css/adherentView.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="public/css/indexView.css" media="screen"/>		
<link rel="stylesheet" type="text/css" href="public/css/indexPrint.css" media="print"/>
<script src="public/js/jquery.min.js"></script>	
<script src="public/js/initPayMethod.js"></script>
<?php require('template.php'); ?>