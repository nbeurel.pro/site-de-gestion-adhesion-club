<?php 
$pageTitle = "Liste des Contacts";
$title = "ACPA " . $pageTitle; 
session_start();
ob_start(); 

$listOrganisme = '<option value="autres">Autres</option>';
while($organisme = $organismes->fetch())
{
$listOrganisme = $listOrganisme . '<option value="'.$organisme[id_organisation] .'">'.$organisme[lib_organisation].'</option>';
}

$listeRole = "";
while($role = $roles->fetch())
{ 
    if($role[yn_tiers] == '1')
    {
        $listeRole = $listeRole .'<option value="'.$role[id_role].'">'. $role[lib_role].'</option>';
    }
}

?>
	   <section class="addNew messagepop popInContacts">
			<div id="addNewCts" class="popinList popInLittleContener">
				<form class="page" id="fichNewContact" >
                    <h2 class="h2View">Ajouter un nouveau contact</h2>
                    <div class="column-tot">
                        <div class="group">
                            <label for="addNameCts" class="labelInputAdd labelRequired">NOM</label>
                            <input type="text" class="inputAdd inputRequired" name="addNameCts" id="addNameCts" />
                        </div>
                        <div class="group">
                            <label for="addFirstNameCts" class="labelInputAdd labelRequired">PRENOM</label>
                            <input type="text" class="inputAdd inputRequired" name="addFirstNameCts" id="addFirstNameCts" />
                        </div>
                        <div class="group">
                            <label for="addOrgCts" class="labelInputAdd labelRequired">ORGANISATION</label>
                            <select class="selectAdd inputRequired" name="addOrgCts" id="addOrgCts">
                                <option value=""></option>
                                <?= $listOrganisme; ?>
                            </select>
                            <input type="text" class="inputAdd" name="addAddOrgCts" id="addAddOrgCts" placeholder="nouvelle organisation"/>
                        </div>
                        <div class="group">
                            <label for="addMailCts" class="labelInputAdd">COURRIEL</label>
                            <input type="email" class="inputAdd" name="addMailCts" id="addMailCts" />
                        </div>
                        <div class="group">
                            <label for="addMobileCts" class="labelInputAdd">MOBILE</label>
                            <input type="tel" class="inputAdd" name="addMobileCts" id="addMobileCts" />
                        </div>
                        <div class="group">
                            <label for="addFixeCts" class="labelInputAdd">FIXE</label>
                            <input type="tel" class="inputAdd" name="addFixeCts" id="addFixeCts" />
                        </div>
                        <div class="group">
                            <label for="addAdressCts" class="labelInputAdd">ADRESSE</label>
                            <input type="text" class="inputAdd" name="addAdressCts" id="addAdressCts" />
                        </div>
                        <div class="group">
                            <label for="addZipCodeCts" class="labelInputAdd">CODE POSTAL</label>
                            <input type="number" class="inputAdd" name="addZipCodeCts" id="addZipCodeCts" />
                        </div>
                        <div class="group">
                            <label for="addCityCts" class="labelInputAdd">VILLE</label>
                            <input type="text" class="inputAdd" name="addCityCts" id="addCityCts" />
                        </div>
                        <div class="group">
                            <label for="addRoleCts" class="labelInputAdd labelRequired">ROLE</label>
                            <select class="selectAdd inputRequired" name="addRoleCts" id="addRoleCts">
                                <option value=""></option>
                                <?= $listeRole  ?>
                            </select>
                        </div>
                    </div>
                    <div class="control">
                        <button type="button" class="buttonContact modify saveAdd">Enregistrer</button>
                        <button type="button" class="buttonContact close resumeAdd">Fermer</button>
                    </div>
                </form>
			</div>
		</section>
        <section class="Modif messagepop popInContacts">
			<div id="ModifiContact" class="popinList popInLittleContener">
				<form class="page" id="fichModifContact" >
                    <h2 class="h2View">Modifier le contact N°: <span id="numIdFocus"></span></h2>
                    <div class="column-tot">
                        <div class="group">
                            <label for="modifNameCts" class="labelInputModif">NOM</label>
                            <input type="text" class="inputModif" name="modifNameCts" id="modifNameCts" />
                        </div>
                        <div class="group">
                            <label for="modifFirstNameCts" class="labelInputModif">PRENOM</label>
                            <input type="text" class="inputModif" name="modifFirstNameCts" id="modifFirstNameCts" />
                        </div>
                        <div class="group">
                            <label for="modifOrgCts" class="labelInputModif">ORGANISATION</label>
                            <select class="selectAdd" name="modifOrgCts" id="modifOrgCts">
                                <?= $listOrganisme; ?> 
                            </select>
                            <input type="text" class="inputModif" name="modifAddOrgCts" id="modifAddOrgCts" placeholder="nouvelle organisation"/>
                        </div>
                        <div class="group">
                            <label for="modifMailCts" class="labelInputModif">COURRIEL</label>
                            <input type="email" class="inputModif" name="modifMailCts" id="modifMailCts" />
                        </div>
                        <div class="group">
                            <label for="modifMobileCts" class="labelInputModif">MOBILE</label>
                            <input type="tel" class="inputModif" name="modifMobileCts" id="modifMobileCts" />
                        </div>
                        <div class="group">
                            <label for="modifFixeCts" class="labelInputModif">FIXE</label>
                            <input type="tel" class="inputModif" name="modifFixeCts" id="modifFixeCts" />
                        </div>
                        <div class="group">
                            <label for="modifAdressCts" class="labelInputModif">ADRESSE</label>
                            <input type="text" class="inputModif" name="modifAdressCts" id="modifAdressCts" />
                        </div>
                        <div class="group">
                            <label for="modifZipCodeCts" class="labelInputModif">CODE POSTAL</label>
                            <input type="number" class="inputModif" name="modifZipCodeCts" id="modifZipCodeCts"/>
                        </div>
                        <div class="group">
                            <label for="modifCityCts" class="labelInputModif">VILLE</label>
                            <input type="text" class="inputModif" name="modifCityCts" id="modifCityCts" />
                        </div>
                        <div class="group">
                            <label for="modifRoleCts" class="labelInputModif">ROLE</label>
                            <select class="selectModif" name="modifRoleCts" id="modifRoleCts">
                                <?= $listeRole ?>
                            </select>
                        </div>
                    </div>
                    <div class="control">
                        <button type="button" class="buttonContact modify saveModif">Enregistrer</button>
                        <button type="button" class="buttonContact close resumeModif">Fermer</button>
                    </div>
                </form>
			</div>
		</section>
		<section id="bdd">
			<h2><?= $pageTitle ?></h2>
			<form method="post" action="#">
				<div id="control">                   
                    <button class="logoAction logoAdd">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path fill-rule="evenodd" d="M8 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10zM13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
                        </svg>
                    </button>                    
					<button class="logoPrintPaper logoAction print" title="imprimer" id="printContacts">
						<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-printer" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
						  <path d="M11 2H5a1 1 0 0 0-1 1v2H3V3a2 2 0 0 1 2-2h6a2 2 0 0 1 2 2v2h-1V3a1 1 0 0 0-1-1zm3 4H2a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h1v1H2a2 2 0 0 1-2-2V7a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-1v-1h1a1 1 0 0 0 1-1V7a1 1 0 0 0-1-1z"/>
						  <path fill-rule="evenodd" d="M11 9H5a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1zM5 8a2 2 0 0 0-2 2v3a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2v-3a2 2 0 0 0-2-2H5z"/>
						  <path d="M3 7.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z"/>
						</svg>
					</button>
					<button class="logoDL logoAction excelize" id="excelizeContacts" title="exporter">
						<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-download" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
						  <path fill-rule="evenodd" d="M.5 8a.5.5 0 0 1 .5.5V12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V8.5a.5.5 0 0 1 1 0V12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V8.5A.5.5 0 0 1 .5 8z"/>
						  <path fill-rule="evenodd" d="M5 7.5a.5.5 0 0 1 .707 0L8 9.793 10.293 7.5a.5.5 0 1 1 .707.707l-2.646 2.647a.5.5 0 0 1-.708 0L5 8.207A.5.5 0 0 1 5 7.5z"/>
						  <path fill-rule="evenodd" d="M8 1a.5.5 0 0 1 .5.5v8a.5.5 0 0 1-1 0v-8A.5.5 0 0 1 8 1z"/>
						</svg>
					</button>
				</div>
				<div id="affichTable" class="printable">
					<table id="tableListContacts" class="tableList">
						<thead>
							<tr class="test">
								<th>NOM
                                    <span class="orderer">
                                        <span class="order asc orderName" id="orderNameAsc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 11L8 5.519 12.796 11H3.204zm-.753-.659l4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z"/>
                                            </svg>
                                        </span>
                                        <span class="order desc orderName" id="orderNameDesc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/>
                                            </svg>
                                        </span>
                                    </span>
                                </th>
								<th>PRENOM
                                    <span class="orderer">
                                        <span class="order asc orderFirstName" id="orderFirstNameAsc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 11L8 5.519 12.796 11H3.204zm-.753-.659l4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z"/>
                                            </svg>
                                        </span>
                                        <span class="order desc orderFirstName" id="orderFirstNameDesc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/>
                                            </svg>
                                        </span>
                                    </span>
                                </th>                               
								<th>ORGANISATION
                                    <span class="orderer">
                                        <span class="order asc orderOrganism" id="orderOrganismAsc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 11L8 5.519 12.796 11H3.204zm-.753-.659l4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z"/>
                                            </svg>
                                        </span>
                                        <span class="order desc orderOrganism" id="orderOrganismDesc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/>
                                            </svg>
                                        </span>
                                    </span>
                                </th>
                                <th>COURRIEL
                                    <span class="orderer">
                                        <span class="order asc orderMail" id="orderMailAsc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 11L8 5.519 12.796 11H3.204zm-.753-.659l4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z"/>
                                            </svg>
                                        </span>
                                        <span class="order desc orderMail" id="orderMailDesc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/>
                                            </svg>
                                        </span>
                                    </span>
                                </th>
								<th>PORTABLE
                                    <span class="orderer">
                                        <span class="order asc orderSmartPhone" id="orderSmartPhoneAsc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 11L8 5.519 12.796 11H3.204zm-.753-.659l4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z"/>
                                            </svg>
                                        </span>
                                        <span class="order desc orderSmartPhone" id="orderSmartPhoneDesc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/>
                                            </svg>
                                        </span>
                                    </span>
                                </th>
								<th>FIXE
                                    <span class="orderer">
                                        <span class="order asc orderFixePhone" id="orderFixePhoneAsc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 11L8 5.519 12.796 11H3.204zm-.753-.659l4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z"/>
                                            </svg>
                                        </span>
                                        <span class="order desc orderFixePhone" id="orderFixePhoneDesc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/>
                                            </svg>
                                        </span>
                                    </span>
                                </th>
								<th>ADRESSE
                                    <span class="orderer">
                                        <span class="order asc orderAdress" id="orderAdressAsc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 11L8 5.519 12.796 11H3.204zm-.753-.659l4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z"/>
                                            </svg>
                                        </span>
                                        <span class="order desc orderAdress" id="orderAdressDesc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/>
                                            </svg>
                                        </span>
                                    </span>
                                </th>
								<th>ROLE
                                    <span class="orderer">
                                        <span class="order asc orderRole" id="orderRoleAsc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 11L8 5.519 12.796 11H3.204zm-.753-.659l4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z"/>
                                            </svg>
                                        </span>
                                        <span class="order desc orderRole" id="orderRoleAsc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/>
                                            </svg>
                                        </span>
                                    </span>
                                </th>
								<th class="derCol"></th>
								<th class="derCol"></th>
                                <th class="hideCol"></th>
							</tr>			
							<tr class="filtres">
								<th class="test">
									<input class="seek_table" type="text" name="var_nom" id="input_nom"/>
								</th>
								<th class="test">
									<input class="seek_table" type="text" name="var_prenom" id="input_prenom"/>
								</th>
								<th class="test">
									<input class="seek_table" type="text" name="var_organisme" id="input_organisme"/>
								</th>
								<th class="test">
									<input class="seek_table" type="text" name="var_mel" id="input_mel"/>
								</th>
								<th class="test">
									<input class="seek_table" type="text" name="var_portable" id="input_portable"/>
								</th>
								<th class="test">
									<input class="seek_table" type="text" name="var_fixe" id="input_fixe"/>
								</th>
								<th class="test">
									<input class="seek_table" type="text" name="var_adresse" id="input_adresse"/>
								</th>
								<th class="test">
                                    <select class="seek_table"  name="var_role" id="input_role">
                                        <option value="%"></option>
                                        <?php while($role = $roles->fetch())
                                        { if($role[yn_tiers] == '1'){?>
                                        <option value="<?= $role[id_role] ?>"><?= $role[lib_role] ?></option>
                                        <?php }} ?>
                                    </select>
								</th>
								<th class="derCol"></th>
								<th class="derCol"></th>
                                <th class="hideCol"></th>
							</tr>
						</thead>
						<tbody id="table">
						</tbody>
					</table>
				</div>		
			</form>
		</section>
<?php $content = ob_get_clean(); ?>
<link rel="stylesheet" type="text/css" href="public/css/adherentView.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="public/css/indexView.css" media="screen"/>		
<link rel="stylesheet" type="text/css" href="public/css/indexPrint.css" media="print"/>	

<script src="public/js/jquery.min.js"></script>	
<script src="public/js/initiContacts.js"></script>
<?php require('view/template.php'); ?>
