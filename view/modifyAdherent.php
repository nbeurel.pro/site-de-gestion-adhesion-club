<?php $title = "ACPA modifier un adherent" ?>
<?php ob_start(); 
$acpa = new Acpa();
$getterAcpa = new GetterAcpa();
$setterAcpa = new SetterAcpa();
$adherant = $adherent->fetch();
$msgAlert="0";
if($adherant['lib_sexe'] == 'M')
{
    $sexe = 'Masculin';
}else{
    $sexe = 'Féminin';
}
if(isset($_POST['test']) && !empty($_POST['test'])){
   
    $idDoc = $_POST['test'];
    $certif = fopen($_FILES['certifMedical']['tmp_name'], 'rb');
    
    $db = $acpa->dbconnect();
    $setterAcpa->updateCertifByIdAdhesion($db, $idDoc, $certif);
    
    $msgAlert="1";
     
}

?>
	 
        <section class="gestionWaiting popInGestion">
            <div id="divGifLoading" class="popinList">
                <img id="GifLoading" src="./public/images/loading.gif" />
            </div>
        </section>
        <section class="gestionCertifMedical popInGestion">
            <div id="divCertifMedical"  class="popinList">
                <form class="page" action="" method="post" enctype="multipart/form-data">
                    <h2 class="h2View">Modifier le certificat médical de l'adhesion n°<?= $adherant['id_adhesion'] ?></h2>
                    <div class="column-tot">
                        <label class="labelAdherent" for="certifMedical">nouveau certificat médical</label>
                        <input type="hidden" name="test" value="<?=$adherant['id_document']?>"/>
                        <input type="file" id="certifMedical" name="certifMedical" accept="application/pdf, image/png, image/jpg" required/>
                        <div class="control">
                            <input type="submit" class="buttonAdherent modifyCertificat" id="modifCertificatEnr" value='Enregistrer'/>
                            <button type="button" class="buttonAdherent returnCertificat" id="returnCertificat">Retour</button>
                        </div >
                    </div>
                </form>
            </div>
        </section>
        <section class="gestionActivity popInGestion">
            <div id="listActivity"  class="popinList">
                <form  class="page">
                    <h2 class="h2View">activité de l'adhésion n°<?= $adherant['id_adhesion'] ?></h2>
                    <div class="column-tot">
                        <?php while ($activityView = $activity->fetch())
                                { 
                                    $makeActivity = $getterAcpa->getActivityAdhesionExist($db, $adherant['id_adhesion'],  $activityView['id_activite']);
                                    $activityOk = $makeActivity->fetchAll();
                                    $activityOk = count($activityOk);                    ?>
                                    <div class="activityList">
                                        <label class="labelActivity" for="<?= $activityView['lib_court_activite'] ?>"><?= $activityView['lib_activite'] ?></label>
                                        <input type="checkbox" class="activityGestion" value="<?= $activityView['id_activite'] ?>" name="<?= $activityView['lib_court_activite'] ?>" 
                                               <?php  if($activityOk == 1){ ?> checked <?php } ?> />
                                    </div>
                        <?php } ?>	
                        <div class="control">
                            <button type="button" class="buttonAdherent modifyActivity" id="modifActivityEnr">Enregistrer</button>
                            <button type="button" class="buttonAdherent returnActivity" id="returnActivity">Retour</button>
                        </div>
                    </div >                    	
                </form>
            </div>
        </section>
        <section class="gestionRoles popInGestion">
            <div id="listRoles" class="popinList">
                <form  class="page">
                    <h2 class="h2View">roles de l'adhérent n°<?= $adherant['id_personne'] ?> pour la saison</h2>
                    <div class="column-tot">
                        <?php while ($rolesView = $roles->fetch())
                                { if($rolesView['yn_adherent'] == '1'){
                                    $haveRole = $getterAcpa->getRoleAdhesionExist($db, $adherant['id_adhesion'],  $rolesView['id_role']);
                                    $roleOk = $haveRole->fetchAll();
                                    $roleOk = count($roleOk);
                        ?>
                        <div class="roleList">
                            <label class="labelRole" for="<?= $rolesView['lib_court_role'] ?>"><?= $rolesView['lib_role'] ?></label>
                            <input type="checkbox" class="roleGestion" value="<?= $rolesView['id_role'] ?>" name="<?= rolesView['lib_court_role'] ?>"
                                               <?php  if($roleOk == 1){ ?> checked <?php } ?> /> 
                        </div>
                        <?php }} ?>
                        <div class="control">
                            <button type="button" class="buttonAdherent modifyRole" id="modifRoleEnr">Enregistrer</button>
                            <button type="button" class="buttonAdherent returnRole" id="returnRole">Retour</button>
                        </div >
                    </div>
                </form>
            </div>
            <br><br>
        </section>
        <section class="page">
			<div id="infoAdherent">
				<form class="" id="">
                    <h3>Modifier l'adhésion n°<span id="numAdhesion"><?= $adherant['id_adhesion'] ?></span></h3>
                    <div class="container">
                        <div class="column">
                            <div class="group">
                                <label class="labelAdherent" for="identifiant" >N° d'adherent</label> 
                                <input class="inputAdherent" type="text" id="identifiant" value="<?= $adherant['id_personne'] ?>" disabled/>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="siffa" >N° de licence SIFFA</label> 
                                <input class="inputAdherent" type="text" id="siffa" value="<?= $adherant['num_licence_SIFFA'] ?>"/>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="licence_date" >Date de licence</label>
                                <input class="inputAdherent" type="date" id="licence_date" value="<?= $adherant['dt_licence'] ?>"/>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="name" >Nom</label>
                                <input class="inputAdherent" type="text" id="name" value="<?= $adherant['lib_nom'] ?>"/>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="first_name" >Prénom</label>
                                <input class="inputAdherent" type="text" id="first_name" value="<?= $adherant['lib_prenom'] ?>"/>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="gender" >Sexe</label>
                                
                                <input type="radio" name="sexe" value="M" <?php if($adherant['lib_sexe'] == M){?>
                                        checked<?php
                                    }
                                ?>/>Masculin
                                <input type="radio" name="sexe" value="F" <?php if($adherant['lib_sexe'] == F){?>
                                        checked<?php
                                    }
                                ?>/>Feminin
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="birthdate" >Date de naissance</label>
                                <input class="inputAdherent" type="date" id="birthdate" value="<?= $adherant['dt_naissance'] ?>"/>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="country" >Nationalité</label>
                                <input class="inputAdherent" type="text" id="country" value="<?= $adherant['lib_nationalite'] ?>" />
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="adress" >Adresse complète</label>
                                <input class="inputAdherent" type="text" id="adress" value="<?= $adherant['lib_adresse'] ?>" />
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="postal" >Code postal</label>
                                <input class="inputAdherent" type="text" id="postal" value="<?= $adherant['num_code_postal'] ?>"/>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="city" >Ville</label>
                                <input class="inputAdherent" type="text" id="city" value="<?= $adherant['lib_ville'] ?>"/>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="mail" >Email</label>
                                <input class="inputAdherent" type="email" id="mail"  value="<?= $adherant['lib_mail_personne'] ?>"/>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="mobile" >Téléphone portable</label>
                                <input class="inputAdherent" type="text" id="mobile" value="<?= $adherant['tel_mobile'] ?>"/>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="phone" >Téléphone fixe</label>
                                <input class="inputAdherent" type="text" id="phone" value="<?= $adherant['tel_fixe'] ?>"/>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="last_date" >Date de dernière connexion</label>
                                <input class="inputAdherent" type="date" id="last_date" value="<?= $adherant['dtm_connexion'] ?>"/>
                            </div>
                            
                            <div class="group">
                                <label class="labelAdherent" for="idparent" >Rattaché au compte de</label>
                                <select id="idparent" class="selectIdparent" name="idparent"  >
                                    <option value="" class='selected' selected> </option>
                                   <?php while ($AdherentsParent = $listAdherent->fetch())
                                    {
                                        if ($AdherentsParent['id_personne'] == $adherant['id_parent'])
                                        {
                                    ?>
                                            <option value="<?= $AdherentsParent['id_personne'] ?>" class='selected' selected><?= $AdherentsParent['lib_nom'] . ' ' . $AdherentsParent['lib_prenom']?> &#10004;</option>
                                    <?php			
                                        }
                                        else
                                        {
                                    ?>
                                            <option value="<?= $AdherentsParent['id_personne'] ?>" ><?= $AdherentsParent['lib_nom'] . ' ' . $AdherentsParent['lib_prenom']?></option>
                                    <?php
                                        }
                                    }
                                    ?>			
                                </select>
                            </div>
                            <hr>
                            <div class="group">
                                <label class="labelAdherent" for="identifiant" >identifiant</label>
                                <input class="inputAdherent-infoc" type="text" id="loginAdherent" value="<?= $adherant['login'] ?>"/>
                            </div>
                        </div>
                        <div class="column">
                            <h3 class="h3View">
                                <?php while ($seasonBdd = $season->fetch())
                                    {
                                       if ($seasonBdd['id_saison'] == $saison)
                                       { ?>
                                            <span><?= $seasonBdd['lib_saison'] ?> </span><input type="hidden" id="idseason" value="<?= $seasonBdd['id_saison'] ?>" />
                                    <?php
                                       }
                                    }
                                    ?>			
                                </h3>
                           <div class="group">
                               <label class="labelAdherent" for="id_group_adh" >ID groupe adhésion</label>
                               <input class="inputAdherent" type="text" id="id_group_adh" value="<?= $adherant['lib_identifiant_gr_adhesion'] ?>"/>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="legal" >Responsable légal</label>
                                <input class="inputAdherent-middle" type="text" id="legal" value="<?= $adherant['lib_nom_prenom_responsable_legal'] ?>"/><input class="inputAdherent-little" type="text" id="legal" value="<?= $adherant['lib_type_responsable_legal'] ?>"/>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="cat_age" >Catégorie d'âge</label>
                                <select id="cat_age" class="selectCatAge" name="CatAge"  >
                                   <?php while ($categoryAge = $category->fetch())
                                    {
                                        if ($categoryAge['lib_categorie_age'] == $adherant['lib_categorie_age'])
                                        {
                                    ?>
                                            <option value="<?= $categoryAge['id_categorie_age'] ?>" class='selected' selected><?= $categoryAge['lib_categorie_age'] ?> &#10004;</option>;
                                    <?php			
                                        }
                                        else
                                        {
                                    ?>
                                            <option value="<?= $categoryAge['id_categorie_age'] ?>"><?= $categoryAge['lib_categorie_age'] ?></option>;
                                    <?php
                                        }
                                    }
                                    ?>			
                                </select>
                            </div>
                            <div class="group">
                                <label  class="labelAdherent">Activités :</label>
                                <textarea class="textareAdherent" rows="1" cols="25" disabled><?= str_replace(",","\n" ,$adherant['list_activite']) ?></textarea>
                                <span class="gestion gestion-activity" title="modifier">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                      <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                    </svg>
                                </span>
                            </div>
                            <div class="group">
                                <label class="labelAdherent">Rôles :</label>
                                <textarea class="textareAdherent-2" rows="1" cols="25" disabled><?= str_replace(',','; ',$adherant['list_role']) ?></textarea>
                                <span class="gestion gestion-role" title="modifier">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                      <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                    </svg>
                                </span>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="licence" >Type et tarif  de la licence</label>
                                <select id="typ_lic" class="selectTypeLic" name="TypeLic"  >
                                   <?php while ($typeLicence = $typeLic->fetch())
                                    {
                                        if ($typeLicence['lib_type_licence'] == $adherant['lib_type_licence'])
                                        {
                                    ?>
                                            <option value="<?= $typeLicence['id_type_licence'] ?>" class='selected' selected><?= $typeLicence['lib_type_licence']?> &#10004;</option>;
                                    <?php			
                                        }
                                        else
                                        {
                                    ?>
                                            <option value="<?= $typeLicence['id_type_licence'] ?>"><?= $typeLicence['lib_type_licence'] ?></option>;
                                    <?php
                                        }
                                    }
                                    ?>			
                                </select>
                                <input class="inputAdherent-little" type="text" id="tarifLicence" value="<?= $adherant['tarif_adhesion'] ?>" />€
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="date_season" >Date d'adhésion pour la saison</label>
                                <input class="inputAdherent" type="date" id="date_season" value="<?= $adherant['dtm_adhesion'] ?>" />
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="statut" >Statut de l'adhésion</label>
                                <select id="statut" class="selectStatut" name="statut"  >
                                   <?php while ($statutPay = $StatusAdhesion->fetch())
                                    {
                                        if ($statutPay['lib_statut'] == $adherant['lib_statut'])
                                        {
                                    ?>
                                            <option value="<?= $statutPay['id_statut'] ?>" class='selected' selected><?= $statutPay['lib_statut']?> &#10004;</option>;
                                    <?php			
                                        }
                                        else
                                        {
                                    ?>
                                            <option value="<?= $statutPay['id_statut'] ?>"><?= $statutPay['lib_statut'] ?></option>;
                                    <?php
                                        }
                                    }
                                    ?>			
                                </select>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="date_paiement" >Date de validation du paiement</label>
                                <input class="inputAdherent-dateco" type="date" id="date_paiement" value="<?= $adherant['dtm_validation'] ?>" />
                            </div>
                            <h4>Autorisation parental:</h4>
                            <div class="group">
                                <label class="labelAdherent" for="prelevementsanguin" >- prélevement sanguin</label>
                                <input type="checkbox" id="prelevementsanguin" <?php
                                    if($adherant['yn_prelevement'] == 1){?>
                                        checked<?php
                                    }
                                ?>/>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="hospitalisation" >- hospitalisation</label>
                                <input type="checkbox" id="hospitalisation" <?php 
                                       if($adherant['yn_hospitalisation'] == 1){?>
                                        checked<?php
                                    }
                                ?>/>
                            </div>
                            <h4>Accepte:</h4>
                            <div class="group">
                                <label class="labelAdherent" for="Ri" >- Le reglement interieur</label>
                                <input type="checkbox" id="Ri" <?php
                                    if($adherant['yn_reglement_interieur'] == 1){?>
                                        checked<?php
                                    }
                                ?>/>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="insurance" >- L'assurance</label>
                                <input type="checkbox" id="insurance" <?php
                                    if($adherant['yn_assurance'] == 1){?>
                                        checked<?php
                                    }
                                ?>/>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="droitImage" >- L'utilisation de son image</label>
                                <input type="checkbox" id="droitImage" <?php
                                    if($adherant['yn_droit_image'] == 1){?>
                                        checked<?php
                                    }
                                ?>/>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="law" >- La loi informatique et libertés</label>
                                
                                <input type="checkbox" id="law"<?php
                                    if($adherant['yn_cnil'] == 1){?>
                                        checked<?php
                                    }
                                ?>/>
                            </div>
                            <div class="group">
                                <label class="labelAdherent" for="certificatMedical"><b>Certificat Medical : </b></label>
                                <a class="lienCertif" href="document.php?id=<?= $adherant['id_document'] ?>" target="_blank" >
                                    <img class="imgCertif" title="voir le document" alt="voir le document" src="./public/images/certif-medical.png" /> 
                                </a>
                                <span class="gestion gestion-certif" title="modifier le document">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                      <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                    </svg>
                                </span>
                                
                            </div>
                        </div>
                    </div>
                    <div class="control">
                        <button type="button" class="buttonAdherent modify">Enregistrer</button>
                        <a href="index.php?view=adherents"><button type="button" class="buttonAdherent return">Retour</button></a>
                    </div >
                </form>
			</div>
		</section>
        
        
<?php 
$content = ob_get_clean(); 
if($msgAlert == '1'){ 
?>
    <script language="javascript">
        setTimeout(function(){ alert("le document a été mis à jour"); }, 3000);
    </script>
   <?php } 
?>
<link rel="stylesheet" type="text/css" href="public/css/modifyView.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="public/css/indexView.css" media="screen"/>	
<link rel="stylesheet" type="text/css" href="public/css/indexPrint.css" media="print"/>
<script src="public/js/jquery.min.js"></script>
<script src="public/js/modifyAdherent.js"></script>
<?php require('view/template.php'); ?>
