<?php 
$pageTitle = "Liste des adhésions en attente";
$title = "ACPA " . $pageTitle; 
ob_start(); ?>
		<section id="control">	
			<h2>Adhésion en attente de validation</h2>
			<a href='index.php?view=adherents'>
				<button class="logoAction logoRetour" title="retour à la liste des licenciés">
					<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					  <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
					  <path fill-rule="evenodd" d="M8.354 11.354a.5.5 0 0 0 0-.708L5.707 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z"/>
					  <path fill-rule="evenodd" d="M11.5 8a.5.5 0 0 0-.5-.5H6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5z"/>
					</svg>
				</button>
			</a>
			<button id="printWaiting" class="logoPrintPaper logoAction print" title="imprimer" >
				<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-printer" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
				  <path d="M11 2H5a1 1 0 0 0-1 1v2H3V3a2 2 0 0 1 2-2h6a2 2 0 0 1 2 2v2h-1V3a1 1 0 0 0-1-1zm3 4H2a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h1v1H2a2 2 0 0 1-2-2V7a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-1v-1h1a1 1 0 0 0 1-1V7a1 1 0 0 0-1-1z"/>
				  <path fill-rule="evenodd" d="M11 9H5a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1zM5 8a2 2 0 0 0-2 2v3a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2v-3a2 2 0 0 0-2-2H5z"/>
				  <path d="M3 7.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z"/>
				</svg>
			</button>
			<button class="logoDL logoAction excelize" id="excelizeWaiting" title="exporter">
				<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-download" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
				  <path fill-rule="evenodd" d="M.5 8a.5.5 0 0 1 .5.5V12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V8.5a.5.5 0 0 1 1 0V12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V8.5A.5.5 0 0 1 .5 8z"/>
				  <path fill-rule="evenodd" d="M5 7.5a.5.5 0 0 1 .707 0L8 9.793 10.293 7.5a.5.5 0 1 1 .707.707l-2.646 2.647a.5.5 0 0 1-.708 0L5 8.207A.5.5 0 0 1 5 7.5z"/>
				  <path fill-rule="evenodd" d="M8 1a.5.5 0 0 1 .5.5v8a.5.5 0 0 1-1 0v-8A.5.5 0 0 1 8 1z"/>
				</svg>
			</button>
			<button id="printReEnrollment" class="logoPrintPaper logoAction2 print" title="imprimer">
				<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-printer" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
				  <path d="M11 2H5a1 1 0 0 0-1 1v2H3V3a2 2 0 0 1 2-2h6a2 2 0 0 1 2 2v2h-1V3a1 1 0 0 0-1-1zm3 4H2a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h1v1H2a2 2 0 0 1-2-2V7a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-1v-1h1a1 1 0 0 0 1-1V7a1 1 0 0 0-1-1z"/>
				  <path fill-rule="evenodd" d="M11 9H5a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1zM5 8a2 2 0 0 0-2 2v3a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2v-3a2 2 0 0 0-2-2H5z"/>
				  <path d="M3 7.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z"/>
				</svg>
			</button>
			<button class="logoDL logoAction2 excelize" id="excelizeReEnrollment" title="exporter">
				<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-download" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
				  <path fill-rule="evenodd" d="M.5 8a.5.5 0 0 1 .5.5V12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V8.5a.5.5 0 0 1 1 0V12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V8.5A.5.5 0 0 1 .5 8z"/>
				  <path fill-rule="evenodd" d="M5 7.5a.5.5 0 0 1 .707 0L8 9.793 10.293 7.5a.5.5 0 1 1 .707.707l-2.646 2.647a.5.5 0 0 1-.708 0L5 8.207A.5.5 0 0 1 5 7.5z"/>
				  <path fill-rule="evenodd" d="M8 1a.5.5 0 0 1 .5.5v8a.5.5 0 0 1-1 0v-8A.5.5 0 0 1 8 1z"/>
				</svg>
			</button>
		</section>
		<section id="sectionbdd">	
			<h3 class="titleTable">Nouveaux adhérents</h3>
			<div id="bdd" class="classBdd">	
				<table id="tableToPrint" class="classTable">
					<thead id='lol'>
						<tr class="firstline">
                            <th class="forCheckbox"></th>
							<th class="zero identiteUn withBorder" colspan=2>IDENTITE</th>
							<th class="un identiteDeux withBorder" colspan=3>IDENTITE</th>
							<th class="un withBorder" colspan=2>INFO INSCRIPTION</th>			
							<th class="un withBorder" colspan=6>COORDONNEES</th>
							<th class="deux withBorder" colspan=4>INSCRIPTION</th>
							<th class="deux withBorder" colspan=4>MINEUR</th>
							<th class="trois withBorder" colspan=4>ACCORD</th>
							<th class="trois withBorder" colspan=2>INFO PAIEMENT</th>
                            <th class="id hideCol"></th>
						</tr>	
						<tr class="secondline">
                            <th class="forCheckbox"></th>
							<th class="zero withBorder">NOM</th>
							<th class="zero withBorder">PRENOM</th>
							<th class="un withBorder">SEXE</th>
							<th class="un withBorder">DATE DE <br/>NAISSANCE</th>
							<th class="un withBorder">NATIONALITE</th>
							<th class="un withBorder">DATE</th>
							<th class="un withBorder">SAISON</th>							
							<th class="un withBorder">ADRESSE</th>
							<th class="un withBorder">CODE POSTAL</th>
							<th class="un withBorder">VILLE</th>
							<th class="un withBorder">E-MAIL</th>
							<th class="un withBorder" colspan=2>TELEPHONE</th>
							<th class="deux withBorder">CATEGORIE</th>
							<th class="deux withBorder">TYPE DE LICENCE</th>
							<th class="deux withBorder">LISTE DES ACTIVITES</th>
							<th class="deux withBorder">LIEN VERS <br/>LE CERTIFICAT MEDICAL</th>
							<th class="deux withBorder">IDENTITE <br/>REPRESENTANT LEGAL</th>
							<th class="deux withBorder">LIEN <br/>AVEC ADHERENT</th>
							<th class="deux withBorder">AUTORISATION <br/>PRELEVEMENT SANGUIN</th>
							<th class="deux withBorder">AUTORISATION <br/>HOSPITALISATION</th>
							<th class="trois withBorder">REGLEMENT <br/>INTERIEUR</th>
							<th class="trois withBorder">DROIT A <br/>L'IMAGE</th>
							<th class="trois withBorders">CNIL</th>
							<th class="trois withBorder">ASSURANCE</th>
							<th class="trois withBorders">ETAT PAIEMENT</th>
							<th class="trois withBorder">CLE PAIEMENT</th>                            
                            <th class="id hideCol"></th>
						</tr>			
					</thead>
					<tbody id='corpTable'>
					<?php
						while ($adherant = $listAdherentWait->fetch())
						{
							?>
						 <tr>
                            <td class="forCheckbox"><input class="checkboxNew" type="checkbox"></td>
                            <td class="zero withBorder nameAd"><?= $adherant['nom'] ?></td>
                            <td class="zero withBorder nicknameAd"><?= $adherant['prenom'] ?></td>
                            <td class="un withBorder sexeAd"><?= $adherant['sexe'] ?></td>
                            <td class="un withBorder birthdayAd"><?= implode('/',array_reverse(explode('-',$adherant['date_naissance']))) ?></td>
                            <td class="un withBorder countryAd"><?= $adherant['nationalité'] ?></td>
                            <td class="un withBorder subscriptionDateAd"><?= implode('/',array_reverse(explode('-',$adherant['date_inscription']))) ?></td>
                            <td class="un withBorder seasonAd"><?= $adherant['saison'] ?></td>
                            <td class="un withBorder adressAd"><?= $adherant['adresse'] ?></td>
                            <td class="un withBorder zipCodeAd"><?= $adherant['code_postal'] ?></td>
                            <td class="un withBorder cityAd"><?= $adherant['ville'] ?></td>
                            <td class="un withBorder emailAd"><?= $adherant['email'] ?></td>
                            <td class="un withBorder mobileAd"><?= $adherant['tel_portable'] ?></td>
                            <td class="un withBorder fixeAd"><?= $adherant['tel_fixe'] ?></td>
                            <td class="deux withBorder ageAd"><?= $adherant['cat_age'] ?></td>
                            <td class="deux withBorder licenceAd"><?= $adherant['type_licence'] ?></td>
                            <td class="deux withBorder activityAd"><?= $adherant['liste_activite'] ?></td>
                            <td class="deux withBorder medicalAd"><a href="<?= $adherant['certif_medical'] ?>" target="_blank">Certificat <?= $adherant['nom'] ?>-<?= $adherant['prenom'] ?></a></td>
                            <td class="deux withBorder identityRepLegalAd"><?= $adherant['identite_rep_legal'] ?></td>
                            <td class="deux withBorder typeRepLegalAd"><?= $adherant['type_rep_legal'] ?></td>
                            <td class="deux withBorder bloodAd"><?= $adherant['autorise_prelevement'] ?></td>
                            <td class="deux withBorder hospitalAd"><?= $adherant['autorise_hospitalisation'] ?></td>
                            <td class="trois withBorder RiAd"><?= $adherant['reglement_interieur'] ?></td>
                            <td class="trois withBorder imageAd"><?= $adherant['droit_image'] ?></td>
                            <td class="trois withBorder cnilAd"><?= $adherant['loi_informatique'] ?></td>
                            <td class="trois withBorder insuranceAd"><?= $adherant['assurance'] ?></td>
                            <td class="trois withBorder paymentAd"><?= $adherant['etat_paiement'] ?></td>
                            <td class="trois withBorder sessionKeyAd"><?= $adherant['cle_de_session'] ?></td>                             
                            <td class="id numId hideCol"><?= $adherant['id'] ?></td>
						</tr>
							<?php

						}
					?>
					</tbody>
				</table>
			</div>
            <div class="valid_delet">selectionné(s): 
                <span class="delete deleteNew">supprimer</span> <span class="validate validateNew">accepter</span>
            </div>
			<h3 class="titleTable">reinscription / mutation</h3>
			<div id="bdd2" class="classBdd">	
				<table id="tableToPrint2" class="classTable">
					<thead id='lol2'>
						<tr class="firstline">	
                            <th class="forCheckbox"></th>
							<th class="zero withBorder" colspan=1>SIFFA</th>							
							<th class="zero identiteUn withBorder" colspan=2>IDENTITE</th>
							<th class="un identiteDeux withBorder" colspan=3>IDENTITE</th>
							<th class="un withBorder" colspan=2>INFO INSCRIPTION</th>			
							<th class="un withBorder" colspan=6>COORDONNEES</th>
							<th class="deux withBorder" colspan=4>INSCRIPTION</th>
							<th class="deux withBorder" colspan=4>MINEUR</th>
							<th class="trois withBorder" colspan=4>ACCORD</th>
							<th class="trois withBorder" colspan=2>INFO PAIEMENT</th>
                            <th class="id hideCol"></th>
						</tr>	
						<tr class="secondline">
                            <th class="forCheckbox"></th>
							<th class="zero withBorder">N° LICENCE</th>
							<th class="zero withBorder">NOM</th>
							<th class="zero withBorder">PRENOM</th>
							<th class="un withBorder">SEXE</th>
							<th class="un withBorder">DATE DE <br/>NAISSANCE</th>
							<th class="un withBorder">NATIONALITE</th>
							<th class="un withBorder">DATE</th>
							<th class="un withBorder">SAISON</th>							
							<th class="un withBorder">ADRESSE</th>
							<th class="un withBorder">CODE POSTAL</th>
							<th class="un withBorder">VILLE</th>
							<th class="un withBorder">E-MAIL</th>
							<th class="un withBorder" colspan=2>TELEPHONE</th>
							<th class="deux withBorder">CATEGORIE</th>
							<th class="deux withBorder">TYPE DE LICENCE</th>
							<th class="deux withBorder">LISTE DES ACTIVITES</th>
							<th class="deux withBorder">LIEN VERS <br/>LE CERTIFICAT MEDICAL</th>
							<th class="deux withBorder">IDENTITE <br/>REPRESENTANT LEGAL</th>
							<th class="deux withBorder">LIEN <br/>AVEC ADHERENT</th>
							<th class="deux withBorder">AUTORISATION <br/>PRELEVEMENT SANGUIN</th>
							<th class="deux withBorder">AUTORISATION <br/>HOSPITALISATION</th>
							<th class="trois withBorder">REGLEMENT <br/>INTERIEUR</th>
							<th class="trois withBorder">DROIT A <br/>L'IMAGE</th>
							<th class="trois withBorder">CNIL</th>
							<th class="trois withBorder">ASSURANCE</th>
							<th class="trois withBorder">ETAT PAIEMENT</th>
							<th class="trois withBorder">CLE PAIEMENT</th>
                            <th class="id hideCol"></th>
						</tr>			
					</thead>
					<tbody id='corpTable2'>
					<?php
						while ($adherant2 = $listAdherentReEnrollement->fetch())
						{
							?>
						 <tr>
                            <td class="forCheckbox"><input  class="checkboxRe" type="checkbox"></td>
                            <td class="zero withBorder numAd"><?= $adherant2['num_lic_siffa'] ?></td>
                            <td class="zero withBorder nameAd"><?= $adherant2['nom'] ?></td>
                            <td class="zero withBorder nicknameAd"><?= $adherant2['prenom'] ?></td>
                            <td class="un withBorder sexeAd"><?= $adherant2['sexe'] ?></td>
                            <td class="un withBorder birthdayAd"><?= implode('/',array_reverse(explode('-',$adherant2['date_naissance']))) ?></td>
                            <td class="un withBorder countryAd"><?= $adherant2['nationalité'] ?></td>
                            <td class="un withBorder subscriptionDateAd"><?= implode('/',array_reverse(explode('-',$adherant2['date_inscription']))) ?></td>
                            <td class="un withBorder seasonAd"><?= $adherant2['saison'] ?></td>
                            <td class="un withBorder adressAd"><?= $adherant2['adresse'] ?></td>
                            <td class="un withBorder zipCodeAd"><?= $adherant2['code_postal'] ?></td>
                            <td class="un withBorder cityAd"><?= $adherant2['ville'] ?></td>
                            <td class="un withBorder emailAd"><?= $adherant2['email'] ?></td>
                            <td class="un withBorder mobileAd"><?= $adherant2['tel_portable'] ?></td>
                            <td class="un withBorder fixeAd"><?= $adherant2['tel_fixe'] ?></td>
                            <td class="deux withBorder ageAd"><?= $adherant2['cat_age'] ?></td>
                            <td class="deux withBorder licenceAd"><?= $adherant2['type_licence'] ?></td>
                            <td class="deux withBorder activityAd"><?= $adherant2['liste_activite'] ?></td>
                            <td class="deux withBorder medicalAd"><a href="<?= $adherant2['certif_medical'] ?>" target="_blank">Certificat <?= $adherant2['nom'] ?>-<?= $adherant2['prenom'] ?></a></td>
                            <td class="deux withBorder identityRepLegalAd"><?= $adherant2['identite_rep_legal'] ?></td>
                            <td class="deux withBorder typeRepLegalAd"><?= $adherant2['type_rep_legal'] ?></td>
                            <td class="deux withBorder bloodAd"><?= $adherant2['autorise_prelevement'] ?></td>
                            <td class="deux withBorder hospitalAd"><?= $adherant2['autorise_hospitalisation'] ?></td>
                            <td class="trois withBorder RiAd"><?= $adherant2['reglement_interieur'] ?></td>
                            <td class="trois withBorder imageAd"><?= $adherant2['droit_image'] ?></td>
                            <td class="trois withBorder cnilAd"><?= $adherant2['loi_informatique'] ?></td>
                            <td class="trois withBorder insuranceAd"><?= $adherant2['assurance'] ?></td>
                            <td class="trois withBorder paymentAd"><?= $adherant2['etat_paiement'] ?></td>
                            <td class="trois withBorder sessionKeyAd"><?= $adherant2['cle_de_session'] ?></td>                            
                            <td class="id numId hideCol"><?= $adherant2['id'] ?></td>
						</tr>
							<?php

						}
					?>
					</tbody>
				</table>
			</div>
            <div class="valid_delet">selectionné(s): 
                <span class="delete deleteRe">supprimer</span> <span class="validate validateRe">accepter</span>
            </div>
		</section>
<?php $content = ob_get_clean(); ?>
<script src="public/js/jquery.min.js"></script>
<script src="public/js/addDeleteWaitingAdh.js"></script>
<link rel="stylesheet" type="text/css" href="public/css/listView.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="public/css/listPrint.css" media="print"/>
<?php require('template.php'); ?>
