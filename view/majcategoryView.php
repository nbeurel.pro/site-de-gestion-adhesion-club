<?php 
$pageTitle = "Liste des adhérents changeant de catégorie";
$title = "ACPA " .$pageTitle;

$selectCat = "<select class='selectCat'>
                <option value=''></option>";
while ($categorie = $category->fetch())
{
    $selectCat = $selectCat . "<option value='" .$categorie['id_categorie_age']. "'>" .$categorie['lib_categorie_age']." - ".$categorie['conditions']. "</option>";
}

$selectCat = $selectCat . "</select>";

session_start();
ob_start(); ?>
<section id="bdd">
    <h2><?= $pageTitle ?></h2>
    <form method="post" action="index.php">
        <div id="affichTableAdh" class="printable">
            <table id="tableListAdherents"  class="tableList">
                <thead>
                    <tr class="test">
                        <th>NOM </th>
                        <th>PRENOM</th>
                        <th>DATE DE NAISSANCE</th>
                        <th class="activite">Category d'age actuelle</th>
                        <th class="activite">Nouvelle category d'age</th>
                        <th class="hideCol"></th>
                    </tr>			
                </thead>
                <tbody id="table">
                    <?php
                        while ($adherent = $listAdherents->fetch())
                        { ?>
                            <tr class="modifiCat"><td><?= $adherent['lib_nom'] ?></td>
                            <td><?= $adherent['lib_prenom'] ?></td>
                            <td class="dtnaissance"><?= $adherent['dt_naissance'] ?></td>
                            <td class="oldCat"><?= $adherent['id_categorie_age'] ?></td>
                            <td class="newCat"><?= $selectCat ?></td>
                            <td class="hidecol"><?= $adherent['id_adhesion'] ?></td></tr>
                     <?php   }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="control">
            <button type="button" class="buttonAdherent modify">Enregistrer</button>
            <a href="index.php?view=categories"><button type="button" class="buttonAdherent close">Retour</button></a>
        </div >
    </form>
</section>
<?php $content = ob_get_clean(); ?>
<link rel="stylesheet" type="text/css" href="public/css/adherentView.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="public/css/indexView.css" media="screen"/>		
<link rel="stylesheet" type="text/css" href="public/css/indexPrint.css" media="print"/>

<script src="public/js/jquery.min.js"></script>
<script src="public/js/majCatAge.js"></script>		

<?php require('view/template.php'); ?>