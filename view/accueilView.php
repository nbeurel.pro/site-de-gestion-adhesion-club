<?php $title = "ACPA accueil" ?>
<?php ob_start(); ?>  
	<div class="mainMenu">
		<a href="index.php?view=adherents" class="buttonLicence">
			<i class="fas fas fa-user iconButtonLicence"></i>
			<p class="numberButtonLicence"><?= $numberAdherents ?></p>
			<p title="Pour la <?= $seasonActiv ?>" class="labelButtonLicence">Licenciés</p>		
		</a>
		<a href="index.php?view=contacts" class="buttonLicence">
			<i class="fas fa-id-card iconButtonLicence"></i>
			<p class="numberButtonLicence"><?= $numberContacts ?></p>
			<p class="labelButtonLicence">Contacts</p> 
		</a>
		<a href="#" class="buttonLicence">
			<i class="fas fa-euro-sign iconButtonLicence"></i>
			<p class="numberButtonLicence"><?= $numberPayments ?></p>
			<p id="plop" class="labelButtonLicence">Paiements</p> 
		</a>
	</div>
<?php $content = ob_get_clean(); ?>
<?php require('template.php'); ?>
