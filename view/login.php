<!DOCTYPE html>
<html lang="en">
    <?php 
        $siteKey = G_SITE_KEY;
    ?>
	<head>		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>connexion</title>
        <script src="https://www.google.com/recaptcha/api.js"></script>
        <script>
           function onSubmit(token) {
             document.getElementById("formIdentificate").submit();
           }
         </script>
		<link rel="stylesheet" href="public/css/login.css" media="screen"/>
        
                
	</head>
	<body>
        <header>
            <div class="conteneurImage">
                <img src="https://acpays-ancenis.fr/storage/2019/03/cropped-ACPA_d&#xE9;rog-1.png">
            </div>
			<div class="titre">
				<h1>ACPA</h1>
				<h2>Gestion du club</h2>
			</div>	
		</header>
        <section class="textPresentation">
            <p>Merci de vous connecter</p>
        </section>
        <section class="sectionMsgAlert">
            <p class="msgAlert"><?= $msg ?></p>
        </section>
		<section id="formIdentification">
			<form id="formIdentificate" action="./index.php" method="post">
				<h2>IDENTIFICATION</h2>
				<div class="fieldGroup fieldLogin" id="fieldLogin">
					<label for="inputLogin" class="fieldLabel" id="labelLogin">identifiant</label>
					<br />
					<input type="text" class="fieldInput" id="inputLogin" placeholder="identifiant" autocomplete="username"  name="login"/>
				</div>
				<div class="fieldGroup fieldPwd" id="fieldPwd">
					<label for="inputPwd" class="fieldLabel" id="labelPwd" >mot de passe</label>
					<br />
					<input type="password" class="fieldInput" id="inputPwd" placeholder="password" autocomplete="current-password"  name="pwd"/>
				</div>
				<div class="fieldGroup fieldButton" id="buttonValide">
					<!--<button class="buttons" type="submit">valider</button>-->
                    <button class="g-recaptcha buttons" data-sitekey="<?= $siteKey ?>" data-callback='onSubmit' data-action='submit'  type="submit">valider</button>
				</div>
			</form>
            
		</section>
        <footer>
            <img class="logoAcpaText" src="public/images/Logo_ACPAtext.png">
        </footer>
	</body>	
    <script src="public/js/jquery.min.js"></script>
    <script src="public/js/accueil.js"></script>	
</html>
