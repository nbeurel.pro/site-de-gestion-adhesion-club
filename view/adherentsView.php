<?php 
$pageTitle = "Liste des adhérents";
$title = "ACPA " .$pageTitle; 
session_start();
ob_start(); ?>
	<section class="messagepop pop">
			<div id="fichIdentite" class="popInContener">
				<form class="page" id="fichAdherent" >
                </form>
			</div>
		</section>
		<section id="bdd">
			<h2><?= $pageTitle ?></h2>
			<form method="post" action="index.php">
				<div id="control">
					<select id="selectSaison" class="selectChoice" name="var_saison"  >
						<?php 
						while ($seasonView = $season->fetch())
						{
							if ($seasonView['yn_saison_ouverte'] = "oui")
							{
						?>
								<option value="<?= $seasonView['id_saison'] ?>" selected><?= $seasonView['lib_saison'] ?></option>;
						<?php			
							}
							else
							{
						?>
								<option value="<?= $seasonView['id_saison'] ?>"><?= $seasonView['lib_saison'] ?></option>;
						<?php
							}
						}
						?>			
					</select>
					<select id="selectCategorieAge" class="selectChoice" name="var_catage" >
						<option value="%" >Catégories</option>
							<?php while ($categoryView = $category->fetch())
								{
									echo '<option value="' . $categoryView['id_categorie_age'] . '">' . $categoryView['lib_categorie_age'] . '</option>';
								}
							?>		
					</select>
					<!--<div id="selectSiffaOnly" class="selectChoice">
						<label for="licencie">licencié SIFFA uniquement:</label>
						<input id="ouiSIFFA" type="checkbox" value="oui" name="licencie" checked="checked">
					</div>-->
					<button class="logoPrintPaper logoAction print" title="imprimer" id="printAdherents">
						<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-printer" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
						  <path d="M11 2H5a1 1 0 0 0-1 1v2H3V3a2 2 0 0 1 2-2h6a2 2 0 0 1 2 2v2h-1V3a1 1 0 0 0-1-1zm3 4H2a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h1v1H2a2 2 0 0 1-2-2V7a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-1v-1h1a1 1 0 0 0 1-1V7a1 1 0 0 0-1-1z"/>
						  <path fill-rule="evenodd" d="M11 9H5a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1zM5 8a2 2 0 0 0-2 2v3a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2v-3a2 2 0 0 0-2-2H5z"/>
						  <path d="M3 7.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z"/>
						</svg>
					</button>
					<button class="logoDL logoAction excelize" id="excelizeAdherents" title="exporter">
						<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-download" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
						  <path fill-rule="evenodd" d="M.5 8a.5.5 0 0 1 .5.5V12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V8.5a.5.5 0 0 1 1 0V12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V8.5A.5.5 0 0 1 .5 8z"/>
						  <path fill-rule="evenodd" d="M5 7.5a.5.5 0 0 1 .707 0L8 9.793 10.293 7.5a.5.5 0 1 1 .707.707l-2.646 2.647a.5.5 0 0 1-.708 0L5 8.207A.5.5 0 0 1 5 7.5z"/>
						  <path fill-rule="evenodd" d="M8 1a.5.5 0 0 1 .5.5v8a.5.5 0 0 1-1 0v-8A.5.5 0 0 1 8 1z"/>
						</svg>
					</button>
					<a href="index.php?view=inscription" id="listNonValide" class="nouvAdhesion" >
						<span class="spanButton" >nouvelle adhésion</span>
						<span class="notification"><?= $numberReEnrollement ?></span>
					</a>
				</div>
				<div id="affichTable" class="printable">
					<table id="tableListAdherents"  class="tableList">
						<thead>
							<tr class="test">
								<th>NOM
                                    <span class="orderer">
                                        <span class="order asc orderName" id="orderNameAsc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 11L8 5.519 12.796 11H3.204zm-.753-.659l4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z"/>
                                            </svg>
                                        </span>
                                        <span class="order desc orderName" id="orderNameDesc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/>
                                            </svg>
                                        </span>
                                    </span>
                                </th>
								<th>PRENOM
                                    <span class="orderer">
                                        <span class="order asc orderFirstName" id="orderFirstNameAsc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 11L8 5.519 12.796 11H3.204zm-.753-.659l4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z"/>
                                            </svg>
                                        </span>
                                        <span class="order desc orderFirstName" id="orderFirstNameDesc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/>
                                            </svg>
                                        </span>
                                    </span>
                                </th>
								<th>N°LICENCES
                                    <span class="orderer">
                                        <span class="order asc orderNum" id="orderNumAsc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 11L8 5.519 12.796 11H3.204zm-.753-.659l4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z"/>
                                            </svg>
                                        </span>
                                        <span class="order desc orderNum" id="orderNumDesc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/>
                                            </svg>
                                        </span>
                                    </span>
                                </th>
								<th class="activite">ACTIVITE                                
                                    <span class="orderer">
                                        <span class="order asc orderActivity"  id="orderActivityAsc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 11L8 5.519 12.796 11H3.204zm-.753-.659l4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z"/>
                                            </svg>
                                        </span>
                                        <span class="order desc orderActivity"  id="orderActivityDesc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/>
                                            </svg>
                                        </span>
                                    </span>
                                </th>
								<th>COURRIEL
                                    <span class="orderer">
                                        <span class="order asc orderMail" id="orderMelAsc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 11L8 5.519 12.796 11H3.204zm-.753-.659l4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z"/>
                                            </svg>
                                        </span>
                                        <span class="order desc orderMail"  id="orderMelDesc">
                                            <svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fill-rule="evenodd" d="M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/>
                                            </svg>
                                        </span>
                                    </span>
                                </th>
								<th class="derCol"></th>
							</tr>			
							<tr class="filtres">
								<th class="test">
									<input class="seek_table" type="text" name="var_nom" id="input_nom"/>
								</th>
								<th class="test">
									<input class="seek_table" type="text" name="var_prenom" id="input_prenom"/>
								</th>
								<th class="test">
									<input class="seek_table" type="text" name="var_numlice" id="input_numlice"/>
								</th>
								<th class="test activite">
									<select class="seek_table" name="var_activity" id="select_activity">
										<option value="%"></option>
											<?php while ($activ = $activity->fetch())
												{
													echo '<option value="' . $activ['id_activite'] . '">' . $activ['lib_activite'] . '</option>
													';
												}
											?>
									</select>
								</th>
								<th class="test">
									<input class="seek_table" type="text" name="var_mel" id="input_mel"/>
								</th>
								<th class="derCol"></th>
							</tr>
						</thead>
						<tbody id="table">
						</tbody>
					</table>
				</div>		
			</form>
		</section>
<?php $content = ob_get_clean(); ?>
<link rel="stylesheet" type="text/css" href="public/css/adherentView.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="public/css/indexView.css" media="screen"/>		
<link rel="stylesheet" type="text/css" href="public/css/indexPrint.css" media="print"/>

<script src="public/js/jquery.min.js"></script>	
<script src="public/js/initiAdherents.js"></script>		
<?php require('view/template.php'); ?>
