<?php

$pageTitle = "Gestion des connexions";
$title = "ACPA " . $pageTitle; 
ob_start(); 
?>

<section id="bdd">
    <h2><?= $pageTitle ?></h2>
    <div class="information">Cet outil à pour but de créer/modifier les informations de connexion (login - mot de passe) d'une personnes qui existe déja dans la base de données à l'aide de son numéro d'identification
        <span class="placeInfobulle">
            <span class="infobulle">
                <span class="contentinfobulle">
                    <h3>trouvez ce numero:</h3> 
                    <ul>
                        <li class='niv0'><h4>Cas d'un contact:</h4>
                            <ul>
                                <li class='niv1'>Sur la page contacts</li>
                                <li class='niv1'>Séléctionner le contact voulu</li>
                                <li class='niv1'>Cliquer sur l'icone en forme de crayon</li>
                                <li class='niv1'>Copier le numeros du contact</li>
                            </ul>
                        </li>
                        <li class='niv0'><h4>Cas d'un adhérent:</h4>
                            <ul>
                                <li class='niv1'>Sur la page adhérents</li>
                                <li class='niv1'>Séléctionner le contact voulu</li>
                                <li class='niv1'>Cliquer sur l'icone en forme tête</li>
                                <li class='niv1'>Copier le numeros de l'adherent</li>
                            </ul>
                        </li>
                    </ul>
                </span>
            </span>
        </span>        
        <span class="exposant" title="ou trouver ce numero?">
            <svg width="10px" height="10px" viewBox="0 0 16 16" class="bi bi-info-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
              <path d="M8.93 6.588l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588z"/>
              <circle cx="8" cy="4.5" r="1"/>
            </svg>
        </span>        
    </div>    
	<div>
		<div class="group">
			<label for="idPersonneTool" class="labelAdherent">N° identification</label>
			<input type="number" name="idPersonneTool"  id="idPersonneTool" class="inputAdherent">
        </div>
        <span id="result">
            <div class="group">
                <label for="idConnexion" class="labelAdherent">login</label>
                <input type="text" name="idConnexion"  class="inputAdherent">
            </div>
            <div class="group">
                <label for="pass" class="labelAdherent">mot de passe</label>
                <input type="text" name="pass"  class="inputAdherent">
            </div>
        </span>
		<div>
			<button id="enregistrement" class="buttonAdherent modify">ENREGISTRER</button>
		</div>
	</div>
	<?php
$content = ob_get_clean(); ?>
<link rel="stylesheet" type="text/css" href="public/css/indexView.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="public/css/adherentView.css" media="screen"/>
<?php require('view/template.php'); ?>