<!DOCTYPE html>
<html lang="en">
	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?= $title ?></title>
		
		<link rel="stylesheet" href="public/css/reset.css" media="screen"/>
		<link rel="stylesheet" href="public/css/template.css" media="screen"/>
        
		<script src="https://kit.fontawesome.com/a44469ae65.js" crossorigin="anonymous"></script>
		
	</head>
	<body>	
		<header>
			<div class="titleP1">
				<h1> Gestion du Club</h1>  
			</div>
			<div class="principal fas fa-user-circle logoBonhomme">
                <div class="child">
                    <ul>
                        <li><a href="#">mon compte</a></li>
                        <li><a href="./index.php?view=logout">Deconnexion</a></li>
                    </ul>
                </div>
            </div>
		</header>
		<nav>
			<ul>
                <li>
                    <a href="./">
                        <img src="public/images/Logo_ACPA1.png">
                    </a>
                </li>
                <li id="navButton1" class="principal-nav">
                    <div class="fas fa-address-book navLogo menu"></div>
                    <p class="navLabel">Utilisateurs</p>
                    <span class="child-nav">
                        <ul>
                            <li><a class="link-nav" href="./index.php?view=adherents">adherents</a></li>
                            <li><a class="link-nav" href="./index.php?view=contacts">contacts</a></li>
                        </ul>
                    </span>                    
                </li>
                <li id="navButton2" class="principal-nav">
                    <div class=" fas fa-folder-open navLogo menu"></div>
                    <p class="navLabel">Ged</p>
                    <span class="child-nav">
                        <ul>
                            <li><a class="link-nav" href="#">comming soon</a></li>
                        </ul>
                    </span>
                </li>
                <li id="navButton3" class="principal-nav">
                    <div class="fas fa-file-signature navLogo menu"> </div>
                    <p class="navLabel">Adhésion</p>
                    <span class="child-nav">
                        <ul>
                            <li><a class="link-nav" href="#">comming soon</a></li>
                        </ul>
                    </span>                   
                </li>
                <li id="navButton4" class="principal-nav">
                    <div class="fas fa-toolbox navLogo menu"></div>
                    <p class="navLabel">Paramètres</p>
                    <span class="child-nav">
                        <ul>
                            <li><a class="link-nav" href="./index.php?view=createLogin">gestion des identifiants</a></li>
                            <li><a class="link-nav" href="./index.php?view=saisons">gestion des saisons</a></li>
                            <li><a class="link-nav" href="./index.php?view=categories">gestion des catégories</a></li>
                            <li><a class="link-nav" href="./index.php?view=moyenPayement">gestion des Moyen de payement</a></li>
                        </ul>
                    </span>
                    
                </li>
            </ul>
		</nav>
		<section id="section">		
			<?= $content ?>
		</section>			
		<footer>
			<img class="logoAcpaText" src="public/images/Logo_ACPAtext.png">
		</footer>
		
		<script src="public/js/jquery.min.js"></script>	
		<script src="public/js/commande.js"></script>
        
		

	</body>
</html>
