jQuery(function() {

    /**
     * ajout d'un listener sur le boutons deleteNew
     * definit la variable selector et appel la fonction Fdelete
     */
    $('.deleteNew').click(function(){
        selector = "checkboxNew";
        Fdelete(selector);
    })
    /**
     *  ajout d'un listener sur le boutons deleteRe
     * definit la variable selector et appel la fonction Fdelete
     */
    $('.deleteRe').click(function(){
        selector = "checkboxRe";
        Fdelete(selector);
    })

    /**
     *  ajout d'un listener sur le boutons validateNew
     * definit la variable selector et appel la fonction Fvalidate
     */
     $('.validateNew').click(function(){
        selector = "checkboxNew";
        Fvalidate(selector);
    })

    /**
     *  ajout d'un listener sur le boutons validateRe
     * definit la variable selector et appel la fonction Fvalidate
     */
    $('.validateRe').click(function(){
        selector = "checkboxRe";
        Fvalidate(selector);
    })

    /**
     * fonction qui permet de supprimer les lignes selectionnées et de supprimer l'entrée dans la table WordPress
     * @param selector String definit sur quel type de ligne doit s'appliqué la fonction
     * @constructor
     */
    function Fdelete(selector)
    {
        if(!$('.' + selector).is(":checked")){
            alert('aucune ligne selectionné')
        }else{
            if(confirm('êtes vous sur de vouloir supprimer les lignes séléctionnées')){
                $('.' + selector).each(function(){
                    if($(this).is(":checked")){
                        ligne = $(this).parent().siblings('.numId').html();
                        console.log(ligne)
                        $.ajax({
                            url: "public/ajax/deleteAdherent.php",
                            type: "POST",
                            data: {
                                'id' : ligne
                            }
                        }).done(function(reponse){		
                            console.log(reponse);
                            alert('les lignes on été supprimées')
                            window.location = "index.php?view=inscription";
                        });

                    }
                })
                //alert('les lignes on été supprimées')
                window.location = "index.php?view=inscription";
            }
        }        
    }

    /**
     * function qui permet de valider les ligne selectionné
     * - recupère les informations de l'adherent stocké sur la page
     * - créé un nouvel adherent dans la table ACPA s'il n'existe pas
     * - mets a jours l'adherent s'il existe
     * - supprime la ligne et l'entrée dans la table WordPress
     * @constructor
     */
     function Fvalidate()
    {
        if(!$('.' + selector).is(":checked")){
            alert('aucune ligne selectionné')
        }else{
            if(confirm('êtes vous sur de vouloir accepter les lignes séléctionnées')){
                console.log('ok');
                $('.' + selector).each(function(){
                    if($(this).is(":checked")){

                        /*
                         * recupération des informations stockées dans le tableau
                         */
                        ligne = $(this).parent().siblings('.numId').html();
                        if(selector == 'checkboxRe' ){
                            numlicenceSiffa = $(this).parent().siblings('.numAd').html();
                            selector = 'old';
                        } else {
                            numlicenceSiffa = 0;
                            selector = 'new';
                        }
                        nameAdherent = $(this).parent().siblings('.nameAd').html();
                        nicknameAdherent = $(this).parent().siblings('.nicknameAd').html();
                        sexeAdherent = $(this).parent().siblings('.sexeAd').html();
                        birthdayAdherentWrong = $(this).parent().siblings('.birthdayAd').html();
                        birthdayAdherentWrong = birthdayAdherentWrong.split('/');
                        birthdayAdherent = birthdayAdherentWrong[2] + '-' + birthdayAdherentWrong[1] + '-' + birthdayAdherentWrong[0];
                        countryAdherent = $(this).parent().siblings('.countryAd').html();
                        dayAdhesionWrong = $(this).parent().siblings('.subscriptionDateAd').html();
                        dayAdhesionWrong = dayAdhesionWrong.split('/');
                        dayAdhesion = dayAdhesionWrong[2] + '-' + dayAdhesionWrong[1] + '-' + dayAdhesionWrong[0];
                        seasonAdhesion = $(this).parent().siblings('.seasonAd').html();
                        adressAdherent = $(this).parent().siblings('.adressAd').html();
                        zipCodeAdherent = $(this).parent().siblings('.zipCodeAd').html();
                        cityAdherent = $(this).parent().siblings('.cityAd').html();
                        eMailAdherent = $(this).parent().siblings('.emailAd').html();
                        mobilePhoneAdherent = $(this).parent().siblings('.mobileAd').html();
                        fixePhoneAdherent = $(this).parent().siblings('.fixeAd').html();
                        categorieAgeAdherent = $(this).parent().siblings('.ageAd').html();
                        typeLicenceAdherent = $(this).parent().siblings('.licenceAd').html();
                        activityAdherent = $(this).parent().siblings('.activityAd').html();
                        
                        medicalAdressAdherent = $(this).parent().siblings('.medicalAd').children().attr('href');                        
                        lengthMedical = medicalAdressAdherent.length;
                        start = medicalAdressAdherent.lastIndexOf("/");
                        nameMedical = medicalAdressAdherent.slice(start+1, lengthMedical);                        
                        medicalAdressAdherent = "../../../wp-content/uploads/certificats_medical/" + nameMedical;
                        
                        identityParentalAdvisory = $(this).parent().siblings('.identityRepLegalAd').html();
                        typeParentalAdvisory = $(this).parent().siblings('.typeRepLegalAd').html();
                        if(!(identityParentalAdvisory == "")){
                            identityYoungAdherent = nameAdherent + " " + nicknameAdherent;                            
                        }else{
                            identityYoungAdherent = "";
                        }
                        
                        bloodAutorisation = $(this).parent().siblings('.bloodAd').html(); 
                        hospitalisationAutorisation = $(this).parent().siblings('.hospitalAd').html();
                        
                        ruleAcceptance = $(this).parent().siblings('.RiAd').html();
                        if(ruleAcceptance == "oui"){
                            ruleAcceptance = 1;
                        }else{
                            ruleAcceptance = 0;
                        }
                        
                        imageAcceptance = $(this).parent().siblings('.imageAd').html();
                        if(imageAcceptance == "oui"){
                            imageAcceptance = 1;
                        }else{
                            imageAcceptance = 0;
                        }
                            
                        cnilAcceptance = $(this).parent().siblings('.cnilAd').html();
                        if(cnilAcceptance == "oui"){
                            cnilAcceptance = 1;
                        }else{
                            cnilAcceptance = 0;
                        }
                        
                        insuranceAcceptance = $(this).parent().siblings('.insuranceAd').html();
                        if(insuranceAcceptance == "oui"){
                            insuranceAcceptance = 1;
                        }else{
                            insuranceAcceptance = 0;
                        }
                        
                        PaymentState = $(this).parent().siblings('.paymentAd').html();
                        paymentKey = $(this).parent().siblings('.sessionKeyAd').html();


                        //affichage des info dans la console pour verification
                        console.log(selector + '\n' + numlicenceSiffa + '\n' + nameAdherent + '\n' + nicknameAdherent + '\n' + sexeAdherent + '\n' + birthdayAdherent + '\n' + countryAdherent + '\n' + dayAdhesion + '\n' + seasonAdhesion + '\n' + adressAdherent + '\n' + zipCodeAdherent + '\n' + cityAdherent + '\n' + eMailAdherent + '\n' + mobilePhoneAdherent + '\n' + fixePhoneAdherent + '\n' + categorieAgeAdherent + '\n' + typeLicenceAdherent + '\n' + activityAdherent + '\n' + medicalAdressAdherent + '\n' + identityParentalAdvisory + '\n' + typeParentalAdvisory + '\n' + identityYoungAdherent + '\n' + bloodAutorisation + '\n' + hospitalisationAutorisation + '\n' + ruleAcceptance + '\n' + imageAcceptance + '\n' + cnilAcceptance + '\n' + insuranceAcceptance + '\n' + PaymentState + '\n' + paymentKey);

                        // affichage d'un message dans la console pour verification des étapes
                        console.log('envoi de la requete');

                        /*
                         * requete ajax d'ajout de l'adherent
                         */
                        $.ajax({
                            url: "public/ajax/injectionAdhesion.php",
                            type: "POST",
                            data: {
                                'newOld' : selector,
                                'numSiffa' : numlicenceSiffa,
                                'nameAdherent'  :  nameAdherent,
                                'nicknameAdherent'  :  nicknameAdherent,
                                'sexeAdherent'  :  sexeAdherent,
                                'birthdayAdherent'  :  birthdayAdherent,
                                'countryAdherent'  :  countryAdherent,
                                'dayAdhesion'  :  dayAdhesion,
                                'seasonAdhesion'  :  seasonAdhesion,
                                'adressAdherent'  :  adressAdherent,
                                'zipCodeAdherent'  :  zipCodeAdherent,
                                'cityAdherent'  :  cityAdherent,
                                'eMailAdherent'  :  eMailAdherent,
                                'mobilePhoneAdherent'  :  mobilePhoneAdherent,
                                'fixePhoneAdherent'  :  fixePhoneAdherent,
                                'categorieAgeAdherent'  :  categorieAgeAdherent,
                                'typeLicenceAdherent'  :  typeLicenceAdherent,
                                'activityAdherent'  :  activityAdherent,
                                'medicalAdressAdherent'  :  medicalAdressAdherent,
                                'identityParentalAdvisory'  :  identityParentalAdvisory,
                                'typeParentalAdvisory'  :  typeParentalAdvisory,
                                'identityYoungAdherent'  :  identityYoungAdherent,
                                'bloodAutorisation'  :  bloodAutorisation,
                                'hospitalisationAutorisation'  :  hospitalisationAutorisation,
                                'ruleAcceptance'  :  ruleAcceptance,
                                'imageAcceptance'  :  imageAcceptance,
                                'cnilAcceptance'  :  cnilAcceptance,
                                'insuranceAcceptance'  :  insuranceAcceptance,
                                'PaymentState'  :  PaymentState,
                                'paymentKey'  :  paymentKey
                            },
                            success : function(code_html, statut){		
                    	        console.log('la requete est bien envoyé');
                                console.log('code html= ' + code_html);                                
                                console.log('statut= ' + statut); 
                            },
                            error: function(resultat, statut, erreur){
                                console.log('problème');
                                console.log('resultat= ' + resultat);   
                                console.log('statut= ' + statut); 
                                console.log('erreur= ' + erreur); 
                                return false;
                            },
                            complete : function(resultat, statut){
                                console.log('la requete est complete');
                                console.log('resultat=' + resultat);                                
                                console.log('statut=' + statut);
                            }
                        }).done(function(reponse){		
                            msg=reponse;
                            alert(reponse);
                            index1 = msg.indexOf('cet adherent n\'existe pas');
                            index2 = msg.indexOf('cet adherent semble déjà existé: nom, prénom et année de naissance identique');
                            index3 = msg.indexOf('la base de données a été mise à jours');
                            //alert('index1' + index1 + "\nindex2" + index2+ "\nindex3" + index3);
                            if(index1 == -1){
                                if(index2 != 0){
                                    if(index3 == 0){
                                        $.ajax({
                                            url: "public/ajax/deleteLine.php",
                                            type: "POST",
                                            data: {
                                                'id' : ligne
                                            }
                                        });
                                        window.location = "index.php?view=inscription";
                                    }
                                }else{
                                   if(confirm('voulez vous mettre a jours cet adhérent')){
                                        selector = 'old';
                                        $.ajax({
                                            url: "public/ajax/injectionAdhesion.php",
                                            type: "POST",
                                            data: {
                                                'newOld' : selector,
                                                'numSiffa' : numlicenceSiffa,
                                                'nameAdherent'  :  nameAdherent,
                                                'nicknameAdherent'  :  nicknameAdherent,
                                                'sexeAdherent'  :  sexeAdherent,
                                                'birthdayAdherent'  :  birthdayAdherent,
                                                'countryAdherent'  :  countryAdherent,
                                                'dayAdhesion'  :  dayAdhesion,
                                                'seasonAdhesion'  :  seasonAdhesion,
                                                'adressAdherent'  :  adressAdherent,
                                                'zipCodeAdherent'  :  zipCodeAdherent,
                                                'cityAdherent'  :  cityAdherent,
                                                'eMailAdherent'  :  eMailAdherent,
                                                'mobilePhoneAdherent'  :  mobilePhoneAdherent,
                                                'fixePhoneAdherent'  :  fixePhoneAdherent,
                                                'categorieAgeAdherent'  :  categorieAgeAdherent,
                                                'typeLicenceAdherent'  :  typeLicenceAdherent,
                                                'activityAdherent'  :  activityAdherent,
                                                'medicalAdressAdherent'  :  medicalAdressAdherent,
                                                'identityParentalAdvisory'  :  identityParentalAdvisory,
                                                'typeParentalAdvisory'  :  typeParentalAdvisory,
                                                'identityYoungAdherent'  :  identityYoungAdherent,
                                                'bloodAutorisation'  :  bloodAutorisation,
                                                'hospitalisationAutorisation'  :  hospitalisationAutorisation,
                                                'ruleAcceptance'  :  ruleAcceptance,
                                                'imageAcceptance'  :  imageAcceptance,
                                                'cnilAcceptance'  :  cnilAcceptance,
                                                'insuranceAcceptance'  :  insuranceAcceptance,
                                                'PaymentState'  :  PaymentState,
                                                'paymentKey'  :  paymentKey
                                            },
                                            success : function(code_html, statut){		
                                                console.log('la requete est bien envoyé');
                                                console.log('code html= ' + code_html);                                
                                                console.log('statut= ' + statut); 
                                            },
                                            error: function(resultat, statut, erreur){
                                                console.log('problème');
                                                console.log('resultat= ' + resultat);   
                                                console.log('statut= ' + statut); 
                                                console.log('erreur= ' + erreur); 
                                                return false;
                                            },
                                            complete : function(resultat, statut){
                                                console.log('la requete est complete');
                                                console.log('resultat=' + resultat);                                
                                                console.log('statut=' + statut);
                                            }
                                        }).done(function(reponse){		
                                            msg=reponse;
                                            alert(reponse);
                                            $.ajax({
                                                url: "public/ajax/deleteLine.php",
                                                type: "POST",
                                                data: {
                                                    'id' : ligne
                                                }
                                            });
                                            window.location = "index.php?view=inscription";
                                        });
                                    }else{
                                        alert('action annulée par l\'utilisateur');
                                    }
                                }
                            }else{
                                if(confirm('voulez vous créer un nouvel adhérent')){
                                    selector = 'new';
                                    $.ajax({
                                        url: "public/ajax/injectionAdhesion.php",
                                        type: "POST",
                                        data: {
                                            'newOld' : selector,
                                            'numSiffa' : numlicenceSiffa,
                                            'nameAdherent'  :  nameAdherent,
                                            'nicknameAdherent'  :  nicknameAdherent,
                                            'sexeAdherent'  :  sexeAdherent,
                                            'birthdayAdherent'  :  birthdayAdherent,
                                            'countryAdherent'  :  countryAdherent,
                                            'dayAdhesion'  :  dayAdhesion,
                                            'seasonAdhesion'  :  seasonAdhesion,
                                            'adressAdherent'  :  adressAdherent,
                                            'zipCodeAdherent'  :  zipCodeAdherent,
                                            'cityAdherent'  :  cityAdherent,
                                            'eMailAdherent'  :  eMailAdherent,
                                            'mobilePhoneAdherent'  :  mobilePhoneAdherent,
                                            'fixePhoneAdherent'  :  fixePhoneAdherent,
                                            'categorieAgeAdherent'  :  categorieAgeAdherent,
                                            'typeLicenceAdherent'  :  typeLicenceAdherent,
                                            'activityAdherent'  :  activityAdherent,
                                            'medicalAdressAdherent'  :  medicalAdressAdherent,
                                            'identityParentalAdvisory'  :  identityParentalAdvisory,
                                            'typeParentalAdvisory'  :  typeParentalAdvisory,
                                            'identityYoungAdherent'  :  identityYoungAdherent,
                                            'bloodAutorisation'  :  bloodAutorisation,
                                            'hospitalisationAutorisation'  :  hospitalisationAutorisation,
                                            'ruleAcceptance'  :  ruleAcceptance,
                                            'imageAcceptance'  :  imageAcceptance,
                                            'cnilAcceptance'  :  cnilAcceptance,
                                            'insuranceAcceptance'  :  insuranceAcceptance,
                                            'PaymentState'  :  PaymentState,
                                            'paymentKey'  :  paymentKey
                                        },
                                        success : function(code_html, statut){		
                                            console.log('la requete est bien envoyé');
                                            console.log('code html= ' + code_html);                                
                                            console.log('statut= ' + statut); 
                                        },
                                        error: function(resultat, statut, erreur){
                                            console.log('problème');
                                            console.log('resultat= ' + resultat);   
                                            console.log('statut= ' + statut); 
                                            console.log('erreur= ' + erreur); 
                                            return false;
                                        },
                                        complete : function(resultat, statut){
                                            console.log('la requete est complete');
                                            console.log('resultat=' + resultat);                                
                                            console.log('statut=' + statut);
                                        }
                                    }).done(function(reponse){		
                                        msg=reponse;
                                        alert(reponse);
                                        $.ajax({
                                            url: "public/ajax/deleteLine.php",
                                            type: "POST",
                                            data: {
                                                'id' : ligne
                                            }
                                        });
                                        window.location = "index.php?view=inscription";
                                    });
                                }else{
                                    alert('action annulée par l\'utilisateur');
                                }
                            }
                        });
                        
                    }
                });
                
                
            }
        }        
    }
    
    
    
    
    
});
