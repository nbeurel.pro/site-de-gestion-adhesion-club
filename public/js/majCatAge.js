jQuery(function() {
    
    
    $(".oldCat").each(function(){
        idCatAge = $(this).html();
        console.log('id='+idCatAge);
        dtNaissance = $(this).siblings(".dtnaissance").html();
        year = dtNaissance.slice(0,4);
        console.log('date= '+year);
        libCatAge = $(this).siblings(".newCat").children('.selectCat').children('option[value="'+idCatAge+'"]').html();
        console.log('nom= '+libCatAge);
        $(this).html(libCatAge);
        $(this).siblings(".newCat").children('.selectCat').children('option').each(function(){
            critere=$(this).html();
            if(critere.indexOf("après") != '-1'){
                critere=critere.slice(critere.indexOf("après")+6, critere.indexOf("après")+10);
                //console.log(critere);
                if(critere<=year){
                    $(this).prop('selected', true);
                    console.log($(this).html())
                    if($(this).html() == libCatAge)
                        {
                            $(this).parents('tr').remove();
                        }
                }
            }else if(critere.indexOf("avant") != '-1'){
                critere=critere.slice(critere.indexOf("avant")+6, critere.indexOf("avant")+10);
                //console.log(critere);
                if(critere>=year){
                    $(this).prop('selected', true);
                    console.log($(this).html())
                    if($(this).html() == libCatAge)
                        {
                            $(this).parents('tr').remove();
                        }
                }
            }else if(critere.indexOf("entre") != '-1'){
                critere1=critere.slice(critere.indexOf("entre")+6,critere.indexOf("entre")+10);
                critere2=critere.slice(critere.indexOf(" et ")+4,critere.indexOf(" et ")+8);
                //console.log(critere1 + ' - ' + critere2);
                if(critere1<=year && year<=critere2){
                    $(this).prop('selected', true);
                    console.log($(this).html())
                    if($(this).html() == libCatAge)
                        {
                            $(this).parents('tr').remove();
                        }
                }
            }
        })
    });
    
    
    $(".modify").click(function(e){
        e.preventDefault();
        retour = 0;
        $(".modifiCat").each(function(){
            idAdhesion = $(this).children(".hidecol").html();
            idNewCat = $(this).find(".selectCat").val();
            console.log(idAdhesion + " - " + idNewCat);
            action = "majCatAge";
            $.ajax({
                url: "public/ajax/gestionCategory.php",
                type: "POST",
                data: {
                    'action' : action,
                    'idCategorie' : idNewCat,
                    'idAdhesion' : idAdhesion,
                }
            }).done(function(reponse){		
                console.log(reponse);
            });
        });
        alert("Les adhesions ont été mises à jours")
        location.reload(true);
    });
    
    
});