jQuery(function() {
    
    sorting = 'orderNameAsc';
	
	$(document).ready(function(){
		majBDD(sorting);
         
	});
	
	$("input.seek_table").keyup(function(){
		majBDD(sorting);
	});
    
    $("select.seek_table").change(function(){
		majBDD(sorting);
	});
    
    $('.order').click(function(){
        
        $('.asc').html('<svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M3.204 11L8 5.519 12.796 11H3.204zm-.753-.659l4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z"/></svg>');
        $('.desc').html('<svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/></svg>');            
        if($(this).hasClass('active')){
            sorting = 'orderNameAsc';
            $('.order').removeClass('active');
        }else{
            sorting = $(this).attr('id');
            $('.order').removeClass('active');
            $(this).addClass('active');
            if($(this).hasClass('asc')){
               $(this).html('<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-up-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M7.247 4.86l-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z"/></svg>');
            }else{
                $(this).html('<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-down-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M7.247 11.14L2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/></svg>');
            }
        }
        console.log(sorting);
        majBDD(sorting);
    });
    
	function majBDD(){
        
		console.log('majBDD');
		Nom = $("#input_nom").val();
		Prenom = $("#input_prenom").val();
        Organism = $("#input_organisme").val();
		Mail = $("#input_mel").val();
        Portable = $("#input_portable").val();
		Fixe = $("#input_fixe").val();
		Adress = $("#input_adresse").val();
        Role = $("#input_role").val();
        console.log(Role);
		$.ajax({
			url: "public/ajax/createListContacts.php",
			type: "POST",
			data: {
				'name' : Nom,
				'nickName' : Prenom,
				'organism' : Organism,
				'mail' : Mail,
				'portable' : Portable,
				'fixe' : Fixe,
				'adress' : Adress,
                'role' : Role,
                'sorting' : sorting
			}
		}).done(function(reponse){		
			$("#table").html(reponse);	
			page()
		});
	}
	
		
	function page(){
		$(".link").hide();			
		$('tr').hover(function(elt){
			if($(this).find('.link').is(':visible')){
				$(this).find(".link").hide();
				//console.log('je cache');
			}else{
				$(".link").hide();
				$(this).find(".link").show();
				//console.log('je montre');
			}
			//$(this).find('.link').show();
		});
	
         $('.deleteContact').click(function(event){
             event.preventDefault();
            if(confirm('êtes vous sur de vouloir supprimer les lignes séléctionnées')){
                 ligne = $(this).parent().siblings('.numId').html();
                console.log ('ok ' + ligne);
                $.ajax({
                    url:"public/ajax/deleteContact.php",
                    type:"POST",
                    data: {
                        'NumContact' : ligne,
                    }
                }).done(function(reponse){
                    alert(reponse);
                    location.reload(true);                    
                })
            }
         });
        
        
        $(".modifContact").click(function(e){
            e.preventDefault();
            $('#bdd').addClass("fond");
            $("#numIdFocus").html($(this).parent().siblings(".numId").html());
            $("#modifNameCts").val($(this).parent().siblings(".nom").html());         
            $("#modifFirstNameCts").val($(this).parent().siblings(".prenom").html());
            org=$(this).parent().siblings(".organism").html();
            $("option:contains("+org+")").prop('selected',true); 
            $("#modifMailCts").val($(this).parent().siblings(".mel").html());
            $("#modifMobileCts").val($(this).parent().siblings(".portable").html());
            $("#modifFixeCts").val($(this).parent().siblings(".fixe").html());
            $("#modifAdressCts").val($(this).parent().siblings(".adressPart").children(".NumRue").html());
            $("#modifZipCodeCts").val($(this).parent().siblings(".adressPart").children(".codePostal").html());
            $("#modifCityCts").val($(this).parent().siblings(".adressPart").children(".nomVille").html());
            roles=$(this).parent().siblings(".role").html();
            $("option:contains("+roles+")").prop('selected',true); 
            $('#modifAddOrgCts').hide();
            $('#modifOrgCts').css('width','45%');
            $('.Modif').slideFadeToggle();
        });
        
        
    }
    
    $.fn.slideFadeToggle = function(easing, callback) {
        return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
        };
    
        $('.resumeModif').click(function(e) {
            e.preventDefault;
            $('.Modif').slideFadeToggle();
            $('#bdd').removeClass("fond");
        });
        
        $('#modifOrgCts').change(function(){
            if($(this).val() == 'autres')
            {
                $("#modifAddOrgCts").show();
                $(this).css('width','15%');
            }else{
                $("#modifAddOrgCts").hide();
                $(this).css('width','45%');
            }
        });
        
        $(".saveModif").click(function(e){
            e.preventDefault;
            addOrModif = 'modif';
            gestionContacts(addOrModif);
        })
    
    
        
        $(".logoAdd").click(function(e){
            e.preventDefault();
            $('#bdd').addClass("fond");
            $('#addAddOrgCts').hide();
            $('#addOrgCts').css('width','45%');
            $('.addNew').slideFadeToggle();
        });
    
        $('#addOrgCts').change(function(){
            if($(this).val() == 'autres')
            {
                $("#addAddOrgCts").show();
                $(this).css('width','15%');
            }else{
                $("#addAddOrgCts").hide();
                $(this).css('width','45%');
            }
        });
    
        $('.resumeAdd').click(function(e) {
            e.preventDefault;
            $('.addNew').slideFadeToggle();
            $('#bdd').removeClass("fond");
        });
        
        $(".saveAdd").click(function(e){
            e.preventDefault;
            probleme = '0';
            addOrModif = 'add';
            $('.inputRequired').each(function(){
                if($(this).val() == "" || $(this).val() == " " )
                {
                    probleme = '1'
                    $(this).css('border', '2px solid red');
                    
                }
            })
            if(probleme == '1')
            {
                alert('champs obligatoires mal rempli');
                return;
            }
            gestionContacts(addOrModif);
        });
    
        
    function gestionContacts(selecteur){
        if(selecteur == "modif"){
            idCts = $('#numIdFocus').html();
        }else{
            idCts = "NULL";
        }
        
        nameCts = $('#'+selecteur+'NameCts').val();
        firstNameCts = $('#'+selecteur+'FirstNameCts').val();
        organisationCts = $('#'+selecteur+'OrgCts').val();
        addOrganisationCts = $('#'+selecteur+'AddOrgCts').val();
        mailCts = $('#'+selecteur+'MailCts').val();
        mobileCts = $('#'+selecteur+'MobileCts').val();
        fixeCts = $('#'+selecteur+'FixeCts').val();
        adressCts = $('#'+selecteur+'AdressCts').val();
        zipCodeCts = $('#'+selecteur+'ZipCodeCts').val();
        cityCts = $('#'+selecteur+'CityCts').val();
        roleCts = $('#'+selecteur+'RoleCts').val();
        
        /*alert('nom: ' + nameCts + "\nprenom: " + firstNameCts + "\norganisation1: " + organisationCts + "\norganisation2: " + addOrganisationCts + "\nmail: " + mailCts + "\nmobile: " +  mobileCts + "\nfixe: " + fixeCts + "\nadresse: " +  adressCts + "\n" + zipCodeCts + "; " + cityCts + "\nrole: " + roleCts);*/
        
        $.ajax({
            url: "public/ajax/gestionContacts.php",
            type: "POST",
            data: {
                'idCts' : idCts,
                'nameCts' : nameCts,
                'firstNameCts' : firstNameCts,
                'organisationCts' : organisationCts,
                'addOrganisationCts' : addOrganisationCts,
                'mailCts' : mailCts,
                'mobileCts' : mobileCts,
                'fixeCts' : fixeCts,
                'adressCts' : adressCts,
                'zipCodeCts' : zipCodeCts,
                'cityCts' : cityCts,
                'roleCts' : roleCts
            }            
        }).done(function(reponse){
            alert(reponse);
            setTimeout(function(){
            location.reload(true);
        }, 1000);
        });
    }
    
    
});
