jQuery(function() {
    
    $(".link").hide();			
    $('tr').hover(function(elt){
        if($(this).find('.link').is(':visible')){
            $(this).find(".link").hide();
            //console.log('je cache');
        }else{
            $(".link").hide();
            $(this).find(".link").show();
            //console.log('je montre');
        }
        //$(this).find('.link').show();
    });
    
    $('#addAnneeNeeAvant').keyup(function(){
        $('#addDesc1Categorie').prop("checked", true);
    })
    
    $('#addAnneeNeeEntre1').keyup(function(){
        $('#addDesc2Categorie').prop("checked", true);
    })
    
    $('#addAnneeNeeEntre2').keyup(function(){
        $('#addDesc2Categorie').prop("checked", true);
    })
    
    $('#addAnneeNeeApres').keyup(function(){
        $('#addDesc3Categorie').prop("checked", true);
    })
    
    $('#modifAnneeNeeAvant').keyup(function(){
        $('#modifDesc1Categorie').prop("checked", true);
    })
    
    $('#modifAnneeNeeEntre1').keyup(function(){
        $('#modifDesc2Categorie').prop("checked", true);
    })
    
    $('#modifAnneeNeeEntre2').keyup(function(){
        $('#modifDesc2Categorie').prop("checked", true);
    })
    
    $('#modifAnneeNeeApres').keyup(function(){
        $('#modifDesc3Categorie').prop("checked", true);
    })
    
    $(".deleteCategorie").click(function(e){
        e.preventDefault();
        if(confirm('êtes vous sur de vouloir supprimer les lignes séléctionnées')){
                ligne = $(this).parent().siblings('.idCategorie').html();
                console.log ('ok ' + ligne);
                action='delete';
                $.ajax({
                    url: "public/ajax/gestionCategory.php",
                    type: "POST",
                    data: {
                        'action' : action,
                        'idCategorie' : ligne,
                    }
                }).done(function(reponse){		
                    alert(reponse);
                    location.reload(true);
                });
        }
    });
    
    
    
    $(".logoAdd").click(function(e){
        e.preventDefault();        
        $('#bdd').addClass("fond");
        $('.addNew').slideFadeToggle();
               
    });
    
    $('.resumAdd').click(function(e) {
        e.preventDefault();
        $('.addNew').slideFadeToggle();
        $('#bdd').removeClass("fond");
    });
    
    $('.saveAdd').click(function(e) {
        e.preventDefault();
        action='create';
        comp=0
        nomCategorie = $('#addNomCategorie').val();
        libCategorie = $('#addLibCategorie').val();
        numCategorie = $('#addNumCategorie').val();
        if($('#addDesc1Categorie').is(':checked')){
            condCategorie = 'nés avant ' + $('#addAnneeNeeAvant').val();
        }
        if($('#addDesc2Categorie').is(':checked')){
            condCategorie = 'nés entre ' + $('#addAnneeNeeEntre1').val() + ' et ' + $('#addAnneeNeeEntre2').val();
        }
        if($('#addDesc3Categorie').is(':checked')){
            condCategorie = 'nés après ' + $('#addAnneeNeeApres').val();
        }
        
        $('.addCategorie').each(function(){
            if(!$(this).val()){
                comp=1
            }
        });
        //alert('condition ' + condCategorie);
        if(comp == '1'){
            alert('veuillez remplir les champs');
        }else{
            $.ajax({
                url: "public/ajax/gestionCategory.php",
                type: "POST",
                data: {
                    'action' : action,
                    'nomCategorie' : nomCategorie,
                    'libCategorie' : libCategorie,
                    'numCategorie' : numCategorie,
                    'condCategorie' : condCategorie,
                }
            }).done(function(reponse){		
                alert(reponse);
                location.reload(true);
            });
        }
        
    });
    
    $('.modifCategorie').click(function(e) {
        e.preventDefault();
        numCat = $(this).parent().siblings('.numCategorie').html();
        nomCat = $(this).parent().siblings('.nomCategorie').html();
        libCat = $(this).parent().siblings('.libCategorie').html();
        idCat = $(this).parent().siblings('.idCategorie').html();
        $('#modifNumCategorie').val(numCat);
        $('#modifNomCategorie').val(nomCat);
        $('#modifLibCategorie').val(libCat);
        $('#numIdFocus').html(idCat);
        $('.Modif').slideFadeToggle();
        $('#bdd').addClass("fond");
    })
    
    $('.resumModif').click(function(e) {
        e.preventDefault();
        $('.Modif').slideFadeToggle();
        $('#bdd').removeClass("fond");
    });
    
    $('.saveModif').click(function(e) {
        e.preventDefault();
        comp=0;
        action='modify';
        //alert("enregistrer");
        idCategorie = $('#numIdFocus').html()
        nomCategorie = $('#modifNomCategorie').val();
        libCategorie = $('#modifLibCategorie').val();
        numCategorie = $('#modifNumCategorie').val();
        if($('#modifDesc1Categorie').is(':checked')){
            condCategorie = 'nés avant ' + $('#modifAnneeNeeAvant').val();
        }
        if($('#modifDesc2Categorie').is(':checked')){
            condCategorie = 'nés entre ' + $('#modifAnneeNeeEntre1').val() + ' et ' + $('#modifAnneeNeeEntre2').val();
        }
        if($('#modifDesc3Categorie').is(':checked')){
            condCategorie = 'nés après ' + $('#modifAnneeNeeApres').val();
        }
        $('.modifyCategorie').each(function(){
            if(!$(this).val()){
                comp=1
            }
        });
        //alert('condition ' + condCategorie);
        if(comp == '1'){
            alert('veuillez remplir les champs');
        }else{
            $.ajax({
                url: "public/ajax/gestionCategory.php",
                type: "POST",
                data: {
                    'action' : action,
                    'idCategorie' : idCategorie,
                    'nomCategorie' : nomCategorie,
                    'libCategorie' : libCategorie,
                    'numCategorie' : numCategorie,
                    'condCategorie' : condCategorie,
                }
            }).done(function(reponse){		
                alert(reponse);
                location.reload(true);
            });
        }
    });
    
    
    $.fn.slideFadeToggle = function(easing, callback) {
        return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
    };
    
});
