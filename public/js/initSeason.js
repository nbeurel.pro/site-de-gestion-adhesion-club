jQuery(function() {
    
    $(".link").hide();			
    $('tr').hover(function(elt){
        if($(this).find('.link').is(':visible')){
            $(this).find(".link").hide();
            //console.log('je cache');
        }else{
            $(".link").hide();
            $(this).find(".link").show();
            //console.log('je montre');
        }
        //$(this).find('.link').show();
    });
    
    $(".deleteSaison").click(function(e){
        e.preventDefault();
        if(confirm('êtes vous sur de vouloir supprimer les lignes séléctionnées')){
                ligne = $(this).parent().siblings('.numSaison').html();
                console.log ('ok ' + ligne);
                action='delete';
                $.ajax({
                    url: "public/ajax/gestionSeason.php",
                    type: "POST",
                    data: {
                        'action' : action,
                        'idsaison' : ligne,
                    }
                }).done(function(reponse){		
                    alert(reponse);
                    location.reload(true);
                });
        }
    });
    
    $(".makeActive").click(function(e){
        e.preventDefault();
        nomSaison = $(this).parent().siblings('.nomSaison').html();
        if(confirm('êtes vous sur de vouloir activer la ' + nomSaison)){
                ligne = $(this).parent().siblings('.numSaison').html();
                console.log ('ok ' + ligne);
                action='activate';
                $.ajax({
                    url: "public/ajax/gestionSeason.php",
                    type: "POST",
                    data: {
                        'action' : action,
                        'idsaison' : ligne,
                    }
                }).done(function(reponse){		
                    alert(reponse);
                    location.reload(true);                    
                });
        }
    });
    
    $(".logoAdd").click(function(e){
        e.preventDefault();        
        $('#bdd').addClass("fond");
        $('.addNew').slideFadeToggle();
               
    });
    
    $('.returnSaison').click(function(e) {
        e.preventDefault();
        $('.addNew').slideFadeToggle();
        $('#bdd').removeClass("fond");
    });
    
    $('.enrSaison').click(function(e) {
        e.preventDefault();
        action='create';
        saison1 = $('#newSaison').val();
        saison2 = $('#newSaisonyear1').val();
        saison3 = $('#newSaisonyear2').val();
        newSaison = saison1 + ' ' + saison2 + '-' + saison3;
        if((saison2 === undefined) || (saison3 === undefined) || (saison2 === '') || (saison3 === '')){
            alert('veuillez remplir les champs date');
        }else{
            alert(newSaison);
            $.ajax({
                url: "public/ajax/gestionSeason.php",
                type: "POST",
                data: {
                    'action' : action,
                    'libSeason' : newSaison,
                }
            }).done(function(reponse){		
                alert(reponse);
                location.reload(true);
            });
        }
    });
    
    
    $.fn.slideFadeToggle = function(easing, callback) {
        return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
    };
    
});
