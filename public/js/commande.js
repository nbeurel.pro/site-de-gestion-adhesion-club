jQuery(function() {

	/**
	 * ajout d'un listener sur le bouton de class print
	 * verifie l'id du bouton "clické"
	 * imprime le tableau correspondant au bouton clické
	 */
	$(".print").click(function(event){
		event.preventDefault();
		if($(this).attr("id") == "printWaiting"){
			idTable = "bdd";
			titre = "nouveaux adhérents";
            printTable(idTable, titre);
		}else if($(this).attr("id") == "printReEnrollment"){
			idTable = "bdd2";
			titre = "reinscription";
            printTable(idTable, titre);
		}else if($(this).attr("id") == "printAdherents"){
		  window.print();
        }else if($(this).attr("id") == "printContacts"){
		  window.print();
        }
	});

	/**
	 * modifie temporairement la page html afin de n'imprimer qu'un des 2 tableaux de la page
	 * @param idTable String identifiant du tableau a imprimer
	 * @param titre String titre du tableau
	 */
	function printTable(idTable, titre){
		titreOrigine = $("h3:first").html();
		$("h3:first").html(titre);
		divOrigin = $("#" + idTable).html()
		$("#bdd").html(divOrigin + "<br/>" + divOrigin + "<br/>" + divOrigin).scrollLeft(0);
		window.print();
		$("#bdd").html(divOrigin);	
		$("h3:first").html(titreOrigine)	
	}

	/**
	 * ajout d'un listener sur le bouton de class excelize
	 * verifie l'id du bouton "clické"
	 * definit les variable table et titre
	 * appel la fonction phpToExcel en lui passant les variables
	 */
	$(".excelize").click(function(event){
		event.preventDefault();
		if($(this).attr("id") == "excelizeWaiting"){
			Table = $('#tableToPrint').html();
			Titre = "Nouveaux adherents";
		}else if($(this).attr("id") == "excelizeReEnrollment"){
			Table = $('#tableToPrint2').html();
			Titre = "reinscription";
		}else if($(this).attr("id") == "excelizeAdherents"){
            $(".filtres").hide();
			Table = $('#tableListAdherents').html();
			Titre = "adherents";
            $(".filtres").hide();
		}else if($(this).attr("id") == "excelizeContacts"){
			Table = $('#tableListContacts').html();
			Titre = "contacts";
		}
        
		phpToExcel(Table, Titre);
		
	});


	/**
	 * appel une requete ajax qui convertie et telecharge le tableau en format excel
	 * @param Table String code html du tableau
	 * @param Titre String titre pour le tableu
	 */
	function phpToExcel(Table, Titre){
		$.ajax({
			url: "public/ajax/initExcel.php",
			type: "POST",
			data: {
				'table' : Table,
				'titre' : Titre
			}
		}).done(function(reponse){	
			window.location = "public/ajax/downloadExcel.php";
		});
	}


	/**
	 * ajout d'un listener sur le bouton de class principal
	 * permet d'afficher ou de masque le menu lié au bouton
	 */
	$('.principal').click(function(){
        $(this).children('.child').toggle();
    });


	/**
	 * ajout d'un listener sur le bouton de class principal-nav
	 * permet d'afficher ou de masque le menu lié au bouton
	 */
	$('.principal-nav').hover(function(){
        $(this).children('.child-nav').toggle();
    });


	/**
	 * recupere l'identifiant d'une personne pour afficher sont login
	 */
	$('#idPersonneTool').on("change keyup",function(){
        idPersonne = $(this).val();
        action = 'seek';
        console.log(idPersonne + '' + action);
        $.ajax({
			url: "public/ajax/modifMpd.php",
			type: "POST",
			data: {
                'action' : action,
				'idpersonne' : idPersonne
			}
		}).done(function(reponse){	
			$('#result').html(reponse);
            
		});
    
    });

	/**
	 * ajout un listener sur le bouton de class enregistrement
	 * recupere les identifiant d'un personnes afin de les modifier en bdd
	 */
	$('#enregistrement').click(function(event){
        event.preventDefault();
        action = 'save';
        idPersonne = $('#idPersonneTool').val();
        loginPersonne = $('#idConnexion').val();
        passwordPersonne = $('#pass').val();
        console.log(idPersonne + ' ' + loginPersonne +' '+passwordPersonne);
        $.ajax({
			url: "public/ajax/modifMpd.php",
			type: "POST",
			data: {
                'action' : action,
				'idpersonne' : idPersonne,
                'loginPersonne' : loginPersonne,
                'passwordPersonne' : passwordPersonne             
			}
        }).done(function(reponse){	
			alert(reponse);
        });
                
    });

	/**
	 * Ajoute un listener sur le bouton de class exposant
	 * permet d'afficher et de masquer une infobulle
	 */
	$('.exposant').click(function(){
        $('.infobulle').toggle();
    });
    
});


