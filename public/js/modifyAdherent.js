jQuery(function() {
    
    $('input[type="date"]').change(function(){
        console.log($(this).val());
        regexDate= /^\d{4}(-)(((0)[0-9])|((1)[0-2]))(-)([0-2][0-9]|(3)[0-1])$/;
        if(!$(this).val().match(regexDate)){
            alert('format date invalide');
            $(this).val('');
        }
    });
    
    
    $(".modify").click(function(e){
        e.preventDefault;
        idAdhesion = $("#numAdhesion").html();
        idPersonne = $('#identifiant').val();
        numSIFFA = $('#siffa').val();
        dteLicence = $('#licence_date').val();
        nameAdherent = $('#name').val();
        firstNameAdherent = $('#first_name').val();
        sexeAdherent = $('input[type=radio][name=sexe]:checked').val();
        birthdayAdherent = $('#birthdate').val();
        country = $('#country').val();
        adress = $('#adress').val();
        zipCode = $('#postal').val();
        city = $('#city').val();
        mail = $('#mail').val();
        mobilePhone = $('#mobile').val();
        fixePhone = $('#phone').val();
        dteLastConnect = $('#last_date').val();
        idSaison = $('#idseason').val();
        grpAdherent = $('#id_group_adh').val();
        mntAdhesion  = $('#tarifLicence').val();
        idParent = $('#idparent').val();
        idConnexion = $('#loginAdherent').val(); 
        grpAdhesion = $('#id_group_adh').val();
        respLegal = $('#legal').val();
        typeRespLegal = $('#legal').val();
        identityYoungAdherent = nameAdherent + " " + firstNameAdherent;
        catAge = $('#cat_age').val();
        typeLic = $('#licence').val();
        dateAdhesion = $('#date_season').val();
        statusAdhesion = $('#statut').val();
        datePaiement = $('#date_paiement').val();
        
        console.log(datePaiement);
       
        
        if($(':checkbox[id="prelevementsanguin"]:checked').val()){
            authSample = 1;
        } else {
            authSample = 0;
        } 
        if($(':checkbox[id="hospitalisation"]:checked').val()){
            authHospital = 1;
        } else {
            authHospital = 0;
        }
        if($(':checkbox[id="Ri"]:checked').val()){
            interiorRule = 1;
        } else {
            interiorRule = 0;
        }
        if($(':checkbox[id="insurance"]:checked').val()){
            assurance = 1;
        } else {
            assurance = 0;
        }
        if($(':checkbox[id="droitImage"]:checked').val()){
            authProfil = 1;
        } else {
            authProfil = 0;
        }        
        if($(':checkbox[id="law"]:checked').val()){
            authRGPD = 1;
        } else {
            authRGPD = 0;
        }
              
        
        $.ajax({
            url: "public/ajax/injectionModifAdherent.php",
            type: "POST",
            data: {
                'idPersonne' : idPersonne,
                'numSIFFA' : numSIFFA,
                'dteLicence' : dteLicence,
                'nameAdherent' : nameAdherent,
                'firstNameAdherent' : firstNameAdherent,
                'sexeAdherent' : sexeAdherent,
                'birthdayAdherent' : birthdayAdherent,
                'country' : country,
                'adress' : adress,
                'zipCode' : zipCode,
                'city' : city,
                'mail' : mail,
                'mobilePhone' : mobilePhone,
                'fixePhone' : fixePhone,
                'idConnexion' : idConnexion,
                'dteLastConnect' : dteLastConnect,
                'idParent' : idParent,
                'idAdhesion' : idAdhesion,
                'respLegal' : respLegal,
                'typeRespLegal' : typeRespLegal,
                'identityYoungAdherent' : identityYoungAdherent,
                'authSample' : authSample,
                'authHospital' : authHospital,
                'interiorRule' : interiorRule,
                'assurance' : assurance,
                'authProfil' : authProfil,
                'authRGPD' : authRGPD,
                'datePaiement' : datePaiement,
                'dateAdhesion' : dateAdhesion,
                'grpAdherent' : grpAdherent,
                'mntAdhesion' : mntAdhesion,
                'idSaison' : idSaison,
                'statusAdhesion' : statusAdhesion,
                'catAge' : catAge,
                'typeLic' : typeLic
                
            },
            success : function(code_html, statut){		
                console.log('la requete est bien envoyé');
                //console.log('code html= ' + code_html);                                
                console.log('statut= ' + statut); 
            },
            error: function(resultat, statut, erreur){
                console.log('problème');
                console.log('resultat= ' + resultat);   
                console.log('statut= ' + statut); 
                console.log('erreur= ' + erreur); 
                return false;
            },
            complete : function(resultat, statut){
                console.log('la requete est complete');
                console.log('resultat=' + resultat);                                
                console.log('statut=' + statut);
            }
        }).done(function(reponse){		
            msg=reponse;
            //alert("retour" + reponse);
        });
        
        alert('les informations ont été mises à jours');
        
        $('html,body').animate({scrollTop: 0}, 'fast');
    })
    
    
    $("#modifActivityEnr").click(function(){
        idAdhesion = $("#numAdhesion").html(); 
        //alert (idAdhesion);
        $.ajax({
            url: "public/ajax/supprAllActivity.php",
            type: "POST",
            data: {
                'idAdhesion' : idAdhesion
            }
        }).done(function(reponse){		
            msg=reponse;
            console.log(reponse);
        });
        setTimeout(function(){
            $('.activityGestion').each(function(){
                //alert($(this).val());
                if($(this).prop('checked') ==true){
                    idActivity = $(this).val();
                    //alert('idadhesion: ' + idAdhesion + "\n" + 'idactivity: ' + idActivity);
                    $.ajax({
                        url: "public/ajax/addActivity.php",
                        type: "POST",
                        data: {
                            'idAdhesion' : idAdhesion,
                            'idActivity' : idActivity
                        }
                    });
                }
            });
        }, 5000);
         alert("les informations ont étés mises à jours \nmerci de patienter");
        $('.gestionActivity').slideFadeToggle();
        $('.gestionWaiting').slideFadeToggle();
        setTimeout(function(){
            location.reload(true);
        }, 10000);
        
    });
    
    $("#modifRoleEnr").click(function(){
        idAdhesion = $("#numAdhesion").html(); 
        //alert (idAdhesion);
        $.ajax({
            url: "public/ajax/supprAllRolesAdhesion.php",
            type: "POST",
            data: {
                'idAdhesion' : idAdhesion
            }
        }).done(function(reponse){		
            msg=reponse;
            console.log(reponse);
        });
        setTimeout(function(){
            $('.roleGestion').each(function(){
                //alert($(this).val());
                if($(this).prop('checked') ==true){
                    idRole = $(this).val();
                    //alert('idadhesion: ' + idAdhesion + "\n" + 'idactivity: ' + idActivity);
                    $.ajax({
                        url: "public/ajax/addRoleAdhesion.php",
                        type: "POST",
                        data: {
                            'idAdhesion' : idAdhesion,
                            'idRole' : idRole
                        }
                    });
                }
            });
        }, 5000);
        alert("les informations ont étés mises à jours \nmerci de patienter");
        $('.gestionRoles').slideFadeToggle();
        $('.gestionWaiting').slideFadeToggle();
        setTimeout(function(){
            location.reload(true);
        }, 10000);
        
    });
    
       
    $('.gestion-activity').click(function(e){
        e.preventDefault();
        console.log('click');
        $('.gestionActivity').slideFadeToggle();
        $('#infoAdherent').addClass("fond");
    });
     
    $('.gestion-role').click(function(e){
        e.preventDefault();
        console.log('click');
        $('.gestionRoles').slideFadeToggle();
        $('#infoAdherent').addClass("fond");
    });
     
    $('.gestion-certif').click(function(e){
        e.preventDefault();
        console.log('click');
        $('.gestionCertifMedical').slideFadeToggle();
        $('#infoAdherent').addClass("fond");
    });
    
    $('#returnActivity').click(function(){
        $('.gestionActivity').slideFadeToggle();
        $('#infoAdherent').removeClass("fond");
        location.reload(true);
    });
    
    $('#returnRole').click(function(){
        $('.gestionRoles').slideFadeToggle();
        $('#infoAdherent').removeClass("fond");
        location.reload(true);
    });
    
     $('#returnCertificat').click(function(){
        $('.gestionCertifMedical').slideFadeToggle();
        $('#infoAdherent').removeClass("fond");
        location.reload(true);
    });
    
    $.fn.slideFadeToggle = function(easing, callback) {
        return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
    };
   
});