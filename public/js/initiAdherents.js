jQuery(function() {
    
    sorting = 'orderNameAsc';
	
	$(document).ready(function(){
		majBDD(sorting);
		if($(".notification").html() == 0){
			//console.log('nada');
			$(".notification").hide();
		}else{
			//console.log("il y a " + $(".notification").html() + " inscription en attente");
			$(".notification").show();
		}
	});
	
	$("input").keyup(function(){
		majBDD(sorting);
        console.log(sorting);
	});
	
	$('select').change(function(){
		majBDD(sorting);
        console.log(sorting);
	});
	
	$('#ouiSIFFA').click(function(){
		majBDD(sorting);
        console.log(sorting);
	});
    
    $('.order').click(function(){
        
        $('.asc').html('<svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M3.204 11L8 5.519 12.796 11H3.204zm-.753-.659l4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z"/></svg>');
        $('.desc').html('<svg width="12px" height="12px" viewBox="0 0 16 16" class="bi bi-caret-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"/></svg>');            
        if($(this).hasClass('active')){
            sorting = 'orderNameAsc';
            $('.order').removeClass('active');
        }else{
            sorting = $(this).attr('id');
            $('.order').removeClass('active');
            $(this).addClass('active');
            if($(this).hasClass('asc')){
               $(this).html('<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-up-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M7.247 4.86l-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z"/></svg>');
            }else{
                $(this).html('<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-down-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M7.247 11.14L2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/></svg>');
            }
        }
        console.log(sorting);
        majBDD(sorting);
    });
	                 	
	
	function majBDD(sorting){
        
		console.log('majBDD');
		saison = $("#selectSaison").val();
		categorieAge = $("#selectCategorieAge").val();
		Nom = $("#input_nom").val();
		Prenom = $("#input_prenom").val();	
		NumLic = $("#input_numlice").val();
		Activite = $("#select_activity").val();
		Mail = $("#input_mel").val();
		if ($("#ouiSIFFA").is(":checked")){
			console.log('siffa');
			Siffa = 0;
		}else{
			console.log('nonsiffa');
			Siffa = 666;
		}
		
    
		variables = "saison: " + saison + "\ncatégorie d'age: " + categorieAge + "\nNom:" + 
					Nom + "\nPrenom:" + Prenom + "\nNumero de licence:" + NumLic + "\nactivité:" + Activite +
					"\nMail" + Mail;
		
		console.log(variables);
        
		$.ajax({
			url: "public/ajax/createList.php",
			type: "POST",
			data: {
				'saison' : saison,
				'catage' : categorieAge,
				'nom' : Nom,
				'prenom' : Prenom,
				'numeroLicence' : NumLic,
				'activite' : Activite,
				'mel' : Mail,
				'siffa' : Siffa,
                'sorting' : sorting
			}
		}).done(function(reponse){		
			$("#table").html(reponse);	
			page()
		});
	}
	
		
	function page(){
		$(".link").hide();			
		$('tr').hover(function(elt){
			if($(this).find('.link').is(':visible')){
				$(this).find(".link").hide();
				//console.log('je cache');
			}else{
				$(".link").hide();
				$(this).find(".link").show();
				//console.log('je montre');
			}
			//$(this).find('.link').show();
		});
	
	
		//function deselect(e) {
			//$('.pop').slideFadeToggle(function() {
				//e.removeClass('selected');
			//});    
		//}

		$(function() {
			$('.link').click(function(event) {
				event.preventDefault;
				console.log('coucou');
				if($(this).hasClass('selected')) {
					$(this).removeClass('selected');
					$('.pop').slideFadeToggle();
					$('#bdd').removeClass("fond");               
				} else {
					$(this).addClass('selected');
					nom = $(this).parent().siblings(".nom").html();
					prenom = $(this).parent().siblings(".prenom").html();
					mel = $(this).parent().siblings(".mel").html();
                    NumSeason = $("#selectSaison").val();
					$('#bdd').addClass("fond");
					console.log(nom + ' ' + prenom);
					$.ajax({
						url: "public/ajax/focusAdherent.php",
						type: "POST",
						data: {
							'nAt' : nom,
							'pAt' : prenom,
							'mAt' : mel,
                            'sAt' : NumSeason,
						}
					}).done(function(reponse){	
						$("#fichAdherent").html(reponse);	
						$('.pop').slideFadeToggle();
						$('.close').click(function(e) {
							//deselect($('.link'));
                            e.preventDefault;
							retourNormal();
                        });
                        $('.delete').click(function(e){
                            idAdherent = $('.idAdherent').html();
                            //alert(idAdherent);
                            $.ajax({
                                url: "public/ajax/deleteAdherentAcpa.php",
                                type: "POST",
                                data: {
                                    'idadherent' : idAdherent
                                }
                            }).done(function(reponse){
                                alert(reponse);
                                location.reload(true);
                            });
						});
					});
					
				}
				return false;
			});

			
		});
		
		function retourNormal(){
			$('.pop').slideFadeToggle();
			$('.selected').removeClass('selected');
			$('#bdd').removeClass("fond");
			return false;
		}

		$.fn.slideFadeToggle = function(easing, callback) {
			return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
		};
	}
    
});
