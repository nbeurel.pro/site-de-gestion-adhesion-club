jQuery(function() {
    
    $(".link").hide();			
    $('tr').hover(function(elt){
        if($(this).find('.link').is(':visible')){
            $(this).find(".link").hide();
            //console.log('je cache');
        }else{
            $(".link").hide();
            $(this).find(".link").show();
            //console.log('je montre');
        }
        //$(this).find('.link').show();
    });
    
    $(".deleteMoyenPaiement").click(function(e){
        e.preventDefault();
        if(confirm('êtes vous sur de vouloir supprimer la ligne séléctionnées')){
                ligne = $(this).parent().siblings('.idMoyenPaiement').html();
                action='delete';
                console.log (action + ': ' + ligne);
                $.ajax({
                    url: "public/ajax/gestionPayMethod.php",
                    type: "POST",
                    data: {
                        'action' : action,
                        'idMoyenPaiement' : ligne,
                    }
                }).done(function(reponse){		
                    alert(reponse);
                    location.reload(true);
                });
        }
    });
    
    
    
    $(".logoAdd").click(function(e){
        e.preventDefault();        
        $('#bdd').addClass("fond");
        $('.addNew').slideFadeToggle();
               
    });
    
    $('.resumAdd').click(function(e) {
        e.preventDefault();
        $('.addNew').slideFadeToggle();
        $('#bdd').removeClass("fond");
    });
    
    $('.saveAdd').click(function(e) {
        e.preventDefault();
        action='create';
        nomMoyenPaiement = $('#addNomMoyenPaiement').val();
        if ($('#addAcceptMoyenPaiement').is(':checked')){
            moyenAccepte = '1';
        }else{            
            moyenAccepte = '0';
        }        
        console.log(action);
        $.ajax({
            url: "public/ajax/gestionPayMethod.php",
            type: "POST",
            data: {
                'action' : action,
                'nomMoyenPaiement' : nomMoyenPaiement,
                'moyenAccepte' : moyenAccepte
            }
        }).done(function(reponse){		
            alert(reponse);
            location.reload(true);
        });
        
    });
    
    $('.switchMoyenPaiement').click(function(e) {
        e.preventDefault();
        ligne = $(this).parent().siblings('.idMoyenPaiement').html();
        action = 'switchMoyen';
        if($(this).hasClass('accept')){
            moyenAccepte = '1';
        }else{
            moyenAccepte = '0';
        }
        console.log(action);
        console.log(moyenAccepte);
        $.ajax({
                    url: "public/ajax/gestionPayMethod.php",
                    type: "POST",
                    data: {
                        'action' : action,
                        'idMoyenPaiement' : ligne,
                        'moyenAccepte' : moyenAccepte
                    }
                }).done(function(reponse){		
                    alert(reponse);
                    location.reload(true);
                });
    });
    
        
    $.fn.slideFadeToggle = function(easing, callback) {
        return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
    };
    
});
