<?php

$annee = date("Y");
$moisJour = date("m-d");

$annee = $annee -3;

$test = $annee."-".$moisJour;

echo "date: ". $test ."\n";


$host = "acpaysangestlice.mysql.db";
$dataBaseName = "acpaysangestlice"; 
$user = "acpaysangestlice";
$pass = "ccH2hP84KXh2";

echo "tentative de connexion DB \n";
try
{

    $db = new PDO('mysql:host=' . $host . ';dbname=' . $dataBaseName , $user, $pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

}
catch (Exception $e)
{
    die('erreur : ' . $e->getmessage());
}

echo "connexion DB ok \n";



try{
    $listAdhesions = $db->prepare(
                            'SELECT * FROM `acpa_adhesion` 
                            WHERE acpa_adhesion.dtm_adhesion = :date');

    $listAdhesions->execute(array('date' => $test));
}
catch (Exception $e)
{
    die('erreur : ' . $e->getmessage());
}

echo "liste des adhesions obsoletes \n";

while ($adhesion = $listAdhesions->fetch())
{
    echo "-  - Adhesion:" . $adhesionPersonne['id_adhesion'] . "\n";

    $idAdhesion = $adhesion['id_adhesion'];

    try{
        $listActiviteAdhesion = $db->prepare(
						'DELETE FROM `acpa_activite_adh` 
                        WHERE acpa_activite_adh.id_adhesion = :idAdhesion');
        
        $listActiviteAdhesion->execute(array('idAdhesion' => $idAdhesion));
    }
    catch (Exception $e)
    {
        die('erreur : ' . $e->getmessage());
    }

    echo "activites supprimees \n";

    try{
        $listDocumentAdhesion = $db->prepare(
						'DELETE FROM `acpa_document` 
                        WHERE acpa_document.id_adhesion = :idAdhesion');
        
        $listDocumentAdhesion->execute(array('idAdhesion' => $idAdhesion));
    }
    catch (Exception $e)
    {
        die('erreur : ' . $e->getmessage());
    }

    echo "documents supprimes \n";


    try{
        $listRoleAdhesion = $db->prepare(
						'DELETE FROM `acpa_role_adhesion` 
                        WHERE acpa_role_adhesion.id_adhesion = :idAdhesion');
        
        $listRoleAdhesion->execute(array('idAdhesion' => $idAdhesion));
    }
    catch (Exception $e)
    {
        die('erreur : ' . $e->getmessage());
    }

    echo "roles supprimes \n";
    
    try{
        $Adhesion = $db->prepare(
						'DELETE FROM `acpa_adhesion` 
                        WHERE acpa_adhesion.id_adhesion = :idAdhesion');
        
        $Adhesion->execute(array('idAdhesion' => $idAdhesion));
    }
    catch (Exception $e)
    {
        die('erreur : ' . $e->getmessage());
    }   

    echo "Adhesions supprimees \n";        
}

echo "fin des suppressions d'adhesion \n";

try{
   $listPersonnes = $db->query(
						'SELECT acpa_personne.id_personne
                        AS id_personne_delete
                        FROM acpa_personne 
                        LEFT JOIN acpa_adhesion ON acpa_personne.id_personne = acpa_adhesion.id_personne 
                        WHERE acpa_adhesion.id_personne IS NULL
                        AND acpa_personne.id_organisation IS NULL');
}
catch (Exception $e)
{
    die('erreur : ' . $e->getmessage());
}   

echo "liste des adherents sans adhesion \n";

while ($personne = $listPersonnes->fetch())
{
    echo "-  - Adherent:" .$personne['id_personne_delete'] . "\n";
    
    $idPersonne = $personne['id_personne_delete'];
    
    
    try{
        $listDocumentPersonne = $db->prepare(
						'DELETE FROM `acpa_document` 
                        WHERE acpa_document.id_personne_doc = :idPersonne');
        
        $listDocumentPersonne->execute(array('idPersonne' => $idPersonne));
    }
    catch (Exception $e)
    {
        die('erreur : ' . $e->getmessage());
    }
    
    echo "doc supprime \n";
    
    try{
        $Personne = $db->prepare(
						'DELETE FROM `acpa_personne` 
                        WHERE acpa_personne.id_personne = :idPersonne');
        
        $Personne->execute(array('idPersonne' => $idPersonne));
        }
    catch (Exception $e)
    {
        die('erreur : ' . $e->getmessage());
    } 
    
    echo "adherent supprime \n";
}


echo "DB nettoye \n";
