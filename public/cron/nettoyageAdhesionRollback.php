<?php
/*
Fichier PHP comprenant le script de nettoyage de la base de données

se nettoyage se fait en plusieurs étapes découpé en 2 parties:
|
|-> suppression des adhesion de plus de trois ans
|----->récuperation des identifiants des adhesions qui ont étés crée il y a trois ans ou plus (a date du jour)
|----->Suppression des liens entre ces adhesions et des activités (table acpa_activite_adh)
|----->Suppression des documents liés à ces adhesions(table acpa_document)
|----->Suppression des liens entre ces adhesions et des role (table acpa_role_adhesion)
|----->Supression des adhesions
|
|-> suppression des adherents sans adhesion*
|----->récuperation des identifiants des adherents
|----->Suppression des documents liés à ces adhesions(table acpa_document)
|----->Supression des adherents
*/


//création de la variable de date du jours il y a 3 ans
$annee = date("Y");
$moisJour = date("m-d");
$annee = $annee -3;
$date = $annee."-".$moisJour;

// création des variable de connexion à la base de données
$host = "acpaysangestlice.mysql.db";
$dataBaseName = "acpaysangestlice"; 
$user = "acpaysangestlice";
$pass = "ccH2hP84KXh2";

//2058 968
//affichage des messages dans le fichier logs d'ovh
echo "nettoyage de la base de donnees a la date du : ". $date ."\n";
echo "Tentative de connexion DB \n";


try
{
    //tentative de connexion a la BDD & activation des mode d'erreur
    $db = new PDO('mysql:host=' . $host . ';dbname=' . $dataBaseName , $user, $pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

}
catch (Exception $e)
{
    // si erreur affichage d'un message d'erreur dans fichier logs d'ovh et sortie du programme
    die('erreur : ' . $e->getmessage());
}

//affichage des messages dans le fichier logs d'ovh
echo "Connexion DB ok \n";
echo "\n----------------------------------------------------------------------------\n";
echo "|-Debut de la suppression des adhesions de plus de 3 Ans\n";

try{
    // tentative de réalisation de la 1er partie
    
    
    // lancement de la transaction (debut de "l'enregistrement" pour un potentiel retour en arrière)
    $db->beginTransaction();

    //preparation & execution de la requete de recuperation des ID des adhesions de plus de 3 ans
    $listAdhesions = $db->prepare(
                            'SELECT * FROM `acpa_adhesion` 
                            WHERE acpa_adhesion.dtm_adhesion < :date');
    
    //execution de la requete
    $listAdhesions->execute(array('date' => $date));
    
    // affichage d'un message dans le fichier logs d'OVH
    echo "|---liste des adhesions obsoletes \n";

    // debut de la boucle a executer à chaque adhesion
    while ($adhesion = $listAdhesions->fetch())
    {
        
        // récuperationde l'ID de l'adhesion
        $idAdhesion = $adhesion['id_adhesion'];
        
        // affichage d'un message dans le fichier logs d'OVH
        echo "|-------Adhesion:" . $idAdhesion . "\n";
        
        //preparation & execution de la requete de suppression des liens entre l'adhesion et les activités
        $deleteActiviteAdhesion = $db->prepare(
                        'DELETE FROM `acpa_activite_adh` 
                        WHERE acpa_activite_adh.id_adhesion = :idAdhesion');
        $deleteActiviteAdhesion->execute(array('idAdhesion' => $idAdhesion));
        
        
        //preparation & execution de la requete de suppression des documents de l'adhesion
        $deleteDocumentAdhesion = $db->prepare(
						'DELETE FROM `acpa_document` 
                        WHERE acpa_document.id_adhesion = :idAdhesion');
        
        $deleteDocumentAdhesion->execute(array('idAdhesion' => $idAdhesion));
    

        //preparation & execution de la requete de suppression des liens entre l'adhesion et les roles
        $deleteRoleAdhesion = $db->prepare(
						'DELETE FROM `acpa_role_adhesion` 
                        WHERE acpa_role_adhesion.id_adhesion = :idAdhesion');        
        $deleteRoleAdhesion->execute(array('idAdhesion' => $idAdhesion));
    
        //preparation & execution de la requete de suppression des adhesions
        $Adhesion = $db->prepare(
						'DELETE FROM `acpa_adhesion` 
                        WHERE acpa_adhesion.id_adhesion = :idAdhesion');        
        $Adhesion->execute(array('idAdhesion' => $idAdhesion));
        
               
    }

    // si tous se passe bien, on valide les requetes
    $db->commit();
    
    // affichage des messages dans le fichier logs d'OVH
    echo "|---Activites supprimees \n";
    echo "|---Documents supprimes \n";
    echo "|---Roles supprimes \n";
    echo "|---Adhesions supprimees \n"; 
    echo "|-fin de la suppression des adhesions de plus de 3 Ans\n";
}
catch (Exception $e)
{
    // si une erreur on annule les requetes
    $db->rollback();
    
    //affichage des messages dans le fichier logs d'OVH
    echo "|---il y a eut des erreures \n";
    echo "|---erreur: " . $e->getMessage() ."\n";
    echo "|---N°" . $e->getCode();
    echo "|-pas de suppression des adhesions de plus de 3 Ans\n";
}  

//affichage des messages dans le fichier logs d'OVH
echo "|\n|----------------------------------------------------------------------------\n";
echo "|-Debut de la suppression des adherents sans adhesion\n";


try{
    // tentative de réalisation de la 2eme partie
    
    
    // lancement de la transaction (debut de "l'enregistrement" pour un potentiel retour en arrière)
    $db->beginTransaction();
    
    // execution de la requete de recuperation des ID des adherents sans adhesion
    $listPersonnes = $db->query(
						'SELECT acpa_personne.id_personne
                        AS id_personne_delete
                        FROM acpa_personne 
                        LEFT JOIN acpa_adhesion ON acpa_personne.id_personne = acpa_adhesion.id_personne 
                        WHERE acpa_adhesion.id_personne IS NULL
                        AND acpa_personne.id_organisation IS NULL');

    // affichage d'un message dans le fichier logs d'OVH
    echo "|---liste des adherents sans adhesion \n";

    // debut de la boucle a executer pour chaque adherent
    while ($personne = $listPersonnes->fetch())
    {
        // récuperationde l'ID de l'adhesion
        $idPersonne = $personne['id_personne_delete'];
        
        // affichage d'un message dans le fichier logs d'OVH
        echo "|-------Adherent:" .$personne['id_personne_delete'] . "\n";

        
        // preparation & execution de la requete de suppression des documents liés a l'adherents
        $deleteDocumentPersonne = $db->prepare(
                        'DELETE FROM `acpa_document` 
                        WHERE acpa_document.id_personne_doc = :idPersonne');
        $deleteDocumentPersonne->execute(array('idPersonne' => $idPersonne));
        
        
        // preparation & execution de la requete de suppression des liens entre l'adherent et les groupe ged
        $deleteGroupPersonne = $db->prepare(
                        'DELETE FROM `acpa_appartient_gr` 
                        WHERE acpa_appartient_gr.id_personne = :idPersonne');
        $deleteGroupPersonne->execute(array('idPersonne' => $idPersonne));
        
        
        $deleteLinkChildPersonne = $db->prepare(
            'UPDATE acpa_personne
            SET id_parent = NULL
            WHERE acpa_personne.id_parent = :idPersonne');
        $deleteLinkChildPersonne->execute(array('idPersonne' => $idPersonne));
        
        
        
        // preparation & execution de la requete de suppression des adherents
        $Personne = $db->prepare(
                        'DELETE FROM `acpa_personne` 
                        WHERE acpa_personne.id_personne = :idPersonne');
        $Personne->execute(array('idPersonne' => $idPersonne));
                
    }
    
    // si tous se passe bien, on valide les requetes
    $db->commit();
    
    //affichage des messages dans le fichier logs d'OVH
    echo "|---doc supprimes \n";
    echo "|---adherents supprimes \n";
    echo "|-fin de la suppression des adherents sans adhesion\n";
    
}
catch (Exception $e)
{
    // si une erreur on annule les requetes
    $db->rollback();
    
    //affichage des messages dans le fichier logs d'OVH
    echo "|---il y a eut des erreures \n";
    echo "|---erreur: " . $e->getMessage() ."\n";
    echo "|---N°" . $e->getCode();
    echo "|-pas de suppression des adherent\n";
}  

//affichage des messages dans le fichier logs d'OVH
echo "|\n|----------------------------------------------------------------------------\n";
echo "DB nettoye \n";
