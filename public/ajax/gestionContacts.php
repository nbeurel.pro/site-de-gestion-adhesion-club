<?
/* document ajax permettant l'ajout, ou la modification des contacts et l'ajout d'u dans la bdd depuis un script jquery */


// appel du fichier qui contient les requettes MySQL
require_once('../../model/Acpa.php');

// recuperation des variables
$idAdhesion = $_POST['idAdhesion'];
$idActivity = $_POST['idActivity'];
$idContacts = $_POST['idCts'];
$nameContacts = $_POST['nameCts'];
$firstNameContacts = $_POST['firstNameCts'];
$organisationContacts = $_POST['organisationCts'];
$addOrganisationContacts = $_POST['addOrganisationCts'];
$mailContacts = $_POST['mailCts'];
$mobileContacts = $_POST['mobileCts'];
$fixeContacts = $_POST['fixeCts'];
$adressContacts = $_POST['adressCts'];
$zipCodeContacts = $_POST['zipCodeCts'];
$cityContacts = $_POST['cityCts'];
$roleContacts = $_POST['roleCts'];

// création des instances de class
$acpa = new Acpa();
$setterAcpa = new SetterAcpa();
$getterAcpa = new GetterAcpa();

try
{
    // tentative de réalisation des requettes
    
    // connection a la BDD
    $db = $acpa->dbconnect();
    
    // demarrage de la transaction
    $acpa->startTransaction($db);
// si le contact n'a pas d'ID, il n'existe pas et est crée
    if($idContacts == "NULL"){
        // création d'un message
        $message = "Le nouveau contact a été créé";

        // si l'utilisateur a choisit autre dans organisation, appel de la fonction de création d'une organisation et recuperation de son ID. modification du message
        if($organisationContacts == 'autres'){
    
            // Test des requetes            
            $setterAcpa->addOneOrganisation($db, $addOrganisationContacts);
            $lastOrga = $getterAcpa->getLastOrganisation($db);
            $lastOrganism = $lastOrga->fetch();
            
            // recuperation de la variable
            $organisationContacts = $lastOrganism['max_idorg'];
            
            //modification de la variable
            $message .=  "\nL'organisme a également été créé";
        }

        // création des variables manquantes
        $numLicSiffa = NULL;
        $dteLicence = NULL;
        $sexe = NULL;
        $dateNaissance = NULL;
        $nationalite = NULL;
        $login = NULL;
        $id_parent = NULL;

        //création d'un tableau de variables
        $dataPersonne = array('numLicenceSIFFA' => $numLicSiffa,
                                'dtLicence' => $dteLicence,
                                'libNom' => $nameContacts,
                                'libPrenom' => $firstNameContacts,
                                'libSexe' => $sexe,
                                'dtNaissance' => $dateNaissance,
                                'libNationalite' => $nationalite,
                                'libAdresse' => $adressContacts,
                                'numCodePostal' => $zipCodeContacts,
                                'libVille' => $cityContacts,
                                'libMailPersonne' => $mailContacts,
                                'telMobile' => $mobileContacts,
                                'telFixe' => $fixeContacts,
                                'login' => $login,
                                'idParent' => $id_parent,
                                'idOrganisation' => $organisationContacts);

        // test de la fonction de création d'une personne et recuperation de son ID
        $setterAcpa->addNewPersonne($db, $dataPersonne);
        $lastContact = $getterAcpa->getLastPerson($db);
        $contact = $lastContact->fetch();
        $idContacts = $contact['max_id'];

        // test de la fonction de création d'un role pour la personne dans la table acpa_role_personne
        $setterAcpa->addOneRolePersonne($db, $idContacts, $roleContacts);


    // si le contact a un id, il existe alors on le modifie
    }else{
        // création d'un message
        $message = "le contact a été modifié";

        // si l'utilisateur a choisit autre dans organisation, appel de la fonction de création d'une organisation et recuperation de son ID. modification du message
        if($organisationContacts == 'autres'){
            $setterAcpa->addOneOrganisation($db, $addOrganisationContacts);
            $lastOrga = $getterAcpa->getLastOrganisation($db);
            $lastOrganism = $lastOrga->fetch();
            $organisationContacts = $lastOrganism['max_idorg'];
            $message .=  "\nL'organisme a également été créé";
        }

        //création d'un tableau de variables
        $dataPersonne = array('nameContact' => $nameContacts,
                              'firstNameContact' => $firstNameContacts,
                              'adress' => $adressContacts,
                              'zipCode' => $zipCodeContacts,
                              'city' => $cityContacts,
                              'mail' => $mailContacts,
                              'mobilePhone' => $mobileContacts,
                              'fixePhone' => $fixeContacts,
                              'idOrganisation' => $organisationContacts,
                              'idPersonne' => $idContacts);


            // appel de la fonction de modification d'une personne et mise a jour de son role (supression de ses anciens roles et création des nouveaux dans la table acpa_role_personne)
            $setterAcpa->updateContactById($db,$dataPersonne);
            $setterAcpa->removeAllRolesPersonne($db,$idContacts);
            $setterAcpa->addOneRolePersonne($db, $idContacts, $roleContacts);



        // modification du message
        $message .= "\nUtilisation de l'id recuperé " . $idContacts;

    }
    
    // execution des requetes    
    $acpa->execTransaction($db);
}
catch(Exception $e)
{
    // s'il y a une erreur on annule la transaction
    $acpa->abordTransaction($db);
    
    // Création d'un message d'erreur
    $message = "il y a eut une erreur l'activité n'a pas été ajoutée \n";
    $message .= 'Erreur : '.$e->getMessage()."\n";
    $message .= 'N° : '.$e->getCode();
    
}


// envoie du message pour affichage
echo $message;