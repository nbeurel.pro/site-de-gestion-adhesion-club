<?php
/* document ajax permettant la création, la suppression ou l'acceptation des moyens de paiement  dans la bdd depuis un script jquery */


// appel du fichier qui contient les requettes MySQL
require_once('../../model/Acpa.php');

// récuperation de la variable qui determine l'action sur le moyen de paiement
$action = $_POST['action'];

// création des instances de class
$acpa = new Acpa();
$setterAcpa = new SetterAcpa();
$getterAcpa = new GetterAcpa();


try
{
    // tentative de réalisation des requettes
    
    // connection a la BDD
    $db = $acpa->dbconnect();
    
    // demarrage de la transaction
    $acpa->startTransaction($db);
    
    // si l'action est delete
    if ($action == "delete"){

        // recuperation de la variable ID
        $idPayMethod = $_POST['idMoyenPaiement'];

        //appel de la fonction avec retour d'erreur le cas écheant
        $setterAcpa->deleteTheLinePayMethod($db, $idPayMethod);

        // création d'un message de confirmation
        $message = "la methode de paiement ".$idPayMethod." a bien été supprimé";

    // si l'action est d'accepter ou de resuser le moyen de paiement  
    }elseif ($action == "switchMoyen"){

        // recuperation des variables ID et accepte ou non
        $idPayMethod = $_POST['idMoyenPaiement'];
        $isAccept = $_POST['moyenAccepte'];

        //appel de la fonction avec retour d'erreur le cas écheant
        $setterAcpa->swithAcceptPayMethod($db, $idPayMethod, $isAccept);
         

        // création d'un message de confirmation selon l'acceptation ou non
        $message = 'le moyen de paiement: "'. $idPayMethod;
        if ($isAccept == '1'){
            $message .= '" a été ajouté aux moyens acceptés';
        }else{
            $message .= '" a été retiré des moyens acceptés';
        }

    // si l'action est create
    }elseif ($action == "create"){

        // recuperation des variables ID et accepte ou non
        $nomPayMethod = $_POST['nomMoyenPaiement'];
        $isAccept = $_POST['moyenAccepte'];

        //appel de la fonction avec retour d'erreur le cas écheant
        $setterAcpa->CreatePayMethod($db, $nomPayMethod, $isAccept);
        

        // création d'un message de confirmation
        $message = 'le moyen de paiement: "'. $nomPayMethod . '" a été créé';
    }
    
    // execution des requetes
   $acpa->execTransaction($db);
}
catch(Exception $e)
{
    // s'il y a une erreur on annule la transaction
    $acpa->abordTransaction($db);
    
    // Création d'un message d'erreur
    $message = "il y a eut une erreur l'activité n'a pas été ajoutée \n";
    $message .= 'Erreur : '.$e->getMessage()."\n";
    $message .= 'N° : '.$e->getCode();
    
}

// envoie du message de confirmation pour affichage
echo $message;