<?
require_once('../../model/Acpa.php');

$idAdhesion = $_POST['idAdhesion'];
    
$acpa = new Acpa();
$setterAcpa = new SetterAcpa();


try
{
    
    $db = $acpa->dbconnect();
    $acpa->startTransaction($db);
    $setterAcpa->removeAllActivity($db, $idAdhesion);
    $acpa->execTransaction($db);
}
catch(Exception $e)
{
    $acpa->abordTransaction($db);
    echo "il y a eut une erreur les modification n'ont pas été faite\n";
    echo "erreur: " . $e->getMessage() ."\n";
    echo "N°" . $e->getCode();
    exit();
}


echo "tout est supprimé";