<?php

require_once('../../model/Acpa.php');
$action = $_POST['action'];
$idPersonne = $_POST['idpersonne'];


$acpa = new Acpa();
$getterAcpa = new GetterAcpa();
$setterAcpa = new SetterAcpa();



try
{
    
    $db = $acpa->dbconnect();
    $acpa->startTransaction($db);
    
    if($action == 'seek'){

        $nbrPersonneMdp = $getterAcpa->getPersonnByID($db, $idPersonne);
        $personneMdp = $getterAcpa->getPersonnByID($db, $idPersonne);
        
        $nbr=$nbrPersonneMdp->fetchAll();
        $nbr=count($nbr);

        if($nbr == '1'){
            while ($personne = $personneMdp->fetch()){
                if(!empty($personne['num_licence_SIFFA'])){
                    $reponse =  '<div class="hightlightText">ADHERENT N°'.$personne['num_licence_SIFFA'].': ';
                }else{
                    $reponse = '<div class="hightlightText">CONTACT : ';
                }

                $reponse = $reponse .$personne['lib_prenom'].' '.$personne['lib_nom'].' - mail: ' .$personne['lib_mail_personne'].'</div><div class="group"><label for="idConnexion" class="labelAdherent" >login</label><input type="text" name="idConnexion" id="idConnexion" class="inputAdherent" value="'.$personne['login'].'"></div><div class="group"><label class="labelAdherent" for="pass">mot de passe</label><input type="text" name="pass" id="pass" class="inputAdherent"></div>';
            }

        }else{
            $reponse = '<div class="group">
                    <label for="idConnexion" class="labelAdherent">login</label>
                    <input type="text" name="idConnexion"  class="inputAdherent">
                </div>
                <div class="group">
                    <label for="pass" class="labelAdherent">mot de passe</label>
                    <input type="text" name="pass"  class="inputAdherent">
                </div>';
        }

        
    }else{
        $login = $_POST['loginPersonne'];
        $pass = password_hash($_POST['passwordPersonne'], PASSWORD_DEFAULT);

        $setterAcpa->addPassPersonne($db, $idPersonne, $login, $pass);

        $reponse = $idPersonne . '_' . $login . '-' . $pass;

    }
    
   $acpa->execTransaction($db);
}
catch(Exception $e)
{
    $acpa->abordTransaction($db);
    echo "il y a eut une erreur les modification n'ont pas été faite\n";
    echo "erreur: " . $e->getMessage() ."\n";
    echo "N°" . $e->getCode();
    exit();
}

echo $reponse;

    
?>