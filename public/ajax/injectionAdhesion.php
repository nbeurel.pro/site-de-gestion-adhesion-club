<?php


require_once('../../model/Acpa.php');

$probleme = "";
$newOldAdherent = $_POST['newOld'];	
$numLicSiffa = $_POST['numSiffa'];
$nom = $_POST['nameAdherent'];
$prenom = $_POST['nicknameAdherent'];
$sexeAdherent = $_POST['sexeAdherent'];
if($sexeAdherent == 'homme'){
    $sexe= mb_strtoupper("M");
}elseif($sexeAdherent == 'femme'){
    $sexe= mb_strtoupper("F");
}
$dateNaissance = $_POST['birthdayAdherent'];
$nationalite = mb_strtoupper($_POST['countryAdherent']);
$dteLicence = $_POST['dayAdhesion'];

$seasonAdhesion= $_POST['seasonAdhesion'];
        
$adress = mb_strtoupper($_POST['adressAdherent']);
$codepostale = $_POST['zipCodeAdherent'];
$ville = mb_strtoupper($_POST['cityAdherent']);
$mailPersonne = $_POST['eMailAdherent'];
$telMobile = $_POST['mobilePhoneAdherent'];
$TelFixe = $_POST['fixePhoneAdherent'];
$id_parent = NULL;
$catAge = $_POST['categorieAgeAdherent'];
$typeLic = $_POST['typeLicenceAdherent'];
$ListActivite = $_POST['activityAdherent'];
$libCertificatMedical = 'certificat medical ' . $prenom . ' ' . $nom;		
$cheminCertificatMedical = $_POST['medicalAdressAdherent'];
$identityLegalRepresentant = $_POST['identityParentalAdvisory'];
if(!isset($identityLegalRepresentant) || empty($identityLegalRepresentant)){
    $identityLegalRepresentant = NULL;
}            

$typLegalRepresentant = $_POST['typeParentalAdvisory'];
if(!isset($typLegalRepresentant) || empty($typLegalRepresentant)){
    $typLegalRepresentant = NULL;
}            

$identityEnfant = $_POST['identityYoungAdherent'];
if(!isset($identityEnfant) || empty($identityEnfant)){
    $identityEnfant = NULL;
}            

$prelevement = $_POST['bloodAutorisation'];
if(!isset($prelevement) || empty($prelevement)){
    $prelevement = NULL;
}

$hospitalisation = $_POST['hospitalisationAutorisation'];
if(!isset($hospitalisation) || empty($hospitalisation)){
    $hospitalisation = NULL;
}

$ruleAcceptance = $_POST['ruleAcceptance'];
if($ruleAcceptance == "1"){
    $reglementInterieur = "1";
}else{
    $reglementInterieur = "0";
}

$imageAcceptance= $_POST['imageAcceptance'];
if($imageAcceptance == "1"){
    $droitImage = "1";
}else{
    $droitImage = "0";
}

$cnilAcceptance= $_POST['cnilAcceptance'];
if($cnilAcceptance == "1"){
    $cnil = "1";
}else{
    $cnil = "0";
}

$insuranceAcceptance= $_POST['insuranceAcceptance'];
if($insuranceAcceptance == "1"){
    $assurance = "1";
}else{
    $assurance = "0";
}

$idOrganisation = NULL;

$etatPayement = $_POST['PaymentState'];
$paymentKey = $_POST['paymentKey'];

$dateAdhesion = $dteLicence;

$ListActivite = substr($ListActivite,0,-2);
$activity = explode("; ", $ListActivite);
$login = $prenom . "." . $nom . substr($dateNaissance,2,-6);
$nom = mb_strtoupper($nom);
$prenom = mb_strtoupper($prenom);

$dataPersonne = array('numLicenceSIFFA' => $numLicSiffa,
                                'dtLicence' => $dteLicence,
                                'libNom' => $nom,
                                'libPrenom' => $prenom,
                                'libSexe' => $sexe,
                                'dtNaissance' => $dateNaissance,
                                'libNationalite' => $nationalite,
                                'libAdresse' => $adress,
                                'numCodePostal' => $codepostale,
                                'libVille' => $ville,
                                'libMailPersonne' => $mailPersonne,
                                'telMobile' => $telMobile,
                                'telFixe' => $TelFixe,
                                'login' => $login,
                                'idParent' => $id_parent,
                                'idOrganisation' => $idOrganisation);
												
$cheminCertificatMedical = file_get_contents($cheminCertificatMedical);

$dateDeposeCertificat = new DateTime($dteLicence); 
$dateValidite = clone $dateDeposeCertificat;
$interval = new DateInterval('P3Y');
$dateValidite -> add($interval);

$typeDocument = '19';

$acpa = new Acpa();
$setterAcpa = new SetterAcpa();
$getterAcpa = new GetterAcpa();

try
{
    
    $db = $acpa->dbconnect();
    $acpa->startTransaction($db);
    
    if ($newOldAdherent == 'new'){

        // INSERTION des informations du formulaire dans la table acpa_personne
        $setterAcpa->addNewPersonne($db, $dataPersonne);
        
        // RECUPERATION de l'identifiant de la dernière entrée de la table acpa_personne
        $lastAdherent = $getterAcpa->getLastPerson($db);
        

        $adherent = $lastAdherent->fetch();
        $idAdherent = $adherent['max_id'];
    }else{
         // RECUPERATION de l'identifiant de l'entrée mise a jour de la table acpa_personne
        $numAdherent = $getterAcpa->getIdUpdatePerson($db, $nom, $prenom, $dateNaissance);     

        $adherent = $numAdherent->fetch();
        if($adherent['id_personne'] == ""){        
            exit('cet adherent n\'existe pas');
        }else{
            $idAdherent = $adherent['id_personne'];
            $message = ('adherent trouvé: ' . $idAdherent);

            $dataPersonne2 = array('numLicenceSIFFA' => $numLicSiffa,
                                    'libSexe' => $sexe,
                                    'libNationalite' => $nationalite,
                                    'libAdresse' => $adress,
                                    'numCodePostal' => $codepostale,
                                    'libVille' => $ville,
                                    'libMailPersonne' => $mailPersonne,
                                    'telMobile' => $telMobile,
                                    'telFixe' => $TelFixe,
                                    'login' => $login,
                                    'idParent' => $id_parent,
                                    'idAdherent' => $idAdherent);
        }
        // MAJ des informations du formulaire dans la table acpa_personne
        $setterAcpa->updateAdherent($db, $dataPersonne2);     

    }

    // RECUPERATION de l'identifiant de la saison ouverte dans la table acpa_saison
    
    $idSeasonAdhesion = $getterAcpa->getSeasonAdhesion($db, $seasonAdhesion);
      
    $openSeason = $idSeasonAdhesion->fetch();

    // RECUPERATION de l'identifiant du statut de paiement "paiement en attente" dans la table acpa_statut

    $idStatutPayment = $getterAcpa->getStatusPayAdhesion($db, $etatPayement);
         
    $statutPayment = $idStatutPayment->fetch();


    // RECUPERATION de l'identifiant de la categorie d'age dans la table acpa_categorie_age	
    $idCatAge = $getterAcpa->getCategoryAdhesion($db, $catAge);
        
    $catAge = $idCatAge->fetch();

    // RECUPERATION de l'identifiant du type de licence dans la table acpa_type_licence

    $idTypeLicence = $getterAcpa->getTypLicAdhesion($db, $typeLic);
         
    $typeLicence = $idTypeLicence->fetch();

    $datValidate = date("Y-m-d");

    // INSERTION des informations du formulaire dans la table acpa_adhesion

    $dataAdhesion = array('identiteRepLegal' => $identityLegalRepresentant,
                            'typeRepLegal' => $typLegalRepresentant,
                            'identiteEnfant' => $identityEnfant,
                            'prelevementSanguin' => $prelevement,
                            'hospitalisation' => $hospitalisation,
                            'reglementInterieur' => $reglementInterieur,
                            'assurance' => $assurance,
                            'droitImage' => $droitImage,
                            'cnil' => $cnil,
                            'dateValidation' => $datValidate,
                            'dateAdhesion' => $dateAdhesion,
                            'idGrpAdhesion' => $paymentKey,
                            'idPersonne' => $idAdherent,
                            'idSaison' => $openSeason['id_saison'],
                            'idStatut' => $statutPayment['id_statut'],
                            'idCatAge' => $catAge['id_categorie_age'],
                            'idTypLic' => $typeLicence['id_type_licence'],
                            'mntTarifLicence' => $typeLicence['mnt_tarif_licence']);

    $setterAcpa->addNewAdhesion($db, $dataAdhesion);   

    // RECUPERATION de l'identifiant de la dernière entrée de la table acpa_adhesion
    $lastAdhesion = $getterAcpa->getLastAdhesion($db);
         
    $adhesion = $lastAdhesion->fetch();

    $idAdhesion = $adhesion['max_idad'];

    //debut de la boucle de gestion des activites			
    foreach($activity as $activite){

        // RECUPERATION de l'identifiant de chaque activite de la table  acpa_activite
        $idactivity = $getterAcpa->getIdsActivitysAdhesion($db, $activite);
             
        $idactivite = $idactivity->fetch();

        // INSERTION des couple id adhesion id activité acpa_activite_adh


        $idActiviteAdhesion = $idactivite['id_activite']; 
        $setterAcpa -> addNewActivityAdhesion($db, $idAdhesion, $idActiviteAdhesion);
        
    }

    $dataimage = array('libDocument'=> $libCertificatMedical, 
                                    'cheminFichier'=> $cheminCertificatMedical, 
                                    'dateValidite'=> $dateValidite -> format('Y-m-d'), 
                                    'dateRemise'=> $dateDeposeCertificat -> format('Y-m-d'), 
                                    'idAdhesion'=> $idAdhesion, 
                                    'idTypeDocument'=> $typeDocument, 
                                    'idPersonne'=> $adherent['max_id']);

    $setterAcpa->addNewDoc($db, $dataimage);
     
    $setterAcpa->addRoleNewAdhesion($db, $idAdhesion);    
    
    $acpa->execTransaction($db);
}
catch(Exception $e)
{
    $acpa->abordTransaction($db);
    echo "il y a eut une erreur les modification n'ont pas été faite\n";
    echo "erreur: " . $e->getMessage() ."\n";
    echo "N°" . $e->getCode();
    exit();
} 

$message = 'la base de données a été mise à jours';

echo $message;	

