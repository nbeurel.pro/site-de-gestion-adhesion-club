<?php
/* document ajax permettant l'ajout, la suppression ou la modification des categories et la mise a jours des categories pour les adherents dans la bdd depuis un script jquery */


// appel du fichier qui contient les requettes MySQL
require_once('../../model/Acpa.php');

// récuperation de la variable qui determine l'action sur la catégorie
$action = $_POST['action'];

// création des instances de class
$acpa = new Acpa();
$setterAcpa = new SetterAcpa();
$getterAcpa = new GetterAcpa();


try
{
    // tentative de réalisation des requettes
    
    // connection a la BDD
    $db = $acpa->dbconnect();
    $acpa->startTransaction($db);
    
    // si l'action est modification
    // action sur la table categories
    if ($action == "modify"){

        //recuperation des variables
        $idCategory = $_POST['idCategorie'];
        $nomCategory = $_POST['nomCategorie'];
        $libCategory = $_POST['libCategorie'];
        $numCategory = $_POST['numCategorie'];
        $condCategory = $_POST['condCategorie'];

        //appel de la fonction avec retour d'erreur le cas écheant
        $setterAcpa->modifyCategory($db, $idCategory, $nomCategory, $libCategory, $numCategory, $condCategory);


        // création d'un message de confirmation
        $message = "la catégorie ".$idCategory." a été modifiée";

    // si l'action est suppression 
    // action sur la table categories  
    }elseif ($action == "delete"){

        // récuperation des variables
        $idCategory = $_POST['idCategorie'];

        //appel de la fonction avec retour d'erreur le cas écheant
        $setterAcpa->deleteTheLineCategory($db, $idCategory);
        

        // création d'un message de confirmation
        $message = "la catégorie ".$idCategory." a bien été supprimé";

    // si l'action est création 
    // action sur la table categories  
    }elseif ($action == "create"){

        // récuperation des variables
        $nomCategory = $_POST['nomCategorie'];
        $libCategory = $_POST['libCategorie'];
        $numCategory = $_POST['numCategorie'];
        $condCategory = $_POST['condCategorie'];

        //appel de la fonction avec retour d'erreur le cas écheant
        $setterAcpa->CreateCategory($db, $nomCategory, $libCategory, $numCategory, $condCategory);

        // création d'un message de confirmation
        $message = 'la catégorie: "'. $nomCategory . '" a été créé';

     // si l'action est mise a jours des categories
    // action sur la table adhesion   
    }elseif ($action == "majCatAge"){

        // récuperation des variables
        $idCategory = $_POST['idCategorie'];
        $idAdhesion = $_POST['idAdhesion'];

        //appel de la fonction avec retour d'erreur le cas écheant
       
        $setterAcpa->updateCatAgeAdhesionById($db, $idCategory, $idAdhesion);
         
        // création d'un message de confirmation
        $message = $idAdhesion . ' a été mise a jours';
    }
    
    // execution des requetes
    $acpa->execTransaction($db);
}
catch(Exception $e)
{
    // s'il y a une erreur on annule la transaction
    $acpa->abordTransaction($db);
    
    // Création d'un message d'erreur
    $message = "il y a eut une erreur l'action n'a pas été faite \n";
    $message .= 'Erreur : '.$e->getMessage()."\n";
    $message .= 'N° : '.$e->getCode();
    
}

// envoie du message pour affichage
echo $message;