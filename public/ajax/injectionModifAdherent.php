<?php

require_once('../../model/Acpa.php');

$idPersonne = $_POST['idPersonne'];
if(!empty($_POST['numSIFFA']))
{
    $numSIFFA = $_POST['numSIFFA'];
}else{
   $numSIFFA = '0'; 
}
if(!empty($_POST['dteLicence']))
{
    $dteLicence = $_POST['dteLicence'];
}else{
   $dteLicence = NULL; 
}
if(!empty($_POST['nameAdherent']))
{
    $nameAdherent = $_POST['nameAdherent'];
}else{
   $nameAdherent = NULL; 
}
if(!empty($_POST['firstNameAdherent']))
{
    $firstNameAdherent = $_POST['firstNameAdherent'];
}else{
   $firstNameAdherent = NULL; 
}
if(!empty($_POST['sexeAdherent']))
{
    $sexeAdherent = $_POST['sexeAdherent'];
}else{
   $sexeAdherent = NULL; 
}
if(!empty($_POST['birthdayAdherent']))
{
    $birthdayAdherent = $_POST['birthdayAdherent'];
}else{
   $birthdayAdherent = NULL; 
}
if(!empty($_POST['country']))
{
    $country = $_POST['country'];
}else{
   $country = NULL; 
}
if(!empty($_POST['adress']))
{
    $adress = $_POST['adress'];
}else{
   $adress = NULL; 
}
if(!empty($_POST['zipCode']))
{
    $zipCode = $_POST['zipCode'];
}else{
   $zipCode = NULL; 
}
if(!empty($_POST['city']))
{
    $city = $_POST['city'];
}else{
   $city = NULL; 
}
if(!empty($_POST['mail']))
{
    $mail = $_POST['mail'];
}else{
   $mail = NULL; 
}
if(!empty($_POST['mobilePhone']))
{
    $mobilePhone = $_POST['mobilePhone'];
}else{
   $mobilePhone = NULL; 
}
if(isset($_POST['fixePhone']) && !empty($_POST['fixePhone']))
{
    $fixePhone = $_POST['fixePhone'];
}else{
   $fixePhone = NULL; 
}
if(!empty($_POST['idConnexion']))
{
    $idConnexion  = $_POST['idConnexion'];
}else{
   $idConnexion = NULL; 
}

if(!empty($_POST['dteLastConnect']))
{
    $dteLastConnect = $_POST['dteLastConnect'];
}else{
   $dteLastConnect = NULL; 
}
if(!empty($_POST['idParent']))
{
    $idparent = $_POST['idParent'];
}else{
   $idparent = NULL; 
}

$idAdhesion = $_POST['idAdhesion'];
if(!empty($_POST['respLegal']))
{
    $respLegal = $_POST['respLegal'];
}else{
   $respLegal = NULL; 
}
if(!empty($_POST['typeRespLegal']))
{
    $typeRespLegal = $_POST['typeRespLegal'];
    if(!empty($_POST['identityYoungAdherent']))
    {
        $identityYoungAdherent = $_POST['identityYoungAdherent'];
    }else{
       $identityYoungAdherent = NULL; 
    }
}else{
   $typeRespLegal = NULL; 
   $identityYoungAdherent = NULL;
}
$authSample = $_POST['authSample'];
$authHospital = $_POST['authHospital'];
$interiorRule = $_POST['interiorRule'];
$assurance = $_POST['assurance'];
$authProfil = $_POST['authProfil'];
$authRGPD = $_POST['authRGPD'];
if(!empty($_POST['datePaiement']))
{
    $datePaiement = $_POST['datePaiement'];
}else{
   $datePaiement = NULL; 
}
if(!empty($_POST['dateAdhesion']))
{
    $dateAdhesion = $_POST['dateAdhesion'];
}else{
   $dateAdhesion = NULL; 
}
if(!empty($_POST['grpAdherent']))
{
    $grpAdherent = $_POST['grpAdherent'];
}else{
   $grpAdherent = NULL; 
}
if(!empty($_POST['mntAdhesion']))
{
    $mntAdhesion = $_POST['mntAdhesion'];
}else{
   $mntAdhesion = NULL; 
}
if(!empty($_POST['idSaison']))
{
    $idSaison = $_POST['idSaison'];
}else{
   $idSaison = NULL; 
}
if(!empty($_POST['statusAdhesion']))
{
    $statusAdhesion = $_POST['statusAdhesion'];
}else{
   $statusAdhesion = NULL; 
}
if(!empty($_POST['catAge']))
{
    $catAge = $_POST['catAge'];
}else{
   $catAge = NULL; 
}
if(!empty($_POST['typeLic']))
{
    $typeLic = $_POST['typeLic'];
}else{
   $typeLic = NULL; 
}



$acpa = new Acpa();
$setterAcpa = new SetterAcpa();


try
{
    
    $db = $acpa->dbconnect();
    $acpa->startTransaction($db);

    $dataPersonne = array('numSIFFA' => $numSIFFA,
                            'dteLicence' => $dteLicence,
                            'nameAdherent' => $nameAdherent,
                            'firstNameAdherent' => $firstNameAdherent,
                            'sexeAdherent' => $sexeAdherent,
                            'birthdayAdherent' => $birthdayAdherent,
                            'country' => $country,
                            'adress' => $adress,
                            'zipCode' => $zipCode,
                            'city' => $city,
                            'mail' => $mail,
                            'mobilePhone' => $mobilePhone,
                            'fixePhone' => $fixePhone,
                            'idConnexion' => $idConnexion,
                            'dteLastConnect' => $dteLastConnect,
                            'idParent' => $idparent,
                            'idPersonne' => $idPersonne);

    $setterAcpa->updateAdherentById($db, $dataPersonne); 

    $dataAdhesion = array('respLegal' => $respLegal,
                            'typeRespLegal' => $typeRespLegal,
                            'identityYoungAdherent' => $identityYoungAdherent,
                            'authSample' => $authSample,
                            'authHospital' => $authHospital,
                            'interiorRule' => $interiorRule,
                            'assurance' => $assurance,
                            'authProfil' => $authProfil,
                            'authRGPD' => $authRGPD,
                            'datePaiement' => $datePaiement,
                            'dateAdhesion' => $dateAdhesion,
                            'grpAdherent' => $grpAdherent,
                            'mntAdhesion' => $mntAdhesion,
                            'idPersonne' => $idPersonne,
                            'idSaison' => $idSaison,
                            'statusAdhesion' => $statusAdhesion,
                            'catAge' => $catAge,
                            'typeLic' => $typeLic,
                            'idAdhesion' => $idAdhesion);



    $setterAcpa->updateAdhesionById($db, $dataAdhesion);
    
    $acpa->execTransaction($db);
}
catch(Exception $e)
{
    $acpa->abordTransaction($db);
    echo "il y a eut une erreur les modification n'ont pas été faite\n";
    echo "erreur: " . $e->getMessage() ."\n";
    echo "N°" . $e->getCode();
    exit();
}



$message = "identifiant: " . $idPersonne . " \n numerosiffa: " . $numSIFFA . " \n date de licence: " . $dteLicence . " \n nom: " . $nameAdherent . " - prenom: " . $firstNameAdherent . " sexe: " . $sexeAdherent . " \n date de naissance: " . $birthdayAdherent . " - nationalite: " . $country . " \n adress: " . $adress . "  - " . $zipCode . " - " . $city . " \n mail: " . $mail . " \n mobile: " . $mobilePhone . "  - fixe: " . $fixePhone . " \n idconnexion: " . $idConnexion . " : " . $pwdConnexion . " \n lastConnexion: " . $dteLastConnect . " \n idparent: " . $idparent . " \n idAdhesion: " . $idAdhesion . " \n repLegal: " . $respLegal . " : " . $typeRespLegal . " \n identiteEnfant: " . $identityYoungAdherent . " \n prelevement: " . $authSample . "  -  hospitalisation: " . $authHospital . "  -  RI: " . $interiorRule . "  -  assurance: " . $assurance . "  -  image: " . $authProfil . "  -  rgpd: " . $authRGPD . " \n date paiement: " . $datePaiement . " \n date adhesion: " . $dateAdhesion . " \n grp: " . $grpAdherent . "  -  tarif: " . $mntAdhesion . " \n saison: " . $idSaison . "  - statuts: " . $statusAdhesion . " \n categorie: " . $catAge . "  - typelic: " . $typeLic . " \n idpayement: " . $idPayement . " \n chemin fich: " . $bin_fichier . " \n idcertif: " . $idCertifMedical;

echo $message;	