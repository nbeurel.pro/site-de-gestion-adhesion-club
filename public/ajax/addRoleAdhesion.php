<?
/* document ajax permettant l'ajout d'un role a une adhesion dans la bdd depuis un script jquery */


// appel du fichier qui contient les requettes MySQL
require_once('../../model/Acpa.php');


// recuperation des variables
$idAdhesion = $_POST['idAdhesion'];
$idRole = $_POST['idRole'];

// création des instances de class
$acpa = new Acpa();
$setterAcpa = new SetterAcpa();

try
{
    // tentative de réalisation des requettes
    
    // connection a la BDD
    $db = $acpa->dbconnect();
    
    // demarrage de la transaction
    $acpa->startTransaction($db);
    
    // Test des requetes
    $setterAcpa->addOneRoleAdhesion($db, $idAdhesion, $idRole);
    
    // execution des requetes
   $acpa->execTransaction($db);
}
catch(Exception $e)
{
    // si il y a une erreur on annule la transaction
    $acpa->abordTransaction($db);
    
    // Création d'un message d'erreur
    $message = "il y a eut une erreur le role n'a pas été ajoutée \n";
    $message .=  'Erreur : '.$e->getMessage()."\n";
    $message .= 'N° : '.$e->getCode();
    
    // envoi du message d'erreur pour affichage et arret de l'execution
    echo $message;
    exit();
}

// renvoie un message
echo 'role ' . $idRole . ' créer pour :' .$idAdhesion;