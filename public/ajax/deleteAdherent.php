<?php
/* document ajax permettant la suppression d'un adherent dans la bdd wordpress depuis un script jquery */


// appel du fichier qui contient les requettes MySQL
require_once('../../model/Wordpress.php');

// verification qu'une variable existe (securité)
if(!empty($_POST))
{	
    // récuperation de la variable
	$ligne = $_POST['id'];
    
    // appel de la fonction
	$setterWordpress = new SetterWordpress();
	$deleteLine = $setterWordpress->deleteTheLine($ligne);
    
    // renvoi d'un message 
    echo 'ligne supprimé: ' . $ligne;
}
else
{
    // si pas de variable affichage d'un autre message
	echo('erreur');
	
}

?>
