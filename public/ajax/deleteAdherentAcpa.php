<?php


require_once('../../model/Acpa.php');

// recuperation des variables
$idPersonne=$_POST['idadherent'];

// création des instances de class
$acpa = new Acpa();
$setterAcpa = new SetterAcpa();
$getterAcpa = new GetterAcpa();


try
{
    // tentative de réalisation des requettes
    
    // connection a la BDD
    $db = $acpa->dbconnect();
    
    // demarrage de la transaction
    $acpa->startTransaction($db);
    
    // récuperation de la liste des adhesions liés
    $listAdhesionByPersonne = $getterAcpa->idAdhesionByIdPersonne($db, $idPersonne);
   
    // pour chaque adhesion
    while ($adhesionforID = $listAdhesionByPersonne->fetch()) 
    {
        // recuperation de l'ID de l'adhesion
        $idAdhesion = $adhesionforID['id_adhesion'];
       
        // suppression des liens avec les activités
        $setterAcpa->removeAllActivity($db, $idAdhesion);
       
        // suppression des documents
        $setterAcpa->removeDocAdhesion($db, $idAdhesion);
               
        // suppression des liens avec les roles
        $setterAcpa->removeAllRolesAdhesion($db, $idAdhesion);
              
        // suppression de l'adhesion
        $setterAcpa->deleteTheLineAdhesion($db, $idAdhesion);
    }

    
    // suppresion des documents
    $setterAcpa->removeDocAdherent($db, $idPersonne);
         
    // suppression des liens avec les groupes ged
    $setterAcpa->removeAllGroupePersonne($db, $idPersonne);
    
    // suppression des liens parent avec les adherents
    $setterAcpa->deleteLinkChildPersonne($db, $idPersonne);
    
    // suppression de la personnes
    $setterAcpa->deleteTheLineContact($db, $idPersonne);
    
    // execution des requetes
    $db->commit();
    
    // creation d'un message en cas de reussite
    $message = "adherent supprimé";
 

   
}
catch (Exception $e)
{
    // si une erreur on annule les requetes
    $db->rollback();
    
    // creation d'un message en cas d'erreur
    $message = "il y a eut des erreures \n";
    $message .= "erreur: " . $e->getMessage() ."\n";
    $message .=  "N°" . $e->getCode();
    $message .=  "l'adherent n'a pas été supprimé\n";
} 

// renvoie du message
echo $message;
    