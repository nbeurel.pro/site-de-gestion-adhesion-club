<?php
/* document ajax permettant la suppression d'un adherent dans la bdd acpa depuis un script jquery */


// appel du fichier qui contient les requettes MySQL
require_once('../../model/Acpa.php');

// verification qu'une variable existe (securité)
if(!empty($_POST))
{	
    // récuperation de la variable
	$idPersonne = $_POST['NumContact'];

// création des instances de class
    $acpa = new Acpa();
	$setterAcpa = new SetterAcpa();
    
    try
    {
        // tentative de réalisation des requettes

        // connection a la BDD
        $db = $acpa->dbconnect();
    
        // demarrage de la transaction
        $acpa->startTransaction($db);
    
        // Test des requetes
        $setterAcpa->removeAllRolesContact($db, $idPersonne);
        $setterAcpa->deleteTheLineContact($db, $idPersonne);
    
        // execution des requetes
        $acpa->execTransaction($db);
    }
    catch(Exception $e)
    {
        // s'il y a une erreur on annule la transaction
        $acpa->abordTransaction($db);
    
        // Création d'un message d'erreur
        $message = "il y a eut une erreur le contact n'a pas été supprimé \n";
        $message .= 'Erreur : '.$e->getMessage()."\n";
        $message .= 'N° : '.$e->getCode();

        // envoi du message d'erreur pour affichage et arret de l'execution
        echo $message;
        exit();
    }
    
    // renvoi d'un message 
    echo 'ligne supprimé: ' . $idPersonne;
}
else
{
    // si pas de variable affichage d'un autre message
	echo('erreur');
	
}

?>