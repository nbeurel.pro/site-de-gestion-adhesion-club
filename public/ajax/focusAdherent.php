<?php
/* document ajax permettant la création de la fiche adherant avant son affichage*/


//verification de la presence de variable
if(!empty($_POST))
{
	
    // ouverture d'une session
    session_start();
    
    // appel du fichier qui contient les requettes MySQL
    require_once('../../model/Acpa.php');
	
    // recuperation des variables
	$nomAdherent = $_POST['nAt'];
	$prenomAdherent = $_POST['pAt'];
	$mailAdherent = $_POST['mAt'];
    $saison = $_POST['sAt'];

    // création des instances de class
    $acpa = new Acpa();
	$getterAcpa = new GetterAcpa();
    
    
    
    try
    {
        // tentative de réalisation des requettes

        // connection a la BDD
        $db = $acpa->dbconnect();
    
        // demarrage de la transaction
        $acpa->startTransaction($db);
    
        // Test des requetes
        $adherent = $getterAcpa->getAdherent($db, $nomAdherent, $prenomAdherent, $mailAdherent, $saison);


        // definition du sexe en fonction de la données stocké dans la bdd
        $adherant = $adherent->fetch();
        if($adherant['lib_sexe'] == 'M')
        {
            $sexe = 'Masculin';
        }else{
            $sexe = 'Féminin';
        }

        // Creation de variable selon les données de la bdd
        $idAdherent = $adherant['id_personne'];
        $idAdhesion = $adherant['id_adhesion_1'];

        // création d'une partie de la fiche adherants
        $contenu = '<h2 class="h2View">Détails de l\'adhesion: ' . $idAdhesion . '</h2>
                <div class="container">
                    <div class="column">
                         <h3> identifiant adhérent: <span class="idAdherent">' . $idAdherent . '</span></h3>
                        <div class="group">
                            <label class="labelAdherent" for="siffa" >N° de licence SIFFA</label> 
                            <input class="inputAdherent" type="text" id="siffa" name="siffa" value="' . $adherant['num_licence_SIFFA'] . '" disabled/>
                        </div>
                        <div class="group">
                            <label class="labelAdherent" for="licence_date" >Date de licence</label>
                            <input class="inputAdherent" type="date" id="licence_date" name="licence_date" value="' . $adherant['dt_licence'] . '" disabled/>
                        </div>
                        <div class="group">
                            <label class="labelAdherent" for="name" >Nom</label>
                            <input class="inputAdherent" type="text" id="name" name="name" value="' . $adherant['lib_nom'] . '" disabled/>
                        </div>
                        <div class="group">
                            <label class="labelAdherent" for="first_name" >Prénom</label>
                            <input class="inputAdherent" type="text" id="first_name" name="first_name" value="' . $adherant['lib_prenom'] . '" disabled/>
                        </div>
                        <div class="group">
                            <label class="labelAdherent" for="gender" >Sexe</label>
                            <input class="inputAdherent" type="text" id="gender" value="' . $sexe . '" disabled/>
                        </div>
                        <div class="group">
                            <label class="labelAdherent" for="birthdate" >Date de naissance</label>
                            <input class="inputAdherent" type="date" id="birthdate" name="birthdate" value="' . $adherant['dt_naissance'] . '" disabled/>
                        </div>
                        <div class="group">
                            <label class="labelAdherent" for="country" >Nationalité</label>
                            <input class="inputAdherent" type="text" id="country" name="country" value="' . $adherant['lib_nationalite'] .'" disabled/>
                        </div>
                        <div class="group">
                            <label class="labelAdherent" for="adress" >Adresse complète</label>
                            <input class="inputAdherent" type="text" id="adress" name="adress" value="' . $adherant['lib_adresse'] . '" disabled/>
                        </div>
                        <div class="group">
                            <label class="labelAdherent" for="postal" >Code postal</label>
                            <input class="inputAdherent" type="text" id="postal" name="postal" value="' . $adherant['num_code_postal'] . '" disabled/>
                        </div>
                        <div class="group">
                            <label class="labelAdherent" for="city" >Ville</label>
                            <input class="inputAdherent" type="text" id="city" name="city" value="' . $adherant['lib_ville'] . '" disabled/>
                        </div>
                        <div class="group">
                            <label class="labelAdherent" for="mail" >Email</label>
                            <input class="inputAdherent" type="email" id="mail" name="mail" value="' . $adherant['lib_mail_personne'] . '" disabled/>
                        </div>
                        <div class="group">
                            <label class="labelAdherent" for="mobile" >Téléphone portable</label>
                            <input class="inputAdherent" type="text" id="mobile" name="mobile" value="' . $adherant['tel_mobile'] . '" disabled/>
                        </div>
                        <div class="group">
                            <label class="labelAdherent" for="phone" >Téléphone fixe</label>
                            <input class="inputAdherent" type="text" id="phone" name="phone" value="' . $adherant['tel_fixe'] . '" disabled/>
                        </div>
                        <div class="group">
                            <label class="labelAdherent" for="last_date" >Date de dernière connexion</label>
                            <input class="inputAdherent" type="date" id="last_date" name="last_date" value="' . $adherant['dtm_connexion'] . '" disabled/>
                        </div>
                    </div>
                    <div class="column">
                        <h3 class="h3View">Saison en cours</h3>';
        $contenu = $contenu . '<div class="group">';

        // ajout ou non de la partie responsable légal
        if($adherant['lib_nom_prenom_responsable_legal'] == '')
        {
            $contenu = $contenu . '<label class="labelAdherent labelNull" for="legal" >Responsable légal</label>
                            <input class="inputAdherent" type="text" id="legal" name="legal" value="Non applicable" disabled/>';
        }else{
            $contenu = $contenu . '<label class="labelAdherent" for="legal" >Responsable légal</label>
                            <input class="inputAdherent2" type="text" id="legal" name="legal" value="' . $adherant['lib_nom_prenom_responsable_legal'] . '" disabled/><input class="inputAdherent3" type="text" id="typelegal" name="typelegal" value="' . $adherant['lib_type_responsable_legal'] . '" disabled/>';
        }

        // ajout de contenu à la fiche adherants
        $contenu = $contenu . '</div>
                        <div class="group">
                            <label class="labelAdherent" for="cat_age" >Catégorie d\'âge</label>
                            <input class="inputAdherent" type="text" id="cat_age" name="cat_age" value="' . $adherant['lib_categorie_age'] . '" disabled/>
                        </div><div class="group">
                            <label  class="labelAdherent">Activités</label>
                           <textarea class="textareAdherent" rows="1" cols="25" disabled>' . str_replace(",","\n",$adherant['list_activite']) . '</textarea>
                        </div>
                        <div class="group">
                            <label class="labelAdherent">Rôles</label>
                            <textarea class="textareAdherent2" rows="1" cols="25" disabled>' . str_replace(",","\n",$adherant['list_role']) . '</textarea>
                        </div>
                        <div class="group">
                            <label class="labelAdherent" for="licence" >Type de licence</label>
                            <input class="inputAdherent" type="text" id="licence" name="licence"  value="' . $adherant['lib_type_licence'] . '" disabled/>
                        </div>
                        <div class="group">
                            <label class="labelAdherent" for="date_season" >Date d\'adhésion pour la saison</label>
                            <input class="inputAdherent" type="date" id="date_season" name="date_season"  value="' . $adherant['dtm_adhesion'] . '" disabled/>
                        </div>
                        <div class="group">
                            <label class="labelAdherent" for="statut" >Statut de l\'adhésion</label>
                            <input class="inputAdherent" type="text" id="statut" name="statut" value="' . $adherant['lib_statut'] . '" disabled/>
                        </div>
                        <div class="group">
                            <label class="labelAdherent" for="date_paiement" >Date de validation du paiement</label>
                            <input class="inputAdherent" type="date" id="date_paiement" name="date_paiement" value="' . $adherant['dtm_validation'] . '"disabled/>
                        </div>
                        <h4>Autorisation parental</h4>
                        <div class="group groupCheckbox groupCheckboxLeft">';

        // ajout ou non des partie autorisation liée au mineur
        if($adherant['lib_nom_prenom_responsable_legal'] == '')
        {
            $contenu = $contenu . '<label class="labelcheckbox labelNull" for="prelevementsanguin" >Prélevement sanguin</label>
                                <input class="inputAdherentCheckbox" type="checkbox" id="prelevementsanguin" name="prelevementsanguin" disabled/>
                            </div>
                            <div class="group groupCheckbox groupCheckboxRight">
                                <label class="labelcheckbox labelNull" for="hospitalisation" >Hospitalisation</label>
                                <input class="inputAdherentCheckbox" type="checkbox" id="hospitalisation" name="hospitalisation" disabled/>';
        }else{       
            if($adherant['yn_prelevement'] == 1)
            {
                $contenu = $contenu . '<label class="labelcheckbox" for="prelevementsanguin" >Prélevement sanguin</label>
                                <input class="inputAdherentCheckbox" type="checkbox" id="prelevementsanguin" name="prelevementsanguin" disabled checked/>';
            }else{
                $contenu = $contenu . '<label class="labelcheckbox" for="prelevementsanguin" >Prélevement sanguin</label>
                                <input class="inputAdherentCheckbox" type="checkbox" id="prelevementsanguin" name="prelevementsanguin" disabled/>';
            }

            $contenu = $contenu . '</div>
                               <div class="group groupCheckbox groupCheckboxRight">';

            if($adherant['yn_hospitalisation'] == 1)
            {
                $contenu = $contenu . '<label class="labelcheckbox" for="hospitalisation" >Hospitalisation</label>
                                <input class="inputAdherentCheckbox" type="checkbox" id="hospitalisation" name="hospitalisation" disabled checked/>';
            }else{
                $contenu = $contenu . '<label class="labelcheckbox" for="hospitalisation" >Hospitalisation</label>
                                <input class="inputAdherentCheckbox" type="checkbox" id="hospitalisation" name="hospitalisation" disabled/>';
            }
        }

        // ajout de contenu a la fiche adherent
        $contenu = $contenu . '</div><br>
                        <h4>Acceptation</h4>
                        <div class="group groupCheckbox groupCheckboxLeft">
                        <label class="labelcheckbox" for="reglement">Règlement intérieur</label>';


        // ajout des checkbox coché / non coché à la fiche adherent
        if($adherant['yn_reglement_interieur'] == 1)
        {
            $contenu = $contenu . '<input class="inputAdherentCheckbox" type="checkbox" id="reglement" name="reglement" disabled checked/>';
        }else{
            $contenu = $contenu . '<input class="inputAdherentCheckbox" type="checkbox" id="reglement" name="reglement"  disabled/>';        
        }

         $contenu = $contenu . '</div>
                        <div class="group groupCheckbox groupCheckboxRight">
                        <label class="labelcheckbox" for="insurance">Assurance</label>';

        if($adherant['yn_assurance'] == 1)
        {
            $contenu = $contenu . '<input class="inputAdherentCheckbox" type="checkbox" id="insurance" name="insurance"  disabled checked/>';
        }else{
            $contenu = $contenu . '<input class="inputAdherentCheckbox" type="checkbox" id="insurance" name="insurance" disabled/>';        
        }

        $contenu = $contenu . '</div>
                        <div class="group groupCheckbox groupCheckboxLeft">
                        <label class="labelcheckbox" for="image" >Utilisation de son image</label>';

        if($adherant['yn_droit_image'] == 1)
        {
            $contenu = $contenu . '<input class="inputAdherentCheckbox" type="checkbox" id="image" name="image" disabled checked/>';
        }else{
            $contenu = $contenu . '<input class="inputAdherentCheckbox" type="checkbox" id="image" name="image" disabled/>';        
        }

        $contenu = $contenu . '</div>
                        <div class="group groupCheckbox groupCheckboxRight">
                            <label class="labelcheckbox" for="law" >Loi informatique et libertés</label>';

        if($adherant['yn_cnil'] == 1)
        {
            $contenu = $contenu . '<input class="inputAdherentCheckbox" type="checkbox" id="law" name="law" disabled checked/>';
        }else{
            $contenu = $contenu . '<input class="inputAdherentCheckbox" type="checkbox" id="law" name="law" disabled/>';        
        }

        // ajout de contenu à la fiche adherent
        $contenu = $contenu . '</div>

                        <div class="group">
                            <label class="labelAdherent">Certificat médical:</label>
                            <a href="document.php?id='. $adherant['id_document'] .'">Consulter le document '. $adherant['id_document'] .'</a>
                        </div>';
        $contenu = $contenu . '</div>
                </div>
                <div class="control">
                    <button type="button" class="buttonAdherent close">Fermer</button>
                    <a href="./index.php?view=modifAdherent&id='. $idAdherent . '&id2=' . $idAdhesion . '&saison=' . $saison .'"><button type="button" class="buttonAdherent modify">Modifier</button></a>
                    <button type="button" class="buttonAdherent delete">supprimer</button>
                </div >';
    
        // execution des requetes    
        $acpa->execTransaction($db);
    }
    catch(Exception $e)
    {
        // s'il y a une erreur on annule la transaction
        $acpa->abordTransaction($db);

        // Création d'un message d'erreur
        $message = "il y a eut une erreur la fiche adherent n'as pas été crée \n";
        $message .= 'Erreur : '.$e->getMessage()."\n";
        $message .= 'N° : '.$e->getCode();

        // envoi du message d'erreur pour affichage et arret de l'execution
        echo $message;
        exit();
    }
    // affichage de la fiche adherent
    echo $contenu;
}
else
{
    //si pas de variable affichage d'un code d'erreur 403
	http_response_code(403);
	
}

?>
