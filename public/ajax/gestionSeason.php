<?php
/* document ajax permettant l'ajout, la suppression ou l'activation des saisons dans la bdd depuis un script jquery */


// appel du fichier qui contient les requettes MySQL
require_once('../../model/Acpa.php');

// récuperation de la variable qui determine l'action sur la catégorie
$action = $_POST['action'];

// création des instances de class
$acpa = new Acpa();
$setterAcpa = new SetterAcpa();
$getterAcpa = new GetterAcpa();

try
{
    // tentative de réalisation des requettes
    
    // connection a la BDD
    $db = $acpa->dbconnect();
    
    // demarrage de la transaction
    $acpa->startTransaction($db);
    
    // si l'action est activate
    if ($action == "activate"){

        // recuperation des variables
        $idSeason = $_POST['idsaison'];
        
        // test des requetes 
        $setterAcpa->resetSeasons($db);
        $setterAcpa->activateSeason($db, $idSeason);
         
        // Création d'un message
        $message = "la saison ".$idSeason." a été activé";
    
    // si l'action est delete
    }elseif ($action == "delete"){

        // recuperation des variables
        $idSeason = $_POST['idsaison'];
        
        // test des requetes 
        $setterAcpa->deleteTheLineSeason($db, $idSeason);
         
        // Création d'un message
        $message = "la saison ".$idSeason." a été supprimé";
    
    // si l'action est create
    }elseif ($action == "create"){

        // recuperation des variables
        $libSaison = $_POST['libSeason'];
        
        // test des requetes 
        $setterAcpa->CreateSeason($db, $libSaison);
         
        // Création d'un message        
        $message = "la ". $libSaison ." a été créé";
    }
    
   $acpa->execTransaction($db);
}
catch(Exception $e)
{
    $acpa->abordTransaction($db);
    echo "il y a eut une erreur les modification n'ont pas été faite\n";
    echo "erreur: " . $e->getMessage() ."\n";
    echo "N°" . $e->getCode();
    exit();
}

echo $message;