<?php
/* document ajax permettant l'ajout d'un role a une adhesion dans la bdd depuis un script jquery */


// appel du fichier qui contient les requettes MySQL			
require_once('../../model/Acpa.php');


// recuperation des variables
$saison = $_POST['saison'];
$catage = $_POST['catage'];	
$nom = $_POST['nom'];
$prenom = $_POST['prenom'];
$numeroLicence = $_POST['numeroLicence'];
$activite = $_POST['activite'];
$mel = $_POST['mel'];
$siffa = $_POST['siffa'];
$sorting = $_POST['sorting'];


// création des instances de class
$acpa = new Acpa();
$getterAcpa = new GetterAcpa();

try
{
    // tentative de réalisation des requettes
    
    // connection a la BDD
    $db = $acpa->dbconnect();
    
    // demarrage de la transaction
    $acpa->startTransaction($db);
    
    // Test des requetes
    $listAdherent = $getterAcpa->getListAdherents($db, $saison, $catage, $nom, $prenom, $numeroLicence, $activite, $mel, $siffa, $sorting);
    
    // execution des requetes
    $acpa->execTransaction($db);
}
catch(Exception $e)
{
    // s'il y a une erreur on annule la transaction
    $setterAcpa->abordTransaction($db);
    
    // Création d'un message d'erreur
    $message = "il y a eut une erreur la liste n'a pas été recupéré \n";
    $message .= 'Erreur : '.$e->getMessage()."\n";
    $message .= 'N° : '.$e->getCode();
    
    // envoi du message d'erreur pour affichage et arret de l'execution
    echo $message;
    exit();
}

$code="";	
// boucle de création de la liste
while ($adherant = $listAdherent->fetch())
{
    $code = '<tr class="cliquable">
    <td class="nom">' . $adherant['lib_nom'] . '</td>
    <td class="prenom">' . $adherant['lib_prenom'] . '</td>
    <td class="numLicSIFFA">' . $adherant['num_licence_SIFFA'] . '</td>
    <td class="activite">' . str_replace(",",", ",$adherant['list_activite']) . '</td>
    <td class="mel">' . $adherant['lib_mail_personne'] . '</td>
    <td class="derCol">
        <a class="link" href="#" id="">
            <svg class="bi bi-person-square" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                <path fill-rule="evenodd" d="M2 15v-1c0-1 1-4 6-4s6 3 6 4v1H2zm6-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
            </svg>
        </a>
        </td>
    </tr>' . $code;
}
    

// renvoie de la liste pour affichage
echo $code;
			

?>
