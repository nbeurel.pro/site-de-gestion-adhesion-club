<?
/* document ajax permettant l'ajout d'une activité a une adhesion dans la bdd depuis un script jquery */


// appel du fichier qui contient les requettes MySQL
require_once('../../model/Acpa.php');


// recuperation des variables
$idAdhesion = $_POST['idAdhesion'];
$idActivity = $_POST['idActivity'];

// création des instances de class
$acpa = new Acpa();
$setterAcpa = new SetterAcpa();

try
{
    // tentative de réalisation des requettes
    
    // connection a la BDD
    $db = $acpa->dbconnect();
    
    // demarrage de la transaction
    $acpa->startTransaction($db);
    
    // Test des requetes
    $setterAcpa->addOneActivity($db, $idAdhesion, $idActivity);
    
    // execution des requetes
    $acpa->execTransaction($db);
}
catch(Exception $e)
{
    // s'il y a une erreur on annule la transaction
    $acpa->abordTransaction($db);
    
    // Création d'un message d'erreur
    $message = "il y a eut une erreur l'activité n'a pas été ajoutée \n";
    $message .= 'Erreur : '.$e->getMessage()."\n";
    $message .= 'N° : '.$e->getCode();
    
    // envoi du message d'erreur pour affichage et arret de l'execution
    echo $message;
    exit();
}

// renvoi un message
echo 'activité ' . $idActivity . ' créer pour :' .$idAdhesion;