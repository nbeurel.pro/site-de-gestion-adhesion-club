<?php
/* document ajax permettant l'ajout d'un role a une adhesion dans la bdd depuis un script jquery */


// appel du fichier qui contient les requettes MySQ	
require_once('../../model/Acpa.php');


// recuperation des variables
$Name = $_POST['name'];
$nickName = $_POST['nickName'];
$organism = $_POST['organism'];
$mail = $_POST['mail'];
$portable = $_POST['portable'];
$fixe = $_POST['fixe'];
$adress = $_POST['adress'];
$role = $_POST['role'];
$sorting = $_POST['sorting'];


// création des instances de class
$acpa = new Acpa();
$getterAcpa = new GetterAcpa();


try
{
    // tentative de réalisation des requettes
    
    // connection a la BDD
    $db = $acpa->dbconnect();
    
    // demarrage de la transaction
    $acpa->startTransaction($db);
    
    // Test des requetes
    $listContact = $getterAcpa->getListContacts($db, $Name, $nickName, $organism, $mail, $portable, $fixe, $adress, $role, $sorting);
    
    
    $code="";


    // boucle de création de la liste
    while ($contact = $listContact->fetch())
    {
       $code = '<tr class="cliquable">
            <td class="nom">' . $contact['lib_nom'] . '</td>
            <td class="prenom">' . $contact['lib_prenom'] . '</td>
            <td class="organism">' . $contact['lib_organisation'] . '</td>
            <td class="mel">' . $contact['lib_mail_personne'] . '</td>
            <td class="portable">' . $contact['tel_mobile'] . '</td>
            <td class="fixe">' . $contact['tel_fixe'] . '</td>
            <td class="adress">' . str_replace("-sautDeLigne-","<br>" ,$contact['adresse_complete']) . '</td>
            <td class="role">' . $contact['lib_role'] . '</td>
            <td class="derCol">
                <span class="link modifContact" >
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil" fill="green" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5L13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175l-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                    </svg>
                </span>
            </td>
            <td class="derCol">
                <span class="link deleteContact" >
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-x-octagon" fill="red" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4.54.146A.5.5 0 0 1 4.893 0h6.214a.5.5 0 0 1 .353.146l4.394 4.394a.5.5 0 0 1 .146.353v6.214a.5.5 0 0 1-.146.353l-4.394 4.394a.5.5 0 0 1-.353.146H4.893a.5.5 0 0 1-.353-.146L.146 11.46A.5.5 0 0 1 0 11.107V4.893a.5.5 0 0 1 .146-.353L4.54.146zM5.1 1L1 5.1v5.8L5.1 15h5.8l4.1-4.1V5.1L10.9 1H5.1z"/>
                        <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                    </svg>
                </span> 
            </td>
            <td class="hideCol numId">' . $contact['id_personne'] .'</td>
            <td class="hideCol adressPart"><span class="NumRue">' . $contact['lib_adresse'] .'</span><span class="codePostal">' . $contact['num_code_postal'] .'</span><span class="nomVille">' . $contact['lib_ville'] .'</span></td>
            </tr>' . $code;
    }
    
    // execution des requetes
    $acpa->execTransaction($db);
}
catch(Exception $e)
{
    // s'il y a une erreur on annule la transaction
    $acpa->abordTransaction($db);
    
    // Création d'un message d'erreur
    $message = "il y a eut une erreur la liste n'a pas été recupéré \n";
    $message .= 'Erreur : '.$e->getMessage()."\n";
    $message .= 'N° : '.$e->getCode();
    
    // envoi du message d'erreur pour affichage et arret de l'execution
    echo $message;
    exit();
}


// renvoie de la liste pour affichage
echo $code;
                   
?>
