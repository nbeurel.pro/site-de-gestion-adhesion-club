<?php

session_start();
require_once('model/Acpa.php');
if(isset($_SESSION['login']) AND (!empty($_SESSION['login']))){
    $idDocument = $_GET['id'];

    $acpa = new Acpa();
    $getterAcpa = new GetterAcpa();
    
    $db = $acpa->dbconnect();
    $document = $getterAcpa->seeDoc($db, $idDocument);
    $document = $document->fetch();

    $theData = $document['bin_fichier'];
    //echo $idDocument;
    if(stristr($theData, 'PNG')){
        //echo 'le fichier est un png';
        $typFichier = 'image/png';
    }elseif(stristr($theData, 'PDF')){
        //echo 'le fichier est un pdf';
        $typFichier = 'application/pdf';
    }else{
        //echo 'le fichier est un jpeg/jpg';
        $typFichier = 'image/jpeg';
    }

    header ("Content-type: " . $typFichier);
    echo $document['bin_fichier'];
}else{
    echo "acces interdit";
}