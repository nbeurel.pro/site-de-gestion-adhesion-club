<?php
session_start();

require('controller/frontend.php');
require('model/conf.php');

if(isset($_GET['view'])){
    $entry = $_GET['view'];
}else{
    $entry = 'accueil';
}

$idAdherent = "noIdAdherent";
$idAdhesion = "noIdAdhesion";


if(isset($_POST['login']) AND (!empty($_POST['login'])))
{
	$secret = G_SECRET_KEY;
    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
    $responseData = json_decode($verifyResponse);
    if($responseData->success)
    { 
        $login = $_POST['login'];
        $pwd = $_POST['pwd'];
        
        $acpa = new Acpa();
        $getterAcpa = new GetterAcpa();
        
        $db = $acpa->dbconnect();
        $loginAdherents = $getterAcpa->checkIdentifiant($db, $login);
        $loginACPA = $loginAdherents->fetch();
        $loginAcpaUser = $loginACPA['login'];
        $pwdAcpaUser = $loginACPA['pwd'];

        if(password_verify($pwd,$pwdAcpaUser))
        {
            $_SESSION['login'] = $login;
            currentView($entry);
        }else{	
            $msg='identifiant et/ou mot de passe erroné';	
            login($msg);	
        }
    }else{
       $msg= 'bot validation échoué';
	   login($msg);
    }
}elseif(isset($_SESSION['login']) AND (!empty($_SESSION['login']))){		
        currentView($entry);
}else{ 
	$msg= 'essai';
	login($msg);	
}

function currentView($entry)
{
    if($entry == 'accueil'){
        accueil();
    }elseif($entry == 'adherents'){
        adherents();
    }elseif($entry == 'contacts'){
        contacts();
    }elseif($entry == 'inscription'){
        inscription();
    }elseif($entry == 'createLogin'){
        createLogin();
    }elseif($entry == 'saisons'){
        season();
    }elseif($entry == 'categories'){
        category();
    }elseif($entry == 'majcategories'){
        majcategory();
    }elseif($entry == 'moyenPayement'){
        PayMethode();
    }elseif($entry == 'modifAdherent'){
        if((isset($_GET['id'])) AND (!empty($_GET['id'])) AND (isset($_GET['id2']))){
            $idAdherent = $_GET['id'];
            $idAdhesion = $_GET['id2'];
            $saison = $_GET['saison'];
            modifAdherent($idAdherent, $idAdhesion, $saison);
        }else{
            error();
        }  
    }elseif($entry == 'logout'){
        $_SESSION['login'] ="";
        $msg= 'vous êtes deconnecté';
	    login($msg);
    }else{
        error();
    }
}