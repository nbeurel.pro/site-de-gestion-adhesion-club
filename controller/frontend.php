<?php
/*
 * controleur du site: ce fichier fait le lien entre les models (fichier de connexion a la base de données) et les vues (fichier contenant le code HTML)
*/



/*
Appel des models (fichiers) contenant les functions php de requettes MySql:
- Acpa.php qui contient les requettes lié a la base de données adherents de l'ACPA
- Wordpress qui permet de se connecter a la bdd de wordpress
*/
	require_once('model/Acpa.php');
	require_once('model/Wordpress.php');

/**
function accueil:
- liée au viewer accueilView.php
- affiche la page d'acceuil
*/
	function accueil()
	{
        /*
         * connexion a l'object des requettes de visualisation de la bdd acpa
         */
        $acpa = new Acpa();
		$getterAcpa = new GetterAcpa();
        
        $db = $acpa->dbconnect();
        
        /*
         * appel de la requete de recuperation de la liste des adherents
         * compte le nombre d'adherents
         */
		$listAdherents = $getterAcpa->getListAdherent($db);
        $numberAdherents = $listAdherents->fetchAll();
		$numberAdherents = count($numberAdherents);

		/*
        * appel de la requete de recuperation de la liste des contacts
        * compte le nombre contacts
        */
		$listContacts = $getterAcpa->getListContact($db);
        $numberContacts = $listContacts->fetchAll();
		$numberContacts = count($numberContacts);
        
        /*
         * appel de la requete de recuperation de la liste des paiements
         * compte le nombre paiements
         */
		$lignePayments = $getterAcpa->getListPayment($db);
        $numberPayments = $lignePayments->fetchAll();
		$numberPayments = count($numberPayments);
        
        /*
         * appel de la requete de recuperation de la liste des adherents
         * recupere le nom de la saison active
         */
        $season = $getterAcpa->getSaison($db);
        while($seasonSearch = $season->fetch()){
            if ($seasonSearch['yn_saison_ouverte'] = '1')
            {
                $seasonActiv = $seasonSearch['lib_saison'];
            }
        }
		
		require('view/accueilView.php');
	}



/**
function adherents:
- liée au viewer adherentsView.php
- affiche la page de gestions des adherents
*/	
	function adherents()
	{
        /*
         * connexion a l'object des requettes de visualisation de la bdd acpa
         */
		$acpa = new Acpa();
		$getterAcpa = new GetterAcpa();
        
        $db = $acpa->dbconnect();
        
        /*
         * appel des requetes de recuperation de:
         * - la liste des saisons
         * - la liste des activités
         * - la liste des catégories
         */
		$season = $getterAcpa->getSaison($db);
		$activity = $getterAcpa->getactivity($db);
		$category = $getterAcpa->getcategory($db);
        
        /*
         * connexion a l'object des requettes de visualisation de la bdd wordpress
         */
		$getterWordpress = new GetterWordpress();
        
        /*
         * appel de la requete de recuperation de la liste des inscription en attente
         */
		$numberReEnrollement = $getterWordpress->getNumberReEnrollement();
		
		require('view/adherentsView.php');
	}

/**
function inscription:
- lié au viewer listWaitingAdhesionView.php
- affiche la page de gestion des inscriptions en attentes
*/	
	function inscription()
	{
        /*
         * connexion a l'object des requettes de visualisation de la bdd Wordpress
         */
		$getterWordpress = new GetterWordpress();
        
        /*
         * appel des requetes de recuperation de:
         * - la liste des nouvelles adhesions
         * - la liste des re-inscriptions
         *
         */
		$listAdherentWait = $getterWordpress->getWaitingMembership();
		$listAdherentReEnrollement = $getterWordpress->getWaitingReEnrollement();
			
		require('view/listWaitingAdhesionView.php');
	}
	


/**
function modifAdherent:
- lié au viewer modifyAdherent.php
- affiche la page de modifications des adherents/adhesions
 * @param $idAdherent int identifiant de l'adherent
 * @param $idAdhesion int identifiant de l'adhesion
 * @param $saison int identifiant de la saison
 */
    function modifAdherent($idAdherent, $idAdhesion, $saison)
	{
        // connexion a l'object des requettes de visualisation de la bdd acpa
		$acpa = new Acpa();
		$getterAcpa = new GetterAcpa();
        
        $db = $acpa->dbconnect();
        
        /*
         * appel des requetes de recuperation de:
         * - information de l'adherent et de son adhesion (utilisation des id)
         * - la liste des categories
         * - la liste des types de licences
         * - la liste des status d'adhesions
         * - la liste des adherents
         * - la liste des saisons
         * - la liste des activités
         * - la liste des roles
         */
		$adherent = $getterAcpa->getAdherentById($db, $idAdherent, $idAdhesion);
		$category = $getterAcpa->getcategory($db);
		$typeLic = $getterAcpa->getTypeLic($db);   
		$StatusAdhesion = $getterAcpa->getStatusAdhesion($db);
        $listAdherent = $getterAcpa->getListAdherent($db);
        $season = $getterAcpa->getSaison($db);
        $activity = $getterAcpa->getActivity($db);
        $roles = $getterAcpa->getRoles($db);
			
		require('view/modifyAdherent.php');
	}



/**
function contacts:
- lié au viewer contactsView.php
- affiche la page de gestion des contacts
*/	
	function contacts()
	{
        /*
         * connexion a l'object des requettes de visualisation de la bdd acpa
         */
		$acpa = new Acpa();
		$getterAcpa = new GetterAcpa();
        
        $db = $acpa->dbconnect();
        
        /*
         * appel des requetes de recuperation de:
         * - la liste des roles
         * - la liste des organismes
         */
		$roles = $getterAcpa->getRoles($db);
        $organismes = $getterAcpa->getOrganisation($db);
        
		require('view/contactsView.php');
	}
	
/**
function saison:
- lié au viewer seasonView.php
- affiche page de gestion des saisons
*/	
	function season()
	{
        /*
         * connexion a l'object des requettes de visualisation de la bdd acpa
         */
		$acpa = new Acpa();
		$getterAcpa = new GetterAcpa();
        
        $db = $acpa->dbconnect();
        
        /*
         * appel de la requete de recuperation de la liste des saisons
         */
		$season = $getterAcpa->getSaison($db);
        
		require('view/seasonView.php');
	}
	
/**
function category:
- lié au viewer categoryView.php
- affiche page de gestion des catégories
*/	
	function category()
	{
        /*
         * connexion a l'object des requettes de visualisation de la bdd acpa
         */
		$acpa = new Acpa();
		$getterAcpa = new GetterAcpa();
        
        $db = $acpa->dbconnect();
        
        /*
         * appel de la requete de recuperation de la liste des categories
         */
		$category = $getterAcpa->getCategory($db);
        
		require('view/categoryView.php');
	}
	
/**
function majcategories:
- lié au viewer majcategoryView.php
- affiche la page de gestion des adherents qui change de categories
*/	
	function majcategory()
	{
        /*
         * connexion a l'object des requettes de visualisation de la bdd acpa
         */
		$acpa = new Acpa();
		$getterAcpa = new GetterAcpa();
        
        $db = $acpa->dbconnect();
        
        /*
         * appel des requetes de recuperation de:
         * - la liste des categories
         * - la liste des adherents
         */
		$category = $getterAcpa->getcategory($db);
		$listAdherents = $getterAcpa->getListAdherent($db);
        
		require('view/majcategoryView.php');
	}
	


/**
function PayMethode:
- lié au viewer paymentMethodeView.php
- affiche la page de gestion des moyens de paiements
*/	
	function PayMethode()
	{
        /*
         * connexion a l'object des requettes de visualisation de la bdd acpa
         */
		$acpa = new Acpa();
		$getterAcpa = new GetterAcpa();
        
        $db = $acpa->dbconnect();
        
        /*
         * appel de la requete de recuperation de la liste des moyens de paiements
         */
		$payMethod = $getterAcpa->getPayMethod($db);
        
		require('view/paymentMethodeView.php');
	}

	
/**
function login:
- lié au viewer login.php
- affiche la page de login
*/		
	function login($msg)
	{			
		require('view/login.php');
	}
	
/**
function createLogin:
- lié au viewer gestionLogin.php
- affiche la page gestion des login
*/		
	function createLogin()
	{			
		require('view/gestionLogin.php');
	}

	
/**
function createLogin:
- lié au viewer error.php
- affiche une page d'erreur
*/	
function error()
	{
		require('view/error.php');
	}
